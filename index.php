<?php
  session_start();
  include 'config/koneksi.php';
  include 'config/error_reporting.php';
  include 'lib/lib_general.php';            
  include 'lib/lib_timeout.php';
  include 'lib/lib_security.php';
  include 'lib/defined_session.php';

  use lib\general;
  use lib\timeout;
  use lib\security;

  if (!empty($_SESSION[SESSKEY])) {
    $path = general::parsing_url(security::get_ec($_GET['ec']));
    if (strncmp($path['sid'], $_SESSION[SESSID],32 ) == 0)
      $salt_valid = true;
    else
      $salt_valid = false;
  }

  //echo session_id().':::'.$secure_session;
  if (session_id() != security::get_secured_session_id())
    $hash_session_valid = false;
  else
    $hash_session_valid = true;

  //hapus session sudah login jika melewati timeout
  if( general::get_login() == 1 AND $salt_valid AND $hash_session_valid){
    if( ! timeout::cek_login()){
      general::set_login(0);
      echo $_SESSION[SESSLOGIN];
    }
  }else{
    include "modul/error_page/404.php";
    return;
  } 
?>
<!DOCTYPE html>
<html>
  <head>
    <title>Sistem Aplikasi Layanan Aspirasi Masyarakat</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">    
    <link href="public/css/style.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="public/css/bootstrap.min.css" rel="stylesheet" type="text/css" media="screen" /> 
    <link href="public/css/jquery.smartmenus.bootstrap.css" rel="stylesheet" type="text/css" media="screen" />

    <script>
    //var last_timestamp = 0;
    //alert(document.cookie);
    </script>
    <style>
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
    </style>
  </head>
  <body  onload="initialize();">


      <div class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">          

          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"><img src="public/images/logo.png" height="40px"/></a>
          </div>
          
          <div class="navbar-collapse collapse">
          
            <!-- Left nav -->
            <ul class="nav navbar-nav">
              <?php 
                //generate menu
                include "menu.php"; 
              ?>     
            </ul>
          </div><!--/.nav-collapse -->
        </div><!--/.container -->
      </div>

      <div id="search-style" class="search-bar-style" > 
      </div>
      <div id="search" class="search-bar" > 
        <input type=text id='search-text' placeholder='cari konten di bagian ini' style="margin-left:-19px;">
      </div>
      <div id="titte" class="title-bar" > 
        <span><h2><?=$path['header']?></h2></span>
      </div>
      
      <div class="container-fluid padded" style="margin-top:40px;   ">
        <div class="row" style="margin-top:20px">

            
            <div class="flashmsg">
            <?php 
              //taroh di GET['ec'] jg bisa neh pesan error misalnya
              //echo '<center>test</center>';
              //flash.output() 
            ?>
            </div>

            <?php 
              include 'content.php';
            ?>
            <br/>

        </div>
      </div>

    <script type="text/javascript">
      $('body').scrollspy({
          target: '.bs-docs-sidebar',
          offset: 40
      });
    </script>
    <script type="text/javascript" src="public/js/jquery-1.10.2.min.js"> </script>   
    <script type="text/javascript" src="public/js/bootstrap.min.js"> </script>   
    <script type="text/javascript" src="public/js/jquery.smartmenus.min.js"> </script>   
    <script type="text/javascript" src="public/js/jquery.smartmenus.bootstrap.min.js"> </script>
    <script type="text/javascript" src="public/js/highcharts.js"></script>
    <script type="text/javascript" src="public/js/exporting.js"></script> 
  </body>
</html>