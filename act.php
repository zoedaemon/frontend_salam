<?php

include 'config/koneksi.php';
include 'config/error_reporting.php';
include 'lib/defined_session.php';
include 'lib/lib_general.php';
include 'lib/lib_security.php';

use lib\general;

if (isset($_GET['m']))
	$unit = 'main';
elseif (isset($_GET['tt']))
	$unit = 'setting';
else {
	echo 'invalid command...!!!';
	return;
}

if (isset($_GET['s']))
	$modul = $_GET['s'];
else {
	echo 'invalid command...!!!';
	return;
}
//rangkai path dari parameter yg valid (TODO: Ujicoba keamanannya dengan kombinasi '../..')
$c = str_replace('"', '', $_GET['c']);
$command = 'modul/'.$unit.'/'.$modul.'/'.$c;
//echo $command;
//header('location:./'.$command);
$command = explode('?', $command);
$FakeGet = general::parsing_url($command[1]);
foreach ($FakeGet as $key => $value) {
	$_GET[$key] = $value;	
	//echo "$key => $value <br>\n";
}
//amankan dari tangan jahil...
$included_me = str_replace('../', '', $command[0]);
//include file gak boleh pakai '?' langsung akses file
include $included_me;
?>