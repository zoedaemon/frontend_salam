<?php
	session_start();
	//include_once "plugin/hak_akses/define.php";
  include "config/error_reporting.php";

  include "lib/lib_security.php";
  use lib\security;


  //redirect https
  if (!(isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] == 'on' || 
     $_SERVER['HTTPS'] == 1) ||  
     isset($_SERVER['HTTP_X_FORWARDED_PROTO']) &&   
     $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https'))
  {
     $redirect = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
     header('HTTP/1.1 301 Moved Permanently');
     header('Location: ' . $redirect);
     exit();
  }
  
?>
<!-- DOCTYPE html -->
<html>
<head>

<title>Login</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="shortcut icon" href="public/images/login.jpg" />
<link rel="stylesheet" href="public/css/bootstrap.min.css" />
<link rel="stylesheet" href="public/css/bootstrap-responsive.min.css" />
<link rel="stylesheet" href="public/css/style-login.css?" />
<link href="public/font-awesome/css/font-awesome.css" rel="stylesheet" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>

</head>
<body>

	<script language="javascript">
	function validasi(form){
	  if (form.username.value == ""){
	    alert("Username tidak boleh kosong...!!");
	    form.username.focus();
	    return (false);
	  }
	     
	  if (form.password.value == ""){
	    alert("password tidak boleh kosong...!!");
	    form.password.focus();
	    return (false);
	  }
	 
	  return (true);
	}
	</script>


<?php

//session_start();
//include "timeout.php";

if (empty($_SESSION['user.'.$_SESSION['id_session']]) AND empty($_SESSION['login.'.$_SESSION['id_session']])) {
      $salt = base64_encode(mcrypt_create_iv(PBKDF2_SALT_BYTE_SIZE, MCRYPT_DEV_URANDOM));
      $key = pbkdf2(
              PBKDF2_HASH_ALGORITHM,
              PBKDF2_HASH_BYTE_REGEX,
              $salt,
              PBKDF2_ITERATIONS,
              PBKDF2_HASH_BYTE_SIZE,
              true
            );

      $_SESSION['key'] = $key;
      $_SESSION['salt'] = $salt;//salt harus disimpan lewat session takutnya enkripsi ketahuan nanti hacker bisa seenaknya naroh nilai salt
      $dat = "salt=$salt&unit=defaut&modul=home&\0";
      $enc = security::mcrypt_en($dat, $key, $iv_size);
      $_SESSION['iv_size'] = $iv_size;

    ?>
    <div id="loginbox">            
        <form id="loginform" class="form-vertical" method=POST action=<?php echo "'cek_login.php?ec=".security::mcrypt_en_urlencode($enc).(isset($_GET['TEST'])?"?TEST":"")."'"; ?> onSubmit='return validasi(this)'>
         <div id="logo"  class="control-group normal_text"><h3><img height="70px" src="public/images/logo.png" alt="Logo" style="margin-left:-20px"/></h3></div>
                <div class="control-group">
                    <div class="controls">
                        <div class="main_input_box">
                            <span class="add-on bg_lg"><i class="icon-user"> </i></span>
                            <input type="text" name="username" placeholder="Username" />
                        </div>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <div class="main_input_box">
                            <span class="add-on bg_lo"><i class="icon-lock"></i></span>
                            <input type="password" name="password" placeholder="Password" />
                        </div>
                    </div>
                </div>
                <div class="form-actions">
                    <span class="pull-left"><a href="index.php" class="flip-link btn btn-info" <?php //id="to-recover"?> >Kembali Ke Halaman Utama</a></span>
                    <span class="pull-right"><input class="btn btn-success" type=submit value=Login ></span>
                </div>
            </form>
            <form id="recoverform" action="#" class="form-vertical">
        <p class="normal_text">Masukkan email di bawah ini dan nanti kami akan mengirim email yang berisi instruksi untuk reset password anda</p>
        
                    <div class="controls">
                        <div class="main_input_box">
                            <span class="add-on bg_lo"><i class="icon-envelope"></i></span><input type="text" placeholder="E-mail address" />
                        </div>
                    </div>
               
                <div class="form-actions">
                    <span class="pull-left"><a href="#" class="flip-link btn btn-success" id="to-login">&laquo; Back to login</a></span>
                    <span class="pull-right"><a class="btn btn-info"/>Recover</a></span>
                </div>
            </form>
        </div>
        
    <?php

}else {


    /*if ( $_SESSION['hak_akses'] == ADMIN ) {
     
      echo "<form method=POST action=pilih_menu.php>
      <table>
      <tr><td>Masuk Menu Menu Utama</td></tr>
      <tr><td>
              <dd >
                    <center>  
                    
                    <input type = 'radio' name ='main_menu' value='profil' checked>  Profil 
                    &nbsp &nbsp &nbsp &nbsp
                    <input type = 'radio' name ='main_menu' value='SI' >  Sistem Informasi 
                    </center> 
              </dd>
      <td></td></tr>
      <tr><td>&nbsp;</td></tr>
      <tr><td colspan=1 align=center><input type=submit value=Masuk ></td></tr>
      </table>
      </form>";
    
    }else {
    */
      echo "<center>Anda tidak memiliki izin mengakses bagian ini ! <br>";
      echo "<center><h5>Silahkan tunggu beberapa saat anda akan diarahkan ke halaman utama.</h5></center>";
      echo "<script>setTimeout(function(){location.href='logout.php';},3000);</script>";

    //}

    
}

?>

    <script src="public/js/jquery-1.10.2.min.js"></script>  
    <script src="public/js/login.js"></script> 

</body>
</html>