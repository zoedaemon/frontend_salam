<?php

/**
file ini dipanggil oleh aksi.php
*/
use lib\security;


$build_query .= ( $counter > 2 ? 	
					( 
						($value['type'] == 'file' && ! $_FILES[ $key ]['tmp_name'] ) || //jika file gambar kosong abaikan koma
						($value['type'] == 'password' && empty($_POST[ $key ]) ) //atau password kosong abaikan juga 
						? '' : ','
					) 
					: '');



switch ($value['type']) {
	
	//query builder tuk input dgn type hidden
	case 'hidden':
		
		//gak perlu validasi
		$val = security::anti_injection($mysqli, $_POST[ $value['reference'] ] );
		
		//generate query cuy
		if ($act=='tambah') {

			$build_query_column .= ",".$key;
			$build_query_value .= ",'".$val."'";
		
		}else {//update

			$build_query .= $key; //NOTE: koma diabaikan untuk kolom pertama 
			$build_query .= "='".$val."'";
		}

	break;

	case 'text':

			//cek tipe khusus tuk input text =>  spesial.type:[date|int]			
			switch ( $value['special']['type'] ) {

				//khusus tanggal					
				case 'date':
					try {

						//generate query cuy
						$val = cek_tgl( security::anti_injection($mysqli, $_POST[ $key ]), $value['special']['delimiter']  );
						$val = tgl_to_db( $val, $value['special']['delimiter'], $value['special']['db_delimiter']);

						if ($act=='tambah') {

							$build_query_column .= ",".$key;
							$build_query_value .= ",'".$val."'";
						
						}else {//update

							$build_query .= $key; //NOTE: koma diabaikan untuk kolom pertama 
							$build_query .= "='".$val."'";
						}

					}catch (Exception $e) {
						
						//tampilkan error
						echo "<script>alert('".$e->getMessage()." : kolom \"$value[special]\"');".redirect($redirect_act.(
								$act=='edit'?('&id='.$_POST['id']):''), 2000).'</script>';


						$validation = false; 
						break;	

					}
				break;
				
				//khusus data integer
				case 'int':

					try {
						//generate query cuy
						$val = cek_format_angka( security::anti_injection($mysqli, $_POST[ $key ] ) );
						
						if ($act=='tambah') {

							$build_query_column .= ",".$key;
							$build_query_value .= ",'".$val."'";
						
						}else {//update

							$build_query .= $key; //NOTE: koma diabaikan untuk kolom pertama 
							$build_query .= "='".$val."'";
						}

					}catch (Exception $e) {
						
						//tampilkan error
						echo "<script>alert('".$e->getMessage()."');".redirect($redirect_act, 2000).'</script>';


						$validation = false; 
						break;	

					}
				break;

				//defaulnya data text
				default:

					//gak perlu validasi
					$val = security::anti_injection($mysqli, $_POST[ $key ] ) ;
					
					//generate query cuy
				
					if ($act=='tambah') {

						$build_query_column .= ",".$key;
						$build_query_value .= ",'".$val."'";
					
					}else {//update

						$build_query .= $key; //NOTE: koma diabaikan untuk kolom pertama 
						$build_query .= "='".$val."'";
					}

				break;
			}


	break;

	case 'password':
		//gak perlu validasi
		$val = security::anti_injection($mysqli, $_POST[ $key ] );
		//generate query cuy
		if ($act=='tambah') {

			$build_query_column .= ",".$key;
			$build_query_value .= ",'".md5($val)."'";
		
		}else {//update

			if ( ! empty($val) ) {
				$build_query .= $key; //NOTE: koma diabaikan untuk kolom pertama 
				$build_query .= "='".md5($val)."'";
			}
		}

			
	break;

	//NOTE: combobox n radio button
	case 'select':
	case 'radio':


		//gak perlu validasi
		$val = security::anti_injection($mysqli, $_POST[ $key ] );
		//generate query cuy

		if ($act=='tambah') {

			$build_query_column .= ",".$key;
			$build_query_value .= ",'".$val."'";
		
		}else {//update

			$build_query .= $key; //NOTE: koma diabaikan untuk kolom pertama 
			$build_query .= "='".$val."'";
		}


	break;


	case 'file':
			
			if ( $_FILES[ $key ]) {

				$lokasi_file    = $_FILES[ $key ]['tmp_name'];
			  	
					// Apabila ada gambar yang diupload
				if (!empty($lokasi_file)){

					$tipe_file      = $_FILES[ $key ]['type'];
				  	$nama_file      = $_FILES[ $key ]['name'];
				  	$uniq = md5(date('Y-m-d H:i:s'));//generate kode unik spaya foto dgn nama file yang sama tidak tertumpuk/overwrite
				  	$nama_file = $uniq.$nama_file;
	
				  	if ($tipe_file != "image/jpeg" AND $tipe_file != "image/pjpeg"){
					    
					    redirect($redirect_act, 2000);
					    echo "<script>window.alert('Upload Gagal, Pastikan File yang di Upload bertipe *.JPG');</script>";
					    return;
				    }
					
					//file upload dipindahkan k direktori yang tepat
					move_uploaded_file($lokasi_file, $path_upload.$nama_file);
  	

					//echo "<br>$lokasi_file:$nama_file<br>";

					//gak perlu validasi
					$val = $nama_file;
					
					//generate query cuy
					if ($act=='tambah') {

						$build_query_column .= ",".$key;
						$build_query_value .= ",'".$val."'";
					
					}else {//update

						$build_query .= $key; //NOTE: koma diabaikan untuk kolom pertama 
						$build_query .= "='".$val."'";
					}
				}
			}
	break;

	default:
			echo 'object tidak bisa digenerate - '.$item['type']; 
	break;
}


?>