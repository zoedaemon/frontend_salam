
<SCRIPT TYPE="text/javascript">
<!--
// copyright 1999 Idocs, Inc. http://www.idocs.com
// Distribute this script freely but keep this notice in place
function numbersonly(myfield, e, dec)
{
  var key;
  var keychar;

  if (window.event)
    key = window.event.keyCode;
  else if (e)
    key = e.which;
  else
    return true;
  
  keychar = String.fromCharCode(key);

  // control keys
  if ((key==null) || (key==0) || (key==8) || (key==9) || (key==13) || (key==27) )
    return true;
  // numbers
  else if ((("0123456789").indexOf(keychar) > -1))
    return true;
  // decimal point jump
  else if (dec && (keychar == "."))
  {
    myfield.form.elements[dec].focus();
    return false;
  }
  else
    return false;
}


//-->
</SCRIPT>









<?php


//config form dll
include 'conf.php';

//javascript tuk popup menu   
include 'plugin/form/js_popup.php'; 
 
//tuk operasi view tambah dan edit  
include 'plugin/form/popup_edit.php';  

//tuk operasi view tambah dan edit 
include 'plugin/form/render.php';


//ambil string judul tabel view 
$table_title = $conf['table_title'];
$caption = $conf['caption'];


//ambil nama table database
$private_table_name = $conf['private_table_name'];
//ambil path untuk foto 
$conf_image_path = $conf['image_path'];
//ambil data khusus item
$inputs = $conf['inputs'];
//ambil index key
$keys = array_keys($inputs);

//KHUSUS QUERY SETTING
//urutkan berdasarkan
$query_order_by = $conf['query_order_by'];
$query_adsc = $conf['query_adsc'];



//active column adalah inputs yang bukan hidden ato yang diperlukan dlm operasi tambah dan edit
$index_active_column_start = 1;
$index_active_column_end = sizeof( $inputs );


$aksi = $conf['aksi_url'];


//query default, tp nanti akan dimanipulasi jika ada pencarian ato GROUPINGg 
$query = "SELECT * FROM $private_table_name ";



switch($_GET['act']){
  
  // Tampil pegawai
  default:
    //formtarget='_blank' ???


    //print template
    ?>

    <h1  class="title-separator"><?=$table_title?></h1>
    <!-- ini div bantuan karena left-content-fixed adalah fixed-->
    <div class='row-fluid col-md-10 right-content-helper' >
      <div  class=" crud-container" style="margin:5px 0px;">      
          <div class="widget-box">
                <div class="widget-title"> <span class="icon"> <i class="icon-th"></i> </span>
                </div>
                <div class="widget-content ">

          <?php
          //hidden jika dlm mode cetak
          if ($_GET['do'] != 'cetak') {
            
              //khusus searching
              include 'searching.php';
              
              //memastikan salah satu atribut grouping aktif
              $grouping = false;
              //NOTE GROUPING data ada bagian ini
      //        include 'grouping.php';
        

              include 'searching_generate_query.php';

          }//END CEtak



          if ( !isset($_GET["do"])) { 
            echo "
                <br/>
                <br/>
                <h2></h2>  
                    <input type=button value='Cetak' 
                       class='btn btn-success'
                      style='float:left; margin-left:0px'
                      onclick=\"location.href='cetak.php?$_SERVER[QUERY_STRING]&do=cetak'\">
                  
                  <p style='display: table; margin: -40px auto 0px auto''>   
                    <input style=' margin-bottom:5px;'  class='btn btn-success'  type=button value='Tambah $caption' onclick=\"location.href='?$_SERVER[QUERY_STRING]&act=tambah'\">
                  </p>

              ";
          }
          echo "
            <table class='table table-bordered table-striped with-check'  ".(($_GET['do'] != 'cetak')?"":"style='margin-left:70px'").">
              
              <thead>
                  <tr>    
                    <th>no</th>";

            //tampilkan kolom
            for ($i=$index_active_column_start; $i <= $index_active_column_end; $i++) {
            
                if ( isset($inputs[ $keys[$i] ] ['show_in_table']) AND $inputs[ $keys[$i] ] ['show_in_table'] == true )  
                    echo '<th>'. $inputs[ $keys[$i] ] ['caption'].'</th>';          
            
            }


          if ( $_GET['do'] != 'cetak') 
            echo "    <th colspan='3' align='center'>aksi</th>
                    </tr>";
          
          echo "</tr></thead>";

          $p      = new Paging;
          $batas  = 10;
          $posisi = $p->cariPosisi($batas);


          //$query_original digunakan tuk paginasi
          $query .= " ORDER BY $private_table_name.$query_order_by $query_adsc";
          $query_original = $query;

          //echo $query.'::'.substr($_GET['tahun'], 0, 3);
              
          //jgn tampilkan paginasi jika dalam modus cetak  
          if ($_GET['do'] != 'cetak') {
            $query .= " limit $posisi,$batas";
          }


          $tampil = $mysqli->query($query);
          $no =$posisi+1;
          

          echo $tampil->error; 

          echo "<tbody>";

          while($r=$tampil->fetch_array(MYSQLI_BOTH))
          {
            
            if ($r['type'] == 'Adm')
              continue;      
            
            $nip = "";
            echo "<tr><td>$no</td>";
        
            /**
            tampilkan data di tiap kolom yg bersesuaian, ambil dari file konfigurasi
            */
            for ($i=$index_active_column_start; $i <= $index_active_column_end; $i++) {
            
                if ( isset($inputs[ $keys[$i] ] ['show_in_table']) AND $inputs[ $keys[$i] ] ['show_in_table'] == true )  {
                  
                    //2 kolom neh menunjukan foreign key
                    if ($inputs[ $keys[$i] ] ['type'] == 'select'|| $inputs[ $keys[$i] ] ['type'] == 'radio') {

                        echo '<td>'. $conf['inputs'][ $keys[$i]  ] ['values'] [ $r[ $keys[$i] ] ].'</td>';  

                    }elseif ($keys[$i] == 'nip') {
                        $nip = $r[$keys[$i]];
                        echo '<td>'."<a href='?unit=$_GET[unit]&module=pgw_pendidikan_terakhir&parent=pgw&nip=".$r[$keys[$i]].'\'>'.
                                (($r[$keys[$i]] == -1)? 'HONORER': $r[$keys[$i]] )
                                .'</a></td>';          
                    }else if (substr_count($keys[$i], 'id_') >= 1) {

                      //replace aza langsung tuk dapatkan nama tabelnya
                      $table = str_replace('id_', '', $keys[$i]);
                      $id_where = 'id';
                      if ( ! $r[ $keys[$i] ]) {
                        echo '<td></td>';
                        continue;
                      }
                      //ambil data dan supress warning andaikan primary key tidak ditemukan
                      $new_query = "SELECT * FROM $table WHERE $id_where = ".$r[ $keys[$i] ];
                      $datum = $mysqli->query($new_query);
                      //$data = @mysql_fetch_array( @mysql_query($new_query) );
                      $data = $datum->fetch_array(MYSQLI_BOTH);
                      //print data hasil query
                      echo '<td>'.($keys[$i] == 'id_SKPD' ? 
                                      $data['nama_skpd'] : 
                                      $data['nama_subskpd'] 
                                  ) .'</td>';  

                    }/*elseif ($keys[$i] == 'type') {
                        
                        $real_value = '';
                        $input_type = $inputs[ $keys[$i] ]['values'] ;
                        foreach ( $input_type['prepend_values'] as $val => $cap) {
                          if ( $r[ $keys[$i] ] == $val) {
                            $real_value = $cap;
                          }
                          //echo '<td>1)'.$r[ $keys[$i] ].' vs '.$val.'</td>';
                        }

                        $exec_query = mysql_query( $input_type['query'] );
                        while ( $eq = mysql_fetch_array($exec_query) ) {
                          if ( $r[ $keys[$i] ] == $eq[ $input_type['value'] ]) {
                            $real_value = $eq[ $input_type['caption'] ];
                          }
                          //echo '<td>2)'.$r[ $keys[$i] ].' vs '.$eq[ $input_type['value'] ].'</td>';          
                        }
                        
                        echo '<td>'.$real_value.'</td>';          
                        
                    }*/else {
                    
                        echo '<td>'.$r[$keys[$i]].'</td>';          
                    
                    }
                }
            
            }

            /*
              <!--<td>
              <a href=\"?unit=$_GET[unit]&module=pgw_pendidikan_terakhir&parent=pgw&nip=".$nip."\"><i class='icon-hdd' title='Detail $caption' ></i></a>
              |
              <a href=\"?unit=$_GET[unit]&module=$_GET[module]&parent=$_GET[parent]&act=edit&id=".$r[ $conf['inputs_default_id'] ]."\"><i class='icon-edit' title='Edit $caption'></i></a>
              |
              <a href=\"?unit=$_GET[unit]&module=$_GET[module]&parent=$_GET[parent]&submodule=aksi&act=hapus&id=".$r[ $conf['inputs_default_id'] ]." \" onClick=\"return confirm('Apakah Anda Yakin Menghapus Data $caption : $r[nama] ??')\"><i class='icon-trash' title='Delete $caption'></i></a>
              -->
            */
            if ( $_GET['do'] != 'cetak' ) 
              echo "
                    <td>
                      <a href=\"?$_SERVER[QUERY_STRING]&nip=$_GET[nip]&act=edit&id=".$r[ $conf['inputs_default_id'] ]."\"><i class='glyphicon glyphicon-edit' title='Edit $caption'></i></a>
                    </td>
                    <td>
                      <a href='$aksi&act=hapus&id=".$r[ $conf['inputs_default_id'] ]."' onClick='return confirm(\"Apakah Anda Yakin Menghapus Data $caption : ".$r[$conf['inputs_rows_name']]." ??\")'><i class='glyphicon glyphicon-trash' title='Delete $caption'></i></a>
                    </td>
                  "; 
            
            echo "</tr>";
            $no++; 


          }
          echo "
            <tbody>
          </table>
      ";
          
      
      $jmldata = $mysqli->query($query_original)->num_rows;
      $jmlhalaman  = $p->jumlahHalaman($jmldata, $batas);
      $linkHalaman = $p->navHalaman($_GET['halaman'], $jmlhalaman);  
      if ($_GET['do'] != 'cetak')
        echo "<div id=paging>$linkHalaman</div><br>";

    break;
  

    echo ' </div>
      </div>
    </div>
  </div>
';






    //tambah dan ubah langsung di sini
  case "tambah":
  case "edit":
   ?>
    <div class='row-fluid col-md-10 right-content-helper' style="padding:50px!important;">
      <div  class=" crud-container" style="margin:5px 0px;">
        <?php include 'form.php';   ?>
      </div>
    </div>
    <?php
  break;  
  


}



?>
