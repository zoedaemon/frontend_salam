<?php
	/**
		NOTE: 
		-bedanya option public dan hide_when_login adalah :
			public = true maka akan ditampilkan saat belum login maupun sudah login
			public = false maka TIDAK akan ditampilkan saat belum login tp ditampilkan saat sudah login
			hide_when_login = true maka akan ditampilkan saat belum login tp akan disembunyikan saat sudah login
			hide_when_login = false maka akan DISEMBUNYIKAN saat belum login tp akan dimunculkan saat sudah login
	*/
	$conf = array(
        
        'caption' => 'User',
        'table_title' => 'Manajemen User',
        
        'image_path' => '../../Pengunjung/gambar/foto_siswa/',
        'private_table_name' => 'pengguna',

        'query_order_by' => 'username',//SELECT blablabla ORDER BY id
        'query_adsc' => 'DESC',//SELECT blablabla ORDER BY id DESC

        'inputs_default_id' => 'id',

        'aksi_url' => "act.php?s=$path[module]&tt=1&c=\"aksi.php?\"&$_SERVER[QUERY_STRING]",
        
        //id default di dalam tabel
        'inputs_default_id' => 'id',
        'inputs_rows_name' => 'username',
        
        'inputs' => array(//ITEMS yang ditampilkan di tabel (view/tambah/edit)

        	//Menyimpan nilai hidden
			'id' => array(
				'reference' => 'id',//dipanggil sebagai parameter $_GET[reference]
				'attributes' => '',
	            'type' => 'hidden'
	       	),


			//Muncul sebagai masukkan
            'username' => array(
				'caption' => 'Nama',
				'attributes' => 'maxlength=30 size=30 ',
	            'type' => 'text',
	            'show_in_table' => true// tampil d kolom tabel
	       	),

	       	'type' => array(
				'caption' => 'Hak Akses',
				'attributes' => 'maxlength=20 onKeyPress="return numbersonly(this, event)"',
	            'type' => 'select',
	            'show_in_table' => true,// tampil d kolom tabel
	            //hampir sama dengan pendeklarasian dengan combobox
	            'values' => array(
	            	/*'query' => 'SELECT * FROM skpd',
	            	'value' => 'singkatan',
	            	'caption' => 'nama_skpd',
	            	'prepend_values' => array (
	            		'Adm' => 'Admin',
	            		'Aud' => 'Auditor',
	          		) */ 
	            	//'values' => array (
	            		//'Adm' => 'Admin',
	            		'Aud' => 'Auditor',
	          		//)
	          	)  
 	       	),

	       	'password' => array(
				'caption' => 'password',
				'attributes' => 'maxlength=50',
				'info_when_edit' => ' ----- kosongkan jika tidak ingin mengubah password',
	            'type' => 'password',
	       	),

        )
  

    );

?>