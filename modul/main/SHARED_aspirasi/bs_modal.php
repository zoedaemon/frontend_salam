<!-- Modal -->
  <div id="DetailModal" class="modal fade-scale" role="dialog" tabindex="-1"  aria-labelledby="exampleModalLabel">
    <div class="modal-dialog" role="document">

<!-- Modal content-->
<div class="modal-content sms-card-popup">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" ><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="exampleModalLabel">NoTelp</h4>
      </div>
      <div class="modal-body">
        <form>
          <div class="form-group">
            <label for="sms-pelaporan" class="control-label" style="margin-top:-15px">SMS Pelaporan :</label>
            <textarea class="form-control" id="sms-pelaporan" rows="5"></textarea>
          </div>
          <div class="form-group">
            <label for="tags-list" class="control-label" >Tags : </label>
            <input type="text" class="form-control" id="tags-list" style="color:#E5E6EA!important;margin-left:3px; margin-top:-5px">
          </div>

          <div class="form-group">
            <label for="lokasi" class="control-label">Lokasi : </label>
            <input type="text" class="form-control" id="lokasi" style="color:#E5E6EA!important;margin-left:3px;margin-top:-5px">
          </div>
          <div class="form-group">
            <a id="is-spam" href="?a"><button type="button" class="btn btn-danger">Tandai Spam</button></a>                        
          </div>
          <!--
            <div class="form-group">
              <button type="button" class="btn btn-success">Tandai Sudah Ditangani</button>
            </div>
          -->
          <div class="form-group">
            <a id="is-public" href="?a"><button type="button" class="btn btn-default">Tandai Public</button></a>
          </div>
          <div class="form-group">
            <p class="detail-timestamp">timestamp di sini</p>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-warning" data-dismiss="modal">Tutup</button>
        <!--<button type="button" class="btn btn-primary">Simpan Perubahan</button>-->
      </div>
    </div>
  </div>
</div>