<?php
use lib\general;
?>
<!-- ini div bantuan karena left-content-fixed adalah fixed-->
<div id="left-content-helper" class="left-content-helper col-md-0  hidden-xs hidden-sm" >   
</div>


<!--<div id="main-content" class="col-md-7 col-md-offset-1 " >  Nambah 1 offset, mirip dengan menambahkan kolom kosong dikiri-->
<div id="main-content" class="col-md-12" >  <!-- Hapus col-md-offset-1 karena ada left-content-->
    <!--<div class="page-header">-->
<?php

//modal/popupnya si bootstrap  
include "bs_modal.php";

//last timestamp pindah ke sini biar aman
$qry = $mysqli->query("SELECT * FROM pelaporan ORDER BY timestamp DESC");
if ($rows = $qry->fetch_array(MYSQLI_BOTH)) {
  echo "<script>
  var last_timestamp = \"$rows[timestamp]\";
  </script>";
}
/*SELECT * FROM (
    SELECT EXTRACT( YEAR_MONTH FROM `timestamp` ) AS thn_bln FROM `pelaporan` ORDER BY `timestamp` DESC
    ) A GROUP BY A.thn_bln
  SELECT *  FROM (
        SELECT YEAR(timestamp) AS 'year', MONTH(timestamp) AS 'month'
      FROM pelaporan 
    )A GROUP BY A.year, A.month ORDER BY A.year, A.month DESC
*/
$que = $mysqli->query("SELECT A.year AS Year, A.month AS Month FROM (
                              SELECT YEAR(timestamp) AS 'year', MONTH(timestamp) AS 'month' FROM pelaporan 
                            )A GROUP BY A.year, A.month ORDER BY A.year, A.month DESC");
//loop berdasarkan thn dan bulan
$waktu_arr = array();
$i = 1;
while ($waktu_rows = $que->fetch_array(MYSQLI_BOTH)) {
  $waktu_arr[] = "$waktu_rows[Month]-$waktu_rows[Year]";
?>
<div id='<?="$waktu_rows[Month]-$waktu_rows[Year]"?>' class='row-fluid' style="float:left;">
<h1 id='<?="$waktu_rows[Month]-$waktu_rows[Year]"?>' class="title-separator"><?=general::getBulan($waktu_rows['Month'])." $waktu_rows[Year]"?></h1>
<div  id="PelaporanCard-<?=$i?>" class="sms-container" style="margin:5px 0px;">
   <?php 
    $i++;
    //query 
    $qry = $mysqli->query("SELECT * FROM pelaporan WHERE MONTH(timestamp) = '$waktu_rows[Month]' 
                              AND YEAR(timestamp) = '$waktu_rows[Year]' AND is_spam = 0 ORDER BY timestamp DESC");
    
    while ($rows = $qry->fetch_array(MYSQLI_BOTH)) {
       
   ?> 
    <div class="col-md-2 sms-card clearfix" style="margin:5px 5px; ">
      <div id="gambar<?=$rows['id']?>" style="margin-bottom:20px">
        <!-- Trigger the modal with a button -->
        <div id="buttons<?=$rows['id']?>" style="button-card">
          <button class="btn btn-info" 
          data-toggle="modal" data-target="#DetailModal" data-id_plpran="<?=$rows['id']?>">Edit</button>
          <a href="?unit" >
          <button class="btn btn-warning" style="float:right"><i class="icon-camera" title="Foto Webcam"></i>Hapus</button>
          </a>
        </div>
        <center> 
          <h3 id='sub1' style="color:#ff8899"><?=$rows['no_telp']?></h3>
          <br/>
          <!--<div id="overflow" style="width:250px;height:250px;">
            <a id="imgurl" title="Image 1" href="public/images/foto_peserta/" target=_blank>
              <img class="thumbnail img-responsive" src="public/images/foto_peserta" width="249px">
            </a>
          </div>-->
        </center>
        <div class="pesan"><?=$rows['pesan']?></div><br/>
        <div class="divtags">
        <strong style="color:#7788ff" class="tags">Kategori</strong> : <strong>
        <?php
        $tagsqry = $mysqli->query("SELECT tags.caption AS Caption FROM pelaporan_tags 
                                   INNER JOIN tags ON tags.id=pelaporan_tags.id_tags 
                                   WHERE pelaporan_tags.id_pelaporan=".$rows['id']);
        while ($tagsrows = $tagsqry->fetch_array(MYSQLI_BOTH)) {
            echo $tagsrows['Caption'].',';
        }
        ?>
        </strong>
      </div>

        <strong style="color:#ff7788">Lokasi</strong> : <strong>
        <?php
        $lokasiqry = $mysqli->query("SELECT lokasi.nama_lokasi AS NamaTempat FROM pelaporan_lokasi 
                                   INNER JOIN lokasi ON lokasi.id = pelaporan_lokasi.id_lokasi 
                                   WHERE pelaporan_lokasi.id_pelaporan = ".$rows['id']);
        try {
          while ($lokasirows = $lokasiqry->fetch_array(MYSQLI_BOTH)) {
            echo $lokasirows['NamaTempat'].',';
          }
        }catch(Exception $e){
          var_dump($e->getMessage());
        }
        
        ?>
        </strong>
        <br/>
        <strong style="color:#77ff88">Status</strong> : <strong id="score<?=$rows['id']?>"><?php 
        if ( $rows['score_total'] > 200)
          echo '<strong style="color:red">sangat mengkhawatirkan</strong>'; 
        elseif ( $rows['score_total'] >= 100 AND $rows['score_total'] <= 200)
          echo '<strong style="color:yellow">mengkhawatirkan</strong>'; 
        else
          echo '<strong style="color:orange">cukup mengkhawatirkan</strong>'; 
        ?></strong>
        
      </div>
  </div>
  <?php 
    }
  ?>
</div>
</div>
<?php
} //END Year, Month
?>
<nav id="right-content" class="col-md-offset-1 right-content bs-docs-sidebar" > 
        <span class='RightColumn' style="width:50%;margin-left:12%">Waktu</span >
        <hr />
          <ul class="nav bs-docs-sidenav">
            <?php 
              foreach ($waktu_arr as $w) {
                $waktu = explode('-', $w);
                echo '<li><a href="#'.$w.'">'.general::getBulan($waktu[0]).' '.$waktu[1].'</a></li>';  
              }
            ?>
          </ul>
            
          <!--
            <input type="radio" id="filter" name="Plafon" value="true" >&nbsp;Terbaru<br/>
            <input type="radio" id="filter" name="Plafon" value="Kas">&nbsp;Prioritas Tertinggi<br/>
            <input type="checkbox" id="filterSudahDitangani" name="Plafon" value="Status Kas Titipan">&nbsp;Sudah Ditangani<br/>
          -->
        
</nav> <!-- /col -->

</div> <!--end main-content-->
<script>     
  function initialize() {
      //popup modal bawaan boostrap 3
      $('#DetailModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget); // Button that triggered the modal
        var id_plpran = button.data('id_plpran'); // Extract info from data-* attributes
        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        var detail_pelaporan;
        var modal = $(this);
   
        $.get("act.php?s=SHARED_aspirasi&m=1&c=\"get_detail_pelaporan.php?id_plpran="+id_plpran+"\"", function(d){
          //alert(d);
          try{
            detail_pelaporan = JSON.parse(d);
            //alert(detail_pelaporan.pesan);
            modal.find('.modal-title').text(detail_pelaporan.no_telp );
            modal.find('.detail-timestamp').text(detail_pelaporan.timestamp); 
            modal.find('#sms-pelaporan').text(detail_pelaporan.pesan);
            modal.find('.modal-body #tags-list').val(detail_pelaporan.tags);
            modal.find('#is-spam').attr('href', "act.php?ec=<?php echo $_GET[ec].'&mhash='. $_GET[mhash];?>&s=SHARED_aspirasi&m=1&c=\"aksi_is_spam.php?id_plpran="+id_plpran+"\"&act=tambah_tanda");
            modal.find('#is-public').attr('href', "act.php?ec=<?php echo $_GET[ec].'&mhash='. $_GET[mhash];?>&s=SHARED_aspirasi&m=1&c=\"aksi_is_public.php?id_plpran="+id_plpran+"\"&act=tambah_tanda");

            modal.find('#lokasi').val("Palangkaraya");
          }catch(e) {
            alert(e.message);
          }  
        });     
        
      });

       // Trigger downloadUrl at an interval
      intervalId = setInterval(triggerDownload, 5000);
  }

  function triggerDownload(){

    
    //alert(last_timestamp);
    $.get("act.php?s=SHARED_aspirasi&m=1&c=\"refresh_pelaporan_timestamp.php?timestamp="+last_timestamp+"\"", function(d){
        //alert(typeof(d));
        //alert("d: " + d);

          //alert(last_timestamp);
        
        if (d !=  '' && d.length < 30){
          
          $.get("act.php?s=SHARED_aspirasi&m=1&c=\"refresh_pelaporan.php?timestamp="+last_timestamp+"\"", function(data){
           // alert("Data: " + data);
            //var arr = data.match(/\nlast_timestamp=(.*)$\n/g) || [""]; //could also use null for empty value
            //Regex r = new Regex("=([^;]*);");
            //Match m = r.Match(data);
            //var arr = data.match("=([^;]*);"); //could also use null for empty value
            //while (m.Success) {
            //    string match = arr[0];
                // match should be the text between the '=' and the ';'.
            //    last_timestamp = match;

            //}
            $("#PelaporanCard-1").prepend($(data).hide().fadeIn('slow'));  
          });

          last_timestamp = d;            
          //alert(last_timestamp);

        }else {
          if (d.length > 30)
            alert(d);
        }
    });
  }
</script>
