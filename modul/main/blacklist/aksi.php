<?php
session_start();
//NOTE: perlu anti injection dan validasi
include "lib/validation/cek_format_angka.php";
include "lib/redirect.php";

//Data-data form di sini
include_once 'conf.php';

use lib\general;
use lib\security;

//ambil nilai enkripsi
//TODO:BUG:hmm bug di defined_session.php masih aza terjadi SESSKEY gak kebaca jd panggil manual 'key.'$_SESSION[SESSID]
if (!empty($_SESSION['key.'.$_SESSION[SESSID]])) {
	$path = general::parsing_url(security::get_ec($_GET['ec']));
	if (strncmp($path['sid'], $_SESSION[SESSID],32 ) == 0)
	  $salt_valid = true;
	else
	  $salt_valid = false;
}

//echo session_id().':::'.$secure_session;
if (session_id() != security::get_secured_session_id())
	$hash_session_valid = false;
else
	$hash_session_valid = true;


//echo general::get_login() .' == '. $salt_valid .' == '. $hash_session_valid;
//echo "<br/>";
//print_r($_POST);

//ambil nama table database
$private_table_name = $conf['private_table_name'];

//ambil path untuk foto 
$conf_image_path = $conf['image_path'];

//ambil data khusus item (input)
$items = $conf['inputs'];

//ambil index key
$keys = array_keys($items);


function is_duplikat_key($private_table_name, $key, $val, $id=null) {

//	echo "SELECT * FROM $private_table_name WHERE $key='$val'";

	$check = $GLOBALS['mysqli']->query("SELECT * FROM $private_table_name WHERE $key='$val'")->fetch_array(MYSQLI_BOTH);

	//echo '$check[username]='.$check['username'].':'.$check['id'].'<br/>';
	$check = $check;
	if ($val == $check[$key] ) {
		
		if ($id != null) {
			//double cek apa id juga sama ato gak
			if ($id != $check['id'])
				return true;
		}else
			return true;
	}

return false;
}


function redir($loc) {
	header('location:./'.$loc);
}

function redir_comeback($loc) {
	$kembali = isset($_SERVER['HTTP_REFERER'])? $_SERVER['HTTP_REFERER'] : "";
	$kembali = explode('?', $kembali)[1];
	redir('?'.$kembali.$loc);
}

//cek keabsahansemua pengecekan
if( general::get_login() != 1 OR ! $salt_valid OR ! $hash_session_valid){

	include "modul/error_page/404.php";
	return;

}else{

	include "lib/fungsi_thumb.php";


	$unit = $_GET['unit'];
	$module = $_GET['module'];
	$parent = $_GET['parent'];
	$act = $_GET['act']; 

	$redirect = "index.php?mhash=$_GET[mhash]&ec=$_GET[ec]";
	$redirect_act = "index.php?unit=$unit&module=$module&act=$act";
	$path_upload  = $conf['image_path'];//../../Pengunjung/gambar/foto_siswa/';








	// Menghapus data
	if ($act=='hapus'){

		//cek file gambar ada atau tidak
		//$data=@mysql_fetch_array(mysql_query("SELECT lampiran_download FROM $private_table_name WHERE $conf[inputs_default_id]='$_GET[id]'"));
		  

		//TODO: BUG : Jika raise error maka data akan tetap di hapus :(  
		//jika ada gambarnya
		/*if ($data['lampiran_download']!=''){
				
			$filename = $data['lampiran_download'];

			if (file_exists($path_upload.$filename)) {

				unlink($path_upload.$filename);
				echo 'File '.$filename.' berhasil dihapus';

			} else {

				echo 'File '.$filename.' GAGAL dihapus';

			}

			mysql_query("DELETE FROM $private_table_name WHERE $conf[inputs_default_id] = '$_GET[id]'");

		
			//TODO: HAPUS file gambar juga cuy

		}else{
			<h3>Data berhasil dihapus</h3>

			<script>location.href='<?php echo $redirect ?>';</script>

		*/
		//mysql_query("DELETE FROM $private_table_name WHERE $conf[inputs_default_id] = '$_GET[id]'");

		$ret = $mysqli->query("DELETE FROM $private_table_name WHERE $conf[inputs_default_id] = '".
								security::anti_injection($mysqli, $_GET['id'])."'");//TODO:BUG: misal tambah string ' a' $ret->error gak menghasilkan error apa2...aneh...
		
		if ($ret->error){
			redir($redirect.'&status=error+sistem+internal+data+gagal+ditambahkan/ubah');
			exit(0);
		}else {
			//echo '<h3>Data berhasil ditambah</h3>';	
			redir($redirect.'&status=Data+berhasil+dihapus');
			exit(0);
		}

		//}

		//echo html redirect js
		

	}

	// ***** Input/tambah pegawai
	elseif ($act=='tambah'){

			//VALIDASI...
			$validation = true; 

			//menyimpan nama kolom
			$build_query_column = "";
			//menyimpan nilai bersesuaian dgn kolomnya
			$build_query_value = "";

			//default format (ID harus autoincrement)
			$default_format = "INSERT INTO $private_table_name ($conf[inputs_default_id] %s)  VALUES( null %s)";

			$counter = 0;
			/**
				TODO: tambah key 'uniq' tuk kolom yang tidak boleh memiliki data yang sama
			*/
			foreach ($items as $key => $value) {

				//item pertama yaitu kolom 'id' diabaikan karena dah ada di $default_format
				if ( ! $counter) {
					$counter++;
					continue;
				}

				//jgn simpan data
				if ($value['dont_save'] == true)
					continue;


				//NOTE: panggil statement terpisah yg mengevaluasi tipe input dan tipe data
				//TODO: kyknya perlu pake fungsi, coz misal tuk menghapus spasi d NIP perlu preprosessor terlebih dahulu yg user defined
				include 'generate_query.php';

				//stop loop jika ada field yang gak valid
				if ( ! $validation ) {
					redir_comeback($kembali.'&status=Cek+ulang+data+masukan+anda+!');
					exit(0); 
				}




				//cek nip sama
				$checker = 'no_telp';
				if ($key == $checker) {
						
						if ( is_duplikat_key($private_table_name, $checker, $_POST[ $key ]) ) {
							//echo '<p style="color:red">Kategori sudah terdaftar sebelumnya !!!</p>';
							//redirect($kembali, 2000);
							redir_comeback($kembali.'&status=No.Telp+sudah+terdaftar+sebelumnya+!!!');
							exit(0);
						}
				}



				
			}


			

			//copy query data ke query format
			$final_query =  sprintf($default_format, $build_query_column ,$build_query_value); 
			//echo $final_query;

			$ret = $mysqli->query($final_query);//TODO:BUG: misal tambah string ' a' $ret->error gak menghasilkan error apa2...aneh...
			
			if ($ret->error){
				redir($redirect.'&status=error+sistem+internal+data+gagal+ditambahkan');
				exit(0);
			}else {
				//echo '<h3>Data berhasil ditambah</h3>';	
				redir($redirect.'&status=Data+berhasil+ditambah');
				exit(0);
			}
			redirect($redirect, 1000);




	}

// ***** Update pegawai
	elseif ($act=='edit'){


		//VALIDASI...
			$validation = true; 

			//menyimpan nama kolom dan nilai (kolom = nilai,)
			$build_query = "";
			
			//default format (ID harus autoincrement)
			$default_format = "UPDATE $private_table_name SET %s WHERE $conf[inputs_default_id] = '".$_POST[$conf['inputs_default_id']]."'";


			$counter = 0;
			//$size = count($items); 
			
			/**
				TODO: tambah key 'uniq' tuk kolom yang tidak boleh memiliki data yang sama
			*/
			foreach ($items as $key => $value) {

				//item pertama yaitu kolom 'id' diabaikan karena dah ada di $default_format
				if ( ! $counter) {
					$counter++;
					continue;
				}else
					$counter++;



				//jgn simpan data
				if ($value['dont_save'] == true)
					continue;


				//NOTE: panggil statement terpisah yg mengevaluasi tipe input dan tipe data
				include 'generate_query.php';

				//stop loop jika ada field yang gak valid
				if ( ! $validation ){
					redir_comeback($kembali.'&status=Cek+ulang+data+masukan+anda+!');
					exit(0);
				}


				$checker = 'no_telp';
				if ($key == $checker) {
						
						if ( is_duplikat_key($private_table_name, $checker, $_POST[ $key ], $_POST['id']) ) {
							//echo '<p style="color:red">Kategori sudah terdaftar sebelumnya !!!</p>';
							//redirect($kembali, 2000);
							redir_comeback($kembali.'&status=No.Telp+sudah+terdaftar+sebelumnya+!!!');
							exit(0);
						}
				}


			}






			//copy query data ke query format
			$final_query =  sprintf($default_format, $build_query);
			//echo $final_query;

			$ret = $mysqli->query($final_query);//TODO:BUG: misal tambah string ' a' $ret->error gak menghasilkan error apa2...aneh...
			
			if ($ret->error){
				redir($redirect.'&status=error+sistem+internal+data+gagal+diubah');
				exit(0);
			}else {
				//echo '<h3>Data berhasil ditambah</h3>';	
				redir($redirect.'&status=Data+berhasil+diubah');
				exit(0);
			}
			redirect($redirect, 1000);






	}


}
?>





