<?php
	/**
		NOTE: 
		-bedanya option public dan hide_when_login adalah :
			public = true maka akan ditampilkan saat belum login maupun sudah login
			public = false maka TIDAK akan ditampilkan saat belum login tp ditampilkan saat sudah login
			hide_when_login = true maka akan ditampilkan saat belum login tp akan disembunyikan saat sudah login
			hide_when_login = false maka akan DISEMBUNYIKAN saat belum login tp akan dimunculkan saat sudah login
	*/
	$conf = array(
        //caption di tombol-tombol
        'caption' => 'Blacklist',
        //title table
        'table_title' => 'Blacklist',
        //untuk keperluan upload, boleh dikosongkan jika gak da upload
        'image_path' => 'public/lampiran_download/',
        //nama table
        'private_table_name' => 'blacklist_spammer',
        //query ordering/sorting
        'query_order_by' => 'id',//SELECT blablabla ORDER BY id
        'query_adsc' => 'DESC',//SELECT blablabla ORDER BY id DESC
        //aksi ke submodule

        'aksi_url' => "act.php?s=$path[module]&m=1&c=\"aksi.php?\"&$_SERVER[QUERY_STRING]",
        
        //id default di dalam tabel
        'inputs_default_id' => 'id',
        'inputs_rows_name' => 'no_telp',
        
        //ITEMS yang ditampilkan di tabel (view/tambah/edit)
        'inputs' => array(
 
        	//Menyimpan nilai hidden
			'id' => array(
				'reference' => 'id',//dipanggil sebagai parameter $_GET[reference]
				'attributes' => '',//misal "style='margin-top:20px' onclick=''"
	            'type' => 'hidden',//tipe <input type=hidden />
	            'no_search' => true,//jangan lakukan pencarian
	       	),

			'no_telp' => array(
				'caption' => 'Nomor Telpon',
				'attributes' => 'maxlength=15 size=15 ',
	            'type' => 'text',
	            'show_in_table' => true,// tampil d kolom tabel
	            'info' => 'Nomor Telpon Pelapor yg diblokir'
	       	),

			//Muncul sebagai masukkan
            'keterangan' => array(
				'caption' => 'Keterangan',
				'attributes' => 'maxlength=1000 size=1000 ',
	            'type' => 'text',
	            'show_in_table' => true,// tampil d kolom tabel
	            'info' => 'keterangan kenapa pelapor ini perlu diblokir'
	       	),

			'timestamp' => array(
				'caption' => 'Waktu Input Data',
				'attributes' => 'maxlength=1000 size=1000 ',
	            'type' => 'hidden',
	            'dont_save'=>true,
	            'show_in_table' => true,// tampil d kolom tabel
	            'info' => ''
	       	),

	       		       	
	       	
/*
            'jumlah_uang' => array(
				'caption' => 'Jumlah Uang',
				'attributes' => 'maxlength=15 size=15 ',
	            'type' => 'text',
	            'show_in_table' => true,// tampil d kolom tabel
	            'info' => 'isikan nomor telpon, boleh 08 atau +628'
	       	),

	       
	       	'tgl_permintaan' => array(
	       		'caption' => 'Tanggal Permintaan',
				//TODO: Tambah fitur javascript yang mendetect class tanggal yg nantinya menampilkan popup js jquery date 
				'attributes' => '',
	            'type' => 'text',

				'info' => '----- format : hari/bln/thn misalnya 10/04/1992',
	            'special' => array(
	            		'type' => 'date',
	            		'delimiter' => '/',
	            		'db_delimiter' => '-'
	            	),

	            'show_in_table' => true// tampil d kolom tabel
	       	),
			*/
        )
  

    );

?>