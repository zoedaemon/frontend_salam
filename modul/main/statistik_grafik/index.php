<?php

function tampildata($mysqli, $table, $get_data, $where, $order_by){
    $W = $where?('WHERE '.$where):'';
    $Q = "SELECT $get_data FROM $table $W ORDER BY $order_by";
    $q=$mysqli->query($Q);
    while ($row=$q->fetch_array(MYSQLI_BOTH)) 
        $data[]=$row;
        //echo $Q.' -> '.$table.':'.$get_data.':'.$where.':'.$order_by;
    echo $mysqli->error;
    return $data;
}

//config form dll
include 'conf.php';

//javascript tuk popup menu   
include 'plugin/form/js_popup.php';

//tuk operasi view tambah dan edit  
include 'plugin/form/popup_edit.php';  

//tuk operasi view tambah dan edit 
include 'plugin/form/render_custom_file_upload.php';

//ambil string judul tabel view 
$table_title = $conf['table_title'];
$caption = $conf['caption'];

//ambil nama table database
$private_table_name = $conf['private_table_name'];
//ambil path untuk foto 
$conf_image_path = $conf['image_path'];
//ambil data khusus item
$inputs = $conf['inputs'];
//ambil index key
$keys = array_keys($inputs);

//KHUSUS QUERY SETTING
//urutkan berdasarkan
$query_order_by = $conf['query_order_by'];
$query_adsc = $conf['query_adsc'];

//active column adalah inputs yang bukan hidden ato yang diperlukan dlm operasi tambah dan edit
$index_active_column_start = 1;
$index_active_column_end = sizeof( $inputs );

$aksi = $conf['aksi_url'];

//query default, tp nanti akan dimanipulasi jika ada pencarian ato grouping 
$query = "SELECT * FROM $private_table_name ";

    //formtarget='_blank' ???
    //print template
?>

<h1  class="title-separator"><?=$table_title?></h1>
<!-- ini div bantuan karena left-content-fixed adalah fixed-->

<div class='row-fluid col-md-10 right-content-helper' >
  <div  class=" crud-container" style="margin:5px 0px;">

    <div class="widget-box ">
          <div class="widget-content ">

            <table class='table table-bordered table-striped with-check'>
            <thead>
            <tr>    
              <th>No</th>
              <th>Status Pelaporan</th>
              <th>Jumlah</th>
            </tr>
            </thead>
            <tbody>
                <?php

                    $setget = tampildata($mysqli, 'pelaporan', 'count(*) AS counter', "", 'id');

                    echo "<tr>";

                    echo "<td>1</td>";

                    echo "<td>Pelaporan Keseluruhan</td>";

                    $MhsB = $setget[0]['counter'];
                    echo "<td>".($MhsB)."</td>";

                    echo "</tr>";
                    

                    $setget = tampildata($mysqli, 'pelaporan', 'count(*) AS counter', "is_public=1 ", 'id');

                    echo "<tr>";

                    echo "<td>2</td>";

                    echo "<td>Pelaporan ditampilkan Public</td>";

                    echo "<td>".($MhsP = $setget[0]['counter'])."</td>";

                    echo "</tr>";


                    $setget = tampildata($mysqli, 'pelaporan', 'count(*) AS counter', "is_spam=TRUE", 'id');

                    echo "<tr>";

                    echo "<td>3</td>";

                    echo "<td>Pelaporan yg spam</td>";

                    echo "<td>".($MhsV = $setget[0]['counter'])."</td>";

                    echo "</tr>";



                ?>
            </tbody>
            </table>
            <script type="text/javascript">
              </script>
            <div id="barchart_terdaftar_stat" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
   </div>
  </div>



    <hr/><hr/><center><h2/>Data Pelaporan/Aspirasi Berdasarkan Prioritas</h2></center>
    <div class="widget-box ">
          <div class="widget-content ">

            <table class='table table-bordered table-striped with-check'>
            <thead>
            <tr>    
              <th>No</th>
              <th>Pelaporan</th>
              <th>Jumlah</th>
            </tr>
            </thead>
            <tbody>
                <?php

                    $setget = tampildata($mysqli, 'pelaporan', 'count(*) AS counter', " score_total < 101  AND is_spam = 0", 'id');

                    echo "<tr>";

                    echo "<td>1</td>";

                    echo "<td>Cukup Mengkhawatirkan</td>";

                    $CM = $setget[0]['counter'];
                    echo "<td>".($CM)."</td>";

                    echo "</tr>";
                    

                    $setget = tampildata($mysqli, 'pelaporan', 'count(*) AS counter', "score_total > 100 AND  score_total < 201  AND is_spam = 0", 'id');

                    echo "<tr>";

                    echo "<td>2</td>";

                    echo "<td>Mengkhawatirkan</td>";

                    echo "<td>".($M = $setget[0]['counter'])."</td>";

                    echo "</tr>";


                    $setget = tampildata($mysqli, 'pelaporan', 'count(*) AS counter', " score_total > 200 AND is_spam = 0 ", 'id');

                    echo "<tr>";

                    echo "<td>3</td>";

                    echo "<td>Sangat Mengkhawatirkan</td>";

                    echo "<td>".($SM = $setget[0]['counter'])."</td>";

                    echo "</tr>";



                ?>
            </tbody>
            </table>
            <script type="text/javascript">
                function initialize () {
                  $('#barchart_terdaftar_stat').highcharts({
                      chart: {
                          type: 'column'
                      },
                      title: {
                          text: 'Data Pelaporan/Aspirasi Keseluruhan'
                      },
                      subtitle: {
                          text: ''
                      },
                      xAxis: {
                          categories: ["Pelaporan Keseluruhan", "Pelaporan ditampilkan Public", "Pelaporan yg spam"],
                          crosshair: true
                      },
                      yAxis: {
                          min: 0,
                          title: {
                              text: 'Jumlah Peserta'
                          }
                      },
                      tooltip: {
                          headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                          pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                              '<td style="padding:0"><b>{point.y:1f}</b></td></tr>',
                          footerFormat: '</table>',
                          shared: true,
                          useHTML: true
                      },
                      plotOptions: {
                          column: {
                              pointPadding: 0.2,
                              borderWidth: 0,
                              dataLabels: {
                                  enabled: true,
                                  align: 'center',
                                  x: 0,
                                  y: 35,
                                  style: {
                                      fontSize: '16px',
                                      fontFamily: 'Verdana, sans-serif'
                                  }
                              }
                          }
                      },
                      series: [{
                          name: "Total Pelaporan",
                          data: [<?php 
                                  echo $MhsB.','.$MhsP.','.$MhsV;
                          ?>]
                      }]
                  });

                  $('#barchart_terdaftar_stat-2').highcharts({
                      chart: {
                          type: 'column'
                      },
                      title: {
                          text: 'Data Pelaporan/Aspirasi Keseluruhan'
                      },
                      subtitle: {
                          text: ''
                      },
                      xAxis: {
                          categories: ["Cukup Mengkhawatirkan", "Mengkhawatirkan", "Sangat Mengkhawatirkan"],
                          crosshair: true
                      },
                      yAxis: {
                          min: 0,
                          title: {
                              text: 'Jumlah Peserta'
                          }
                      },
                      tooltip: {
                          headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                          pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                              '<td style="padding:0"><b>{point.y:1f}</b></td></tr>',
                          footerFormat: '</table>',
                          shared: true,
                          useHTML: true
                      },
                      plotOptions: {
                          column: {
                              pointPadding: 0.2,
                              borderWidth: 0,
                              dataLabels: {
                                  enabled: true,
                                  align: 'center',
                                  x: 0,
                                  y: 35,
                                  style: {
                                      fontSize: '16px',
                                      fontFamily: 'Verdana, sans-serif'
                                  }
                              }
                          }
                      },
                      series: [{
                          name: "Total Pelaporan",
                          data: [<?php 
                                  echo $CM.','.$M.','.$SM;
                          ?>]
                      }]
                  });

                }
              </script>
            <div id="barchart_terdaftar_stat-2" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
    </div>
  </div>
</div>
</div>
  