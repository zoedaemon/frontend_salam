<?php

//NOTE: perlu anti injection dan validasi
include_once "lib/general.php";
include "lib/validation/cek_format_angka.php";
include "lib/redirect.php";

//Data-data form di sini
include_once 'conf.php';

use lib\general;


//ambil nama table database
$private_table_name = $conf['private_table_name'];

//ambil path untuk foto 
$conf_image_path = $conf['image_path'];

//ambil data khusus item (input)
$items = $conf['inputs'];

//ambil index key
$keys = array_keys($items);



function is_duplikat_nip($nip, $id=null) {


	$check = mysql_fetch_array(mysql_query("SELECT * FROM pegawai WHERE nip = '$nip'"));

	if ($nip == $check['nip'] AND $check['nip'] != -1 ) {
		
		if ($id != null) {
			if ($id != $check['id'])
				return true;
		}else
			return true;
	}
return false;
}




function is_duplikat_username($username, $id=null) {


	$check = mysql_fetch_array(mysql_query("SELECT * FROM pegawai WHERE username='$username'"));

	//echo '$check[username]='.$check['username'].':'.$check['id'].'<br/>';
	if ($username == $check['username'] ) {
		
		if ($id != null) {
			if ($id != $check['id'])
				return true;
		}else
			return true;
	}
return false;
}



if (empty($_SESSION['username']) AND empty($_SESSION['passuser'])){

  echo "<link href='style.css' rel='stylesheet' type='text/css'>
 <center>Untuk mengakses Halaman Administrator, Anda harus login <br>";

  echo "<a href=index.php><b>LOGIN</b></a></center>";

}
else{


	include "lib/fungsi_thumb.php";


	$unit = $_GET['unit'];
	$module = $_GET['module'];
	$parent = $_GET['parent'];
	$act = $_GET['act']; 

	$redirect = "index.php?unit=$unit&module=$module&parent=$parent";
	$redirect_act = "index.php?unit=$unit&module=$module&act=$act";
	$path_upload  = $conf['image_path'];//../../Pengunjung/gambar/foto_siswa/';








	// Menghapus data
	if ($act=='hapus'){

		//cek file gambar ada atau tidak
		$data=@mysql_fetch_array(mysql_query("SELECT lampiran_download FROM $private_table_name WHERE $conf[inputs_default_id]='$_GET[id]'"));
		  

		 //TODO: BUG : Jika raise error maka data akan tetap di hapus :(  
		//jika ada gambarnya
		if ($data['lampiran_download']!=''){
				
			$filename = $data['lampiran_download'];

			if (file_exists($path_upload.$filename)) {

				unlink($path_upload.$filename);
				echo 'File '.$filename.' berhasil dihapus';

			} else {

				echo 'File '.$filename.' GAGAL dihapus';

			}

			mysql_query("DELETE FROM $private_table_name WHERE $conf[inputs_default_id] = '$_GET[id]'");

		
			//TODO: HAPUS file gambar juga cuy

		}else{

			mysql_query("DELETE FROM $private_table_name WHERE $conf[inputs_default_id] = '$_GET[id]'");

		}

		//echo html redirect js
		?>

			<h3>Data berhasil dihapus</h3>

			<script>location.href='<?php echo $redirect ?>';</script>
		<?php

	}

	// ***** Input/tambah pegawai
	elseif ($act=='tambah'){

			//VALIDASI...
			$validation = true; 

			//menyimpan nama kolom
			$build_query_column = "";
			//menyimpan nilai bersesuaian dgn kolomnya
			$build_query_value = "";

			//default format (ID harus autoincrement)
			$default_format = "INSERT INTO $private_table_name ($conf[inputs_default_id], status %s)  VALUES( null, 'P' %s)";


			$counter = 0;
			/**
				TODO: tambah key 'uniq' tuk kolom yang tidak boleh memiliki data yang sama
			*/
			foreach ($items as $key => $value) {

				//item pertama yaitu kolom 'id' diabaikan karena dah ada di $default_format
				if ( ! $counter) {
					$counter++;
					continue;
				}

				//jgn simpan data
				if ($value['dont_save'] == true)
					continue;


				//NOTE: panggil statement terpisah yg mengevaluasi tipe input dan tipe data
				//TODO: kyknya perlu pake fungsi, coz misal tuk menghapus spasi d NIP perlu preprosessor terlebih dahulu yg user defined
				include 'generate_query.php';

				//stop loop jika ada field yang gak valid
				if ( ! $validation ) {
					$kembali = isset($_SERVER['HTTP_REFERER'])? $_SERVER['HTTP_REFERER'] : "";
					redirect($kembali, 2000);
					break; 
				}




				//cek nip sama
				if ($key == 'nip') {
						
						if ( is_duplikat_nip($_POST[ $key ]) ) {
							echo '<p style="color:red">NIP sudah terdaftar sebelumnya !!!</p>';
							$kembali = isset($_SERVER['HTTP_REFERER'])? $_SERVER['HTTP_REFERER'] : "";
							redirect($kembali, 2000);
							exit(0);
						}
				}



				if ($key == 'username') {
						
						if ( is_duplikat_username($_POST[ $key ]) ) {
							echo '<p style="color:red">NIP sudah terdaftar sebelumnya !!!</p>';
							$kembali = isset($_SERVER['HTTP_REFERER'])? $_SERVER['HTTP_REFERER'] : "";
							redirect($kembali, 2000);
							exit(0);
						}
				}



				
			}




			//copy query data ke query format
			$final_query =  sprintf($default_format, $build_query_column ,$build_query_value); 
			//echo $final_query;

			mysql_query($final_query);
			echo mysql_error(); 

			echo '<h3>Data berhasil ditambah</h3>';	
			redirect($redirect, 1000);




	}

// ***** Update pegawai
	elseif ($act=='edit'){


		//VALIDASI...
			$validation = true; 

			//menyimpan nama kolom dan nilai (kolom = nilai,)
			$build_query = "";
			
			//default format (ID harus autoincrement)
			$default_format = "UPDATE $private_table_name SET %s WHERE $conf[inputs_default_id] = '".$_POST[$conf['inputs_default_id']]."'";


			$counter = 0;
			//$size = count($items); 
			
			/**
				TODO: tambah key 'uniq' tuk kolom yang tidak boleh memiliki data yang sama
			*/
			foreach ($items as $key => $value) {

				//item pertama yaitu kolom 'id' diabaikan karena dah ada di $default_format
				if ( ! $counter) {
					$counter++;
					continue;
				}else
					$counter++;



				//jgn simpan data
				if ($value['dont_save'] == true)
					continue;


				//NOTE: panggil statement terpisah yg mengevaluasi tipe input dan tipe data
				include 'generate_query.php';

				//stop loop jika ada field yang gak valid
				if ( ! $validation )
					break; 


				if ($key == 'nip' ) {
						
						//echo "$_POST[$key], $_POST[id]";
						if ( is_duplikat_nip($_POST[ $key ], $_POST['id']) ) {
							echo '<p style="color:red">NIP sudah terdaftar sebelumnya !!!</p>';
							$kembali = isset($_SERVER['HTTP_REFERER'])? $_SERVER['HTTP_REFERER'] : "";
							redirect($kembali, 2000);
							exit(0);
						}
				}


				if ($key == 'username') {

						//echo "$_POST[$key], $_POST[id]";
						if ( is_duplikat_username($_POST[ $key ], $_POST['id']) ) {
							echo '<p style="color:red">Username sudah terdaftar sebelumnya !!!</p>';
							$kembali = isset($_SERVER['HTTP_REFERER'])? $_SERVER['HTTP_REFERER'] : "";
							redirect($kembali, 2000);
							exit(0);
						}
				}

			}






			//copy query data ke query format
			$final_query =  sprintf($default_format, $build_query);
			//echo $final_query;

			mysql_query($final_query);
			echo mysql_error(); 

			echo '<h3>Data berhasil diperbaharui</h3>';
			//echo $default_format;
			redirect($redirect, 1500);




	}


}
?>





