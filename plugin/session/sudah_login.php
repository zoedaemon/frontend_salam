<?php
    /**
        Fungsi untuk pengecekan sesion sudah login atau belum (true sudah ; false belum)
    */
    function sudah_login() {
        return (isset($_SESSION[SESSLOGIN]) AND  ! empty($_SESSION[SESSLOGIN] )  ? $_SESSION[SESSLOGIN] : false )  ; 
    }
?>