<?php

include "define.php";//atau include "app/plugin/hak_akses/define.php";

include "plugin/menu/menu.php";

//Hak akses Tambahan
define(PRINT_DATA, 32, true);//TODO : coba DELETE << 1

//namespace hak akses
use lib\zHakAkses;


//ObjHakAkses = zHakAkses::init(domain, usr, pass, db);
//ObjHakAkses = zHakAkses::init(db);


zHakAkses::role_add(ADMIN, SUPER_ADMIN);//super Admin 
zHakAkses::role_add(AUDITOR, VIEW);//audit (hnya bisa melihat, tidak bisa tambah, edit, hapus)
//zHakAkses::role_add(SUPERVISOR, VIEW);//default bisa melihat semua, SUPER ADMIN versi view saja
zHakAkses::role_add(SKPD);//skpd dengan smua nama
zHakAkses::role_add(PENGUNJUNG);//skpd dengan smua nama
zHakAkses::role_add(MHS);//super Admin 


$resource = "member-home";
zHakAkses::resource_add(MHS, $resource, VIEW);

$resource = "member-profile";
zHakAkses::resource_add(MHS, $resource, VIEW | INSERT | EDIT | DELETE);
zHakAkses::resource_add(MHS, $resource."-aksi", VIEW |  EDIT ); 

$resource = "setting-ubah_password";
zHakAkses::resource_add(MHS, $resource, VIEW | EDIT );
$resource = "setting-ubah_password-aksi";
zHakAkses::resource_add(MHS, $resource, VIEW | EDIT );


$resource = "si-berita_view";
zHakAkses::resource_add(MHS, $resource, VIEW);
zHakAkses::resource_add(PENGUNJUNG, $resource, VIEW);

//////////// 
$resource = "si-home";
zHakAkses::resource_add(SKPD, $resource, VIEW); 
zHakAkses::resource_add(PENGUNJUNG, $resource, VIEW);

$resource = "si-pegawai";
zHakAkses::resource_add(SKPD, $resource, VIEW | INSERT | EDIT | DELETE); 
zHakAkses::resource_add(SKPD, $resource."-aksi", VIEW | INSERT | EDIT | DELETE); 

$resource = "si-pgw_pendidikan_terakhir";
zHakAkses::resource_add(SKPD, $resource, VIEW | INSERT | EDIT | DELETE); 
zHakAkses::resource_add(SKPD, $resource."-aksi", VIEW | INSERT | EDIT | DELETE); 

$resource = "si-pgw_riwayat_golongan";
zHakAkses::resource_add(SKPD, $resource, VIEW | INSERT | EDIT | DELETE); 
zHakAkses::resource_add(SKPD, $resource."-aksi", VIEW | INSERT | EDIT | DELETE); 

$resource = "si-pgw_riwayat_jabatan";
zHakAkses::resource_add(SKPD, $resource, VIEW | INSERT | EDIT | DELETE); 
zHakAkses::resource_add(SKPD, $resource."-aksi", VIEW | INSERT | EDIT | DELETE); 

$resource = "si-pgw_data_dokter";
zHakAkses::resource_add(SKPD, $resource, VIEW | INSERT | EDIT | DELETE); 
zHakAkses::resource_add(SKPD, $resource."-aksi", VIEW | INSERT | EDIT | DELETE); 

$resource = "si-pgw_data_guru";
zHakAkses::resource_add(SKPD, $resource, VIEW | INSERT | EDIT | DELETE); 
zHakAkses::resource_add(SKPD, $resource."-aksi", VIEW | INSERT | EDIT | DELETE); 

$resource = "si-pgw_riwayat_diklatfung";
zHakAkses::resource_add(SKPD, $resource, VIEW | INSERT | EDIT | DELETE); 
zHakAkses::resource_add(SKPD, $resource."-aksi", VIEW | INSERT | EDIT | DELETE); 

$resource = "si-pgw_riwayat_diklatstruk";
zHakAkses::resource_add(SKPD, $resource, VIEW | INSERT | EDIT | DELETE); 
zHakAkses::resource_add(SKPD, $resource."-aksi", VIEW | INSERT | EDIT | DELETE); 


$resource = "laporan-data";
zHakAkses::resource_add(SKPD, $resource, VIEW ); 
zHakAkses::resource_add(PENGUNJUNG, $resource, VIEW ); 


$resource = "si-dataskpd";
zHakAkses::resource_add(SKPD, $resource, VIEW ); 
zHakAkses::resource_add(PENGUNJUNG, $resource, VIEW ); 
$resource = "si-skpdjabatan";
zHakAkses::resource_add(SKPD, $resource, VIEW ); 
zHakAkses::resource_add(PENGUNJUNG, $resource, VIEW ); 
$resource = "si-skpdnamajabatan";
zHakAkses::resource_add(SKPD, $resource, VIEW ); 
zHakAkses::resource_add(PENGUNJUNG, $resource, VIEW );
$resource = "si-skpdnamajabatan";
zHakAkses::resource_add(SKPD, $resource, VIEW ); 
zHakAkses::resource_add(PENGUNJUNG, $resource, VIEW );
$resource = "si-skpdgolongan";
zHakAkses::resource_add(SKPD, $resource, VIEW ); 
zHakAkses::resource_add(PENGUNJUNG, $resource, VIEW );


$resource = "si-datasekolah";
zHakAkses::resource_add(SKPD, $resource, VIEW ); 
zHakAkses::resource_add(PENGUNJUNG, $resource, VIEW ); 
$resource = "si-daftarsekolah";
zHakAkses::resource_add(SKPD, $resource, VIEW ); 
zHakAkses::resource_add(PENGUNJUNG, $resource, VIEW ); 
$resource = "si-datapendidik";
zHakAkses::resource_add(SKPD, $resource, VIEW ); 
zHakAkses::resource_add(PENGUNJUNG, $resource, VIEW ); 
$resource = "si-listpendidik";
zHakAkses::resource_add(SKPD, $resource, VIEW ); 
zHakAkses::resource_add(PENGUNJUNG, $resource, VIEW ); 


$resource = "si-datatenagapuskesmas";
zHakAkses::resource_add(SKPD, $resource, VIEW ); 
zHakAkses::resource_add(PENGUNJUNG, $resource, VIEW ); 
$resource = "si-datatenagamedis";
zHakAkses::resource_add(SKPD, $resource, VIEW ); 
zHakAkses::resource_add(PENGUNJUNG, $resource, VIEW ); 
$resource = "si-datamedis";
zHakAkses::resource_add(SKPD, $resource, VIEW ); 
zHakAkses::resource_add(PENGUNJUNG, $resource, VIEW ); 

$resource = "si-datamutasi";
zHakAkses::resource_add(SKPD, $resource, VIEW ); 
zHakAkses::resource_add(PENGUNJUNG, $resource, VIEW ); 

$resource = "si-datapensiun";
zHakAkses::resource_add(SKPD, $resource, VIEW ); 
zHakAkses::resource_add(PENGUNJUNG, $resource, VIEW ); 




$resource = "si-grafik";
zHakAkses::resource_add(SKPD, $resource, VIEW ); 
zHakAkses::resource_add(PENGUNJUNG, $resource, VIEW ); 
$resource = "si-jenis_kelamin";
zHakAkses::resource_add(SKPD, $resource, VIEW ); 
zHakAkses::resource_add(PENGUNJUNG, $resource, VIEW ); 
$resource = "si-grafik_sekolah";
zHakAkses::resource_add(SKPD, $resource, VIEW ); 
zHakAkses::resource_add(PENGUNJUNG, $resource, VIEW ); 
$resource = "si-grafik_medis";
zHakAkses::resource_add(SKPD, $resource, VIEW ); 
zHakAkses::resource_add(PENGUNJUNG, $resource, VIEW ); 
$resource = "si-grafik_jumlah_kebutuhan";
zHakAkses::resource_add(SKPD, $resource, VIEW ); 
zHakAkses::resource_add(PENGUNJUNG, $resource, VIEW ); 


//Puskesmas dan sekolah
$resource = "si-sekolah";
zHakAkses::resource_add(SKPD, $resource, VIEW ); 
zHakAkses::resource_add(PENGUNJUNG, $resource, VIEW ); 
$resource = "si-puskesmas";
zHakAkses::resource_add(SKPD, $resource, VIEW ); 
zHakAkses::resource_add(PENGUNJUNG, $resource, VIEW ); 


//pengunjung bisa isi FORM
$resource = "si-form_pendaftaran";
zHakAkses::resource_add(PENGUNJUNG, $resource, VIEW ); 
$resource = "si-form_pendaftaran-aksi";
zHakAkses::resource_add(PENGUNJUNG, $resource, VIEW | INSERT | EDIT | DELETE); 


/*
zHakAkses::resource_add(ADMIN, "si-pegawai", VIEW);
zHakAkses::resource_add(ADMIN, "si-pgw_pendidikan_terakhir", VIEW);

//TODO: buat fungsi yang bisa menambah resource public tuk semua role (no restricted resource) -- array of roles
//profil
zHakAkses::resource_add(ADMIN, "setting-profil", VIEW); 
zHakAkses::resource_add(PEGAWAI, "setting-profil", VIEW); 
zHakAkses::resource_add(GURU, "setting-profil", VIEW); 
zHakAkses::resource_add(SISWA, "setting-profil", VIEW); 

//logout
zHakAkses::resource_add(ADMIN, "setting-logout", VIEW); 
zHakAkses::resource_add(PEGAWAI, "setting-logout", VIEW); 
zHakAkses::resource_add(GURU, "setting-logout", VIEW); 
zHakAkses::resource_add(SISWA, "setting-logout", VIEW); 

//pengaturan - ubah password
zHakAkses::resource_add(ADMIN, "setting-ubah_password", VIEW | EDIT);  
zHakAkses::resource_add(PEGAWAI, "setting-ubah_password", VIEW | EDIT); 
zHakAkses::resource_add(GURU, "setting-ubah_password", VIEW | EDIT); 
zHakAkses::resource_add(SISWA, "setting-ubah_password", VIEW | EDIT); 
//aksi didaftarkan terpisah (supaya lebih aman)
zHakAkses::resource_add(ADMIN, "setting-ubah_password-aksi", VIEW | EDIT);  
zHakAkses::resource_add(PEGAWAI, "setting-ubah_password-aksi", VIEW | EDIT); 
zHakAkses::resource_add(GURU, "setting-ubah_password-aksi", VIEW | EDIT); 
zHakAkses::resource_add(SISWA, "setting-ubah_password-aksi", VIEW | EDIT); 

//ubah setting default
zHakAkses::resource_add(ADMIN, "setting-pengaturan_umum", VIEW | EDIT);  
zHakAkses::resource_add(PEGAWAI, "setting-pengaturan_umum", VIEW); 





//pindah menu
zHakAkses::resource_add(ADMIN, "setting-ganti_profil", VIEW | INSERT | EDIT | DELETE); 
//zHakAkses::resource_add(PEGAWAI, "setting-ganti_profil", VIEW | INSERT | EDIT | DELETE); 

//Data Guru / Pegawai
zHakAkses::resource_add(ADMIN, "si-data_guru", VIEW | INSERT | EDIT | DELETE); 
zHakAkses::resource_add(PEGAWAI, "si-data_guru", VIEW | INSERT | EDIT | DELETE); 
//aksinya
zHakAkses::resource_add(ADMIN, "si-data_guru-aksi", VIEW | INSERT | EDIT | DELETE); 
zHakAkses::resource_add(PEGAWAI, "si-data_guru-aksi", VIEW | INSERT | EDIT | DELETE); 


//Data Siswa
zHakAkses::resource_add(ADMIN, "si-data_siswa", VIEW | INSERT | EDIT | DELETE); 
zHakAkses::resource_add(PEGAWAI, "si-data_siswa", VIEW | INSERT | EDIT | DELETE); 
//aksinya
zHakAkses::resource_add(ADMIN, "si-data_siswa-aksi", VIEW | INSERT | EDIT | DELETE); 
zHakAkses::resource_add(PEGAWAI, "si-data_siswa-aksi", VIEW | INSERT | EDIT | DELETE); 

//Data kelas
zHakAkses::resource_add(ADMIN, "si-data_kelas", VIEW | INSERT | EDIT | DELETE); 
zHakAkses::resource_add(PEGAWAI, "si-data_kelas", VIEW | INSERT | EDIT | DELETE); 
//aksinya
zHakAkses::resource_add(ADMIN, "si-data_kelas-aksi", VIEW | INSERT | EDIT | DELETE); 
zHakAkses::resource_add(PEGAWAI, "si-data_kelas-aksi", VIEW | INSERT | EDIT | DELETE); 
//Data kelas guru
zHakAkses::resource_add(ADMIN, "si-data_kelas_guru", VIEW | INSERT | EDIT | DELETE); 
zHakAkses::resource_add(PEGAWAI, "si-data_kelas_guru", VIEW | INSERT | EDIT | DELETE); 
//aksinya
zHakAkses::resource_add(ADMIN, "si-data_kelas_guru-aksi", VIEW | INSERT | EDIT | DELETE); 
zHakAkses::resource_add(PEGAWAI, "si-data_kelas_guru-aksi", VIEW | INSERT | EDIT | DELETE); 
//Data kelas siswa
zHakAkses::resource_add(ADMIN, "si-data_kelas_siswa", VIEW | INSERT | EDIT | DELETE); 
zHakAkses::resource_add(PEGAWAI, "si-data_kelas_siswa", VIEW | INSERT | EDIT | DELETE); 
//aksinya
zHakAkses::resource_add(ADMIN, "si-data_kelas_siswa-aksi", VIEW | INSERT | EDIT | DELETE); 
zHakAkses::resource_add(PEGAWAI, "si-data_kelas_siswa-aksi", VIEW | INSERT | EDIT | DELETE); 


//Mata Pelajaran
zHakAkses::resource_add(ADMIN, "si-mata_pelajaran", VIEW | INSERT | EDIT | DELETE); 
zHakAkses::resource_add(PEGAWAI, "si-mata_pelajaran", VIEW | INSERT | EDIT | DELETE);
//GURU dan SISWA hanya bisa view
zHakAkses::resource_add(GURU, "si-mata_pelajaran", VIEW );
zHakAkses::resource_add(SISWA, "si-mata_pelajaran", VIEW );
//aksinya
zHakAkses::resource_add(ADMIN, "si-mata_pelajaran-aksi", VIEW | INSERT | EDIT | DELETE); 
zHakAkses::resource_add(PEGAWAI, "si-mata_pelajaran-aksi", VIEW | INSERT | EDIT | DELETE); 




//Jadwal Pelajaran
zHakAkses::resource_add(ADMIN, "si-jadwal_pelajaran", VIEW | INSERT | EDIT | DELETE); 
zHakAkses::resource_add(PEGAWAI, "si-jadwal_pelajaran", VIEW | INSERT | EDIT | DELETE); 
//GURU dan SISWA hanya bisa view
zHakAkses::resource_add(GURU, "si-jadwal_pelajaran", VIEW );
zHakAkses::resource_add(SISWA, "si-jadwal_pelajaran", VIEW );
//aksi
zHakAkses::resource_add(ADMIN, "si-jadwal_pelajaran-aksi", VIEW | INSERT | EDIT | DELETE); 
zHakAkses::resource_add(PEGAWAI, "si-jadwal_pelajaran-aksi", VIEW | INSERT | EDIT | DELETE); 

//khusus tema jadwal pelajaran
zHakAkses::resource_add(ADMIN, "si-jadwal_pelajaran_tema", VIEW | INSERT | EDIT | DELETE); 
zHakAkses::resource_add(PEGAWAI, "si-jadwal_pelajaran_tema", VIEW | INSERT | EDIT | DELETE); 
zHakAkses::resource_add(GURU, "si-jadwal_pelajaran_tema", VIEW | INSERT | EDIT | DELETE );

//khusus detail jadwal pelajaran
zHakAkses::resource_add(ADMIN, "si-jadwal_pelajaran_detail", VIEW | INSERT | EDIT | DELETE); 
zHakAkses::resource_add(PEGAWAI, "si-jadwal_pelajaran_detail", VIEW | INSERT | EDIT | DELETE); 
//GURU dan SISWA hanya bisa view
zHakAkses::resource_add(GURU, "si-jadwal_pelajaran_detail", VIEW );
zHakAkses::resource_add(SISWA, "si-jadwal_pelajaran_detail", VIEW );
//aksi
zHakAkses::resource_add(ADMIN, "si-jadwal_pelajaran_detail-aksi", VIEW | INSERT | EDIT | DELETE); 
zHakAkses::resource_add(PEGAWAI, "si-jadwal_pelajaran_detail-aksi", VIEW | INSERT | EDIT | DELETE); 





//Data nilai
zHakAkses::resource_add(ADMIN, "si-data_nilai", VIEW ); 
zHakAkses::resource_add(GURU, "si-data_nilai", VIEW | INSERT | EDIT | DELETE); 
//aksinya
zHakAkses::resource_add(ADMIN, "si-data_nilai-aksi", VIEW | INSERT | EDIT | DELETE); 
zHakAkses::resource_add(GURU, "si-data_nilai-aksi", VIEW | INSERT | EDIT | DELETE);
zHakAkses::resource_add(GURU, "si-data_nilai-aksi_tambah", VIEW | INSERT | EDIT | DELETE); 

//menu Data nilai (khusus siswa hanya view dengan act==view)
zHakAkses::resource_add(SISWA, "si-data_nilai-view", VIEW ); //untuk manipulasi menu langsung

//khusus untk enablekan menu utama data_nilai saja
zHakAkses::resource_add(ADMIN, "si-data_nilai_view", VIEW ); 
zHakAkses::resource_add(GURU, "si-data_nilai_view", VIEW ); 
zHakAkses::resource_add(SISWA, "si-data_nilai_view", VIEW ); 






//khusus raport OLD
zHakAkses::resource_add(ADMIN, "si-data_nilai-raport", VIEW | INSERT | EDIT | DELETE); 
zHakAkses::resource_add(GURU, "si-data_nilai-raport", VIEW | INSERT | EDIT | DELETE); 
zHakAkses::resource_add(SISWA, "si-data_nilai-raport", VIEW ); //untuk manipulasi menu langsung

//khusus raport NEW
zHakAkses::resource_add(ADMIN, "si-raport", VIEW | INSERT | EDIT | DELETE); 
zHakAkses::resource_add(GURU, "si-raport", VIEW | INSERT | EDIT | DELETE); 
zHakAkses::resource_add(SISWA, "si-raport", VIEW );
zHakAkses::resource_add(ADMIN, "si-raport-aksi", VIEW | INSERT | EDIT | DELETE); 
zHakAkses::resource_add(GURU, "si-raport-aksi", VIEW | INSERT | EDIT | DELETE); 
//zHakAkses::resource_add(SISWA, "si-raport-aksi", VIEW ); //TODO: hapus neh, coz siswa gak boleh mengakses aksi

zHakAkses::resource_add(ADMIN, "si-raport-raport", VIEW | INSERT | EDIT | DELETE); 
zHakAkses::resource_add(GURU, "si-raport-raport", VIEW | INSERT | EDIT | DELETE); 
zHakAkses::resource_add(SISWA, "si-raport-raport", VIEW ); //ini tetap boleh diakses oleh siswa


zHakAkses::resource_add(ADMIN, "si-raport-aksi_detail_raport", VIEW | INSERT | EDIT | DELETE); 
zHakAkses::resource_add(GURU, "si-raport-aksi_detail_raport", VIEW | INSERT | EDIT | DELETE);




//SMS Gateway
zHakAkses::resource_add(ADMIN, "si-sms_gateway", VIEW | INSERT | EDIT | DELETE); 
zHakAkses::resource_add(ADMIN, "si-sms_gateway_inbox", VIEW | INSERT | EDIT | DELETE); 
zHakAkses::resource_add(ADMIN, "si-sms_gateway_set", VIEW | INSERT | EDIT | DELETE); 
zHakAkses::resource_add(ADMIN, "si-sms_gateway_outbox", VIEW | INSERT | EDIT | DELETE); 

*/
?>