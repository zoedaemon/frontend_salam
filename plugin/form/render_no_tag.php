<?php

/**
$items 	: array dari pengaturan layoout dan form
$item 	:	item adalah input items yang terpilih
$name 	: nama input (item)
$i 		:	index keberapa item berada di dalam items
$is_edit: sedang dalam mode edit...?
$value_edit	:	jika dalam status edit $value_edit harus diisi
*/
function render_no_tag($items, $item, $name, $i, $is_edit, $value_edit) {
	
	switch ($item['type']) {
		
		//hidden gak perlu view layout (<tr><td> misalnya)
		case 'hidden':

				echo "<input type='$item[type]' name='$name' id='$name' $item[attributes]
					value='".(($is_edit) ? $value_edit: '')."'> ";

				if ( isset($item['special']['popup']) ) {

					$special_item = $item['special']['popup'];

					if ($special_item['hidden_button'] != true)
						echo "<tr><td></td>
								<td>
									<button  
										onclick='return content_popup(\"$special_item[link_to_unit]\", \"$special_item[link_to_module]\",
										\"".(($is_edit) ? $value_edit: '-1')."\", \"$_GET[unit]\", \"$_GET[module]\", 
										\"$name\"
										 )'>
											$special_item[popup_caption]
										</button>
								</td>
							</tr>";

					foreach ($item['special']['popup']['items_preview'] as $key => $value) {
						//tampilkan input teks biasa
						$target_id = (isset($value['target_id']) ? $value['target_id'] : $key);
						echo "<td>".(($is_edit) ? popup_get_data($special_item['link_to_unit'], 
												$special_item['link_to_module'], $value_edit, $name, $key) : '')."</td>";
					}
						
				}
		break;
		
		//NOTE: standar input text
		case 'text':


				switch ( $item['special']['type'] ) {

					//khusus tanggal					
					case 'date':
						
							echo "<tr><td>$item[caption]</td><td> :<input type='$item[type]' name='$name' id='$name' $item[attributes]
							value='".(($is_edit) ? tgl_to_view($value_edit, $item['special']['delimiter'], $item['special']['db_delimiter'])
										:'')."'  class='datepicker'> $item[info]</td></tr>";
					break;
					
					default:
						//tampilkan input teks biasa
						echo "<td><input type='$item[type]' name='$name' id='$name' $item[attributes]
						value='".(($is_edit) ? $value_edit: '')."'> $item[info]</td>";
					break;

				}
		break;



		//NOTE: input text dengan blog <textarea></textarea>
		case 'textarea':

			//tampilkan teks dalam bentuk textarea
			echo "<tr><td>$item[caption]</td><td> :
					
					<textarea name='$name' id='$name' $item[attributes]>
						".(($is_edit) ? $value_edit: '')."
					</textarea>

					$item[info]

				</td></tr>";
					
		break;


		//NOTE untuk password tidak ada modus edit...  TODO : apakah bagus..?
		case 'password':
			echo "<tr><td>$item[caption]</td><td> :<input type='$item[type]' name='$name' id='$name' $item[attributes]> 
					$item[info]</td></tr>";
		break;

		//NOTE: combobox n radio button
		case 'select':
		case 'radio':

			//jika query ke tabel lain
			if ( isset($item['values']['query']) ) {

				$values = $item['values'];


				//tipenya select/combobox
				if ($item['type'] == 'select'){
					echo "<tr>
							<td>$item[caption]</td>
							<td> :
								<select name='$name' id='$name' $item[attributes]>
									<option value='null' ".($is_edit ? '' : 'selected').">-- Pilih $item[caption]--</option>
	        						";
	        		
	        						$exec_query = mysql_query( $values['query'] );
					        		while ( $eq = mysql_fetch_array($exec_query) ) {
					        			echo '<option value="'.$eq[ $values['value'] ].'"'.
					        			($eq[ $values['value'] ] != $value_edit? 
					        				( (!$i AND !$is_edit)?'selected':'')//TODO $i tidak terdefinisi, hapus !!
					        			:'selected').'>'.$eq[ $values['caption'] ].'</option>';
					        		}
	        						
	        		 echo '		</select>
	        		 			'.$item['info'].'
	        		 		</td>
	        		 	</tr>';

	        	//jika BUKAN query ke tabel lain, langsung ambil nilai value dan caption
	        	}else {

	        		echo "<tr><td>$item[caption]</td>    <td> :";

	        						$i = 0;
					        		

					        		$exec_query = mysql_query( $values['query'] );
					        		while ( $eq = mysql_fetch_array($exec_query) ) {
					        			/*NOTE: jika algoritma ternary '?:' diganti if sama jg dgn
					        				//perlu cek nilai jika edit
					        				if ($is_edit){
					        					if ($eq[ $values['value'] ] == $value_edit)
					        						echo 'checked';
					        					else
					        						echo '';

					        				//index pertama dipilih jika bukan edit
					        				}else {
					        					if (!$i)
					        						echo 'checked';
					        					else
					        						echo '';
											}
										 */
					        			echo "<input type=radio name='$name' $item[attributes] id='$name' value='".$eq[ $values['value'] ]."'".
					        					($eq[ $values['value'] ] != $value_edit ? 
			        								( (!$i AND !$is_edit)?'checked':'')
			        							:'checked').">".$eq[ $values['caption'] ];
					        			$i++;
					        		}


                     echo $item['info'].'</td></tr>';

	        	}

			}else {

				if ($item['type'] == 'select'){
					echo "<tr>
							<td>$item[caption]</td>
							<td> :
								<select name='$name' id='$name' $item[attributes]>
									<option value='null' ".($is_edit ? '' : 'selected').">-- Pilih $item[caption]--</option>
	        						";
	        		
					        		foreach ($item['values'] as $val => $cap) {
					        			echo "<option value='$val' ".
					        				( ($val == $value_edit AND $is_edit) ? 'selected' :'').">$cap </option>";
					        		}
	        						
	        		 echo '		</select>
	        		 			'.$item['info'].'
	        		 		</td>
	        		 	</tr>';
	        	}else {

	        		echo "<tr><td>$item[caption]</td>    <td> :";
	        						$i = 0;
					        		foreach ($item['values'] as $val => $cap) {
					        			echo "<input type=radio name='$name' id='$name' value='$val' $item[attributes]".
					        					($val != $value_edit? 
					        							( (!$i AND !$is_edit)?'checked':'')
					        							:'checked').">$cap";
					        			$i++;
					        		}

                     echo $item['info'].'</td></tr>';

	        	}


			}

		break;


		case 'file':
			echo "<tr><td>$item[caption]</td><td> :<input type='$item[type]' accept='.jpg,.jpeg' name='$name' id='$name' $item[attributes]>
				 $item[info]</td></tr>";
		break;

		default:
				echo 'object tidak bisa dirender - '.$item['type'].'-'.$i; 
		break;
	}

}

?>