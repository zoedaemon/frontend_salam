<?php

/**
NOTE: fungsi javascript tuk mempermudah pemanggilan/pembuatan popup menu, content_popup.php akan bisa mengakses semua modul...

$unit 	: GET unit pada popup yang dibuka 
$module 	:	GET module pada popup yang dibuka
$id 	: id data pada popup yang dibuka

$unit_src 	: unit pada pemanggil popup  
$module_src 	:	 module pada pemanggil popup
//$id_src 	: id pada pemanggil popup

$input 		:	nama input (di dalam conf.php) yang berisi data special  
*/
?>
<script type='text/javascript'>
	function content_popup(unit, module, id, unit_src, module_src, input, anon_func) {
		var parm = 'content_popup.php?unit='+unit+'&module='+module+'&unit_src='+unit_src+'&module_src='+module_src+'&input='+input;
		if (typeof anon_func !== 'undefined')	
				parm += '&f='+anon_func;
		if (id != '-1')
			parm = parm + '&id='+id;
		var popy=window.open( parm, 'popup_form','location=no,menubar=no,status=no,scrollbars=1,top=50%,left=50%,height=350,width=1250');
		//if (anon_func) {//jika anon dibikin non string
		//	anon_func();
		//}
		return false;
	}
</script>
