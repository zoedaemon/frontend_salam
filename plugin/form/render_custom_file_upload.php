<?php

/**
$items 	: array dari pengaturan layoout dan form
$item 	:	item adalah input items yang terpilih
$name 	: nama input (item)
$i 		:	index keberapa item berada di dalam items
$is_edit: sedang dalam mode edit...?
$value_edit	:	jika dalam status edit $value_edit harus diisi
*/
function render($items, $item, $name, $i, $is_edit, $value_edit) {

	//custom style
	$main_wrapper = '<div class="control-group">';
	$main_wrapper_close = '</div>';
	$label_wrapper = '<label class="control-label">';
	$label_wrapper_close = '</label>';
	$input_wrapper = '<div class="controls">';
	$input_wrapper_close = '</div>';
	$info_wrapper = '<span class="help-block">';
	$info_wrapper_close = '</span>';

	
	switch ($item['type']) {
		
		//hidden gak perlu view layout (<tr><td> misalnya)
		case 'hidden':

				echo "<input type='$item[type]' name='$name' id='$name' 
					value='".(($is_edit) ? $value_edit: '')."'> ";

				echo "<hr/>";
				echo $main_wrapper;
						
				if ( isset($item['special']['popup']) ) {

					$special_item = $item['special']['popup'];

					if ($special_item['hidden_button'] != true) {
						echo '<script>';
						echo ' 
							function wrapper_content_popup_'.$name.' () { ';
						
						if ( isset($special_item['function']) )
							echo 'var annon_func = '.$special_item['function'].';';

						echo '		return content_popup("'.$special_item[link_to_unit].'", "'.$special_item[link_to_module].'",
										"'.(($is_edit) ? $value_edit: '-1').'", "'.$_GET[unit].'", "'.$_GET[module].'", 
										"'.$name.'" '.($special_item['function']?', annon_func':'').'
										 )
							}
						';
						echo '</script>';
						echo "$input_wrapper
									<button  
										$item[attributes]   
										onclick='return wrapper_content_popup_$name()'>
											$special_item[popup_caption]
										</button>
							$input_wrapper_close";
					}

					foreach ($item['special']['popup']['items_preview'] as $key => $value) {
						//tampilkan input teks biasa
						$target_id = (isset($value['target_id']) ? $value['target_id'] : $key);
						echo "$label_wrapper 
								$value[caption] : 
							$label_wrapper_close 
							$input_wrapper
								<input type='text' id='$target_id' $value[attributes]
								value='".(($is_edit) ? popup_get_data($special_item['link_to_unit'], 
														$special_item['link_to_module'], $value_edit, $name, $key) : '')."' disabled> 
							$input_wrapper_close";
					}
											
					echo $main_wrapper_close;
					echo "<hr/>";

				}
		break;
		
		//NOTE: standar input text
		case 'text':


				switch ( $item['special']['type'] ) {

					//khusus tanggal					
					case 'date':

						//tampilkan input teks biasa
						echo $main_wrapper;
							//print label
							echo $label_wrapper;
								echo "$item[caption] :";
							echo $label_wrapper_close;
							//print input
							echo $input_wrapper;
								echo " <input type='$item[type]' name='$name' id='$name' $item[attributes]
										value='".(($is_edit) ? tgl_to_view($value_edit, $item['special']['delimiter'], $item['special']['db_delimiter'])
										:'')."'  class='datepicker' data-date-format='dd-mm-yyyy'  />
									";
								//print info
								echo $info_wrapper;		 
									echo $item['info'];
								echo $info_wrapper_close;		 
							echo $input_wrapper_close;								
						echo $main_wrapper_close;

					break;
					
					default:
						//tampilkan input teks biasa
						echo $main_wrapper;
							//print label
							echo $label_wrapper;
								echo "$item[caption] :";
							echo $label_wrapper_close;
							//print input
							echo $input_wrapper;
								echo "  <input type='$item[type]' name='$name' id='$name' $item[attributes]
											value='".(($is_edit) ? $value_edit: '')."'>";
								//print info
								echo $info_wrapper;		 
									echo $item['info'];
								echo $info_wrapper_close;		 
							echo $input_wrapper_close;								
						echo $main_wrapper_close;
	
					break;

				}
		break;



		//NOTE: input text dengan blog <textarea></textarea>
		case 'textarea':

			//tampilkan teks dalam bentuk textarea
			echo $main_wrapper;
				//print label
				echo $label_wrapper;
					echo "$item[caption] :";
				echo $label_wrapper_close;
				//print input
				echo $input_wrapper;
					echo "<textarea name='$name' id='$name' $item[attributes]>
								".(($is_edit) ? $value_edit: '')."
							</textarea>
						";
					//print info
					echo $info_wrapper;		 
						echo $item['info'];
					echo $info_wrapper_close;		 
				echo $input_wrapper_close;								
			echo $main_wrapper_close;

		break;


		//NOTE untuk password tidak ada modus edit...  TODO : apakah bagus..?
		case 'password':
			//tampilkan input teks biasa
			echo $main_wrapper;
				//print label
				echo $label_wrapper;
					echo "$item[caption] :";
				echo $label_wrapper_close;
				//print input
				echo $input_wrapper;
					echo "  <input type='$item[type]' name='$name' id='$name' $item[attributes]>";
					//print info
					echo $info_wrapper;		 
						echo $item['info'];
					echo $info_wrapper_close;		 
				echo $input_wrapper_close;								
			echo $main_wrapper_close;
			
		break;

		//NOTE: combobox n radio button
		case 'select':
		case 'radio':

			//jika query ke tabel lain
			if ( isset($item['values']['query']) ) {

				$values = $item['values'];


				//tipenya select/combobox
				if ($item['type'] == 'select'){

					//tampilkan input teks biasa
					echo $main_wrapper;
						//print label
						echo $label_wrapper;
							echo "$item[caption] :";
						echo $label_wrapper_close;
						//print input
						echo $input_wrapper;
							echo " <select name='$name' id='$name' $item[attributes]>
									<option value='null' ".($is_edit ? '' : 'selected').">-- Pilih $item[caption]--</option>";
									
									if (isset($values['prepend_values'])) {
						        		foreach ($values['prepend_values'] as $val => $cap) {
						        			echo "<option value='$val' ".
						        				( ($val == $value_edit AND $is_edit) ? 'selected' :'').">$cap </option>";
						        		}
					        		}

									$exec_query = mysql_query( $values['query'] );
					        		while ( $eq = mysql_fetch_array($exec_query) ) {
					        			echo '<option value="'.$eq[ $values['value'] ].'" '.
					        			($eq[ $values['value'] ] != $value_edit? 
					        				( (!$i AND !$is_edit)?'selected':'')//TODO $i tidak terdefinisi, hapus !!
					        			:'selected').'>'.$eq[ $values['caption'] ].'</option>';
					        		}
					        		
							echo '</select>';
							//print info
							echo $info_wrapper;		 
								echo $item['info'];
							echo $info_wrapper_close;		 
						echo $input_wrapper_close;								
					echo $main_wrapper_close;

	        	}else {

	        		echo $main_wrapper;
	        		echo "$label_wrapper $item[caption] : $label_wrapper_close";
		        		echo $input_wrapper;
	    						
	    						$i = 0;
				        		
				        		$exec_query = mysql_query( $values['query'] );
				        		while ( $eq = mysql_fetch_array($exec_query) ) {
				        			/*NOTE: jika algoritma ternary '?:' diganti if sama jg dgn
				        				//perlu cek nilai jika edit
				        				if ($is_edit){
				        					if ($eq[ $values['value'] ] == $value_edit)
				        						echo 'checked';
				        					else
				        						echo '';

				        				//index pertama dipilih jika bukan edit
				        				}else {
				        					if (!$i)
				        						echo 'checked';
				        					else
				        						echo '';
										}
									 */
				        			echo "<input type=radio name='$name' $item[attributes] id='$name' value='".$eq[ $values['value'] ]."'".
				        					($eq[ $values['value'] ] != $value_edit ? 
		        								( (!$i AND !$is_edit)?'checked':'')
		        							:'checked').">".$eq[ $values['caption'] ];
				        			$i++;
				        		}


	                     echo $info_wrapper . $item['info'] . $info_wrapper;
	                     echo $input_wrapper_close;
                     echo $main_wrapper_close;
	        	}

	        //jika BUKAN query ke tabel lain, langsung ambil nilai value dan caption
			}else {

				if ($item['type'] == 'select') {

	        		echo $main_wrapper;
	        		echo "$label_wrapper $item[caption] : $label_wrapper_close";
		        		echo $input_wrapper;
							echo "<select name='$name' id='$name' $item[attributes]>
											<option value='null' ".($is_edit ? '' : 'selected').">-- Pilih $item[caption]--</option>
			        						";
			        		
							        		foreach ($item['values'] as $val => $cap) {
							        			echo "<option value='$val' ".
							        				( ($val == $value_edit AND $is_edit) ? 'selected' :'').">$cap </option>";
							        		}

	                     	echo '</select>'.
	                     		$info_wrapper . $item['info'] . $info_wrapper;
	                     echo $input_wrapper_close;
                     echo $main_wrapper_close;

	        	}else {


	        		echo $main_wrapper;
	        		echo "$label_wrapper $item[caption] : $label_wrapper_close";
		        		echo $input_wrapper;
    						
    						$i = 0;
			        		foreach ($item['values'] as $val => $cap) {
			        			echo "<input type=radio name='$name' id='$name' value='$val' ".
			        					($val != $value_edit? 
			        							( (!$i AND !$is_edit)?'checked':'')
			        							:'checked')."  $item[attributes]>$cap &nbsp;&nbsp;";
			        			$i++;
			        		}

	                     	echo $info_wrapper . $item['info'] . $info_wrapper;
	                     echo $input_wrapper_close;
                     echo $main_wrapper_close;

	        	}


			}

		break;


		case 'file':

									//tampilkan input teks biasa
						echo $main_wrapper;
							//print label
							echo $label_wrapper;
								echo "$item[caption] :";
							echo $label_wrapper_close;
							//print input
							echo $input_wrapper;
								echo "  <input type='$item[type]' name='$name' id='$name' $item[attributes]
											value='".(($is_edit) ? $value_edit: '')."'>";
								//print info
								echo $info_wrapper;		 
									echo $item['info'];
								echo $info_wrapper_close;		 
							echo $input_wrapper_close;								
						echo $main_wrapper_close;


			//echo "<tr><td>$item[caption]</td><td> :<input type='$item[type]' accept='.jpg,.jpeg' name='$name' id='$name' $item[attributes]>
			//	 $item[info]</td></tr>";
		break;

		default:
				echo 'object tidak bisa dirender - '.$item['type'].'-'.$i; 
		break;
	}

}

?>