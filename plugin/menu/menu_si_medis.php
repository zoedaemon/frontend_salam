<?php
	/**
		NOTE: 
		-bedanya option public dan hide_when_login adalah :
			public = true maka akan ditampilkan saat belum login maupun sudah login
			public = false maka TIDAK akan ditampilkan saat belum login tp ditampilkan saat sudah login
			hide_when_login = true maka akan ditampilkan saat belum login tp akan disembunyikan saat sudah login
			hide_when_login = false maka akan DISEMBUNYIKAN saat belum login tp akan dimunculkan saat sudah login
	*/
	$menu_samping = array(

        'datatenagapuskesmas' =>array(
            'caption' => 'List Pegawai Medis',
            'title' => 'List Pegawai Medis',
            'style' => 'icon-file',
            'controller' => 'index.php', 
            'unit' => 'si',//boleh kosong default folder app/modul/public
            'module' => 'datatenagapuskesmas',//.($_SESSION['hak_akses'] == ADMIN ? '&tahun='.(date("Y")-1):''),//undur 1 thn  
            'parent' => 'medis',
            'public' => true
        ),

        'datatenagamedis' =>array(
            'caption' => 'Daftar Tenaga Medis',
            'title' => 'Daftar Tenaga Medis',
            'style' => 'icon-file',
            'controller' => 'index.php', 
            'unit' => 'si',//boleh kosong default folder app/modul/public
            'module' => 'datatenagamedis',//.($_SESSION['hak_akses'] == ADMIN ? '&tahun='.(date("Y")-1):''),//undur 1 thn  
            'parent' => 'medis',
            'public' => true
        ),

        'datamedis' =>array(
            'caption' => 'List Tenaga Medis',
            'title' => 'Daftar Tenaga Medis',
            'style' => 'icon-file',
            'controller' => 'index.php', 
            'unit' => 'si',//boleh kosong default folder app/modul/public
            'module' => 'datamedis',//.($_SESSION['hak_akses'] == ADMIN ? '&tahun='.(date("Y")-1):''),//undur 1 thn  
            'parent' => 'medis',
            'public' => true
        )

    );

?>