<?php
	/**
		NOTE: 
		-bedanya option public dan hide_when_login adalah :
			public = true maka akan ditampilkan saat belum login maupun sudah login
			public = false maka TIDAK akan ditampilkan saat belum login tp ditampilkan saat sudah login
			hide_when_login = true maka akan ditampilkan saat belum login tp akan disembunyikan saat sudah login
			hide_when_login = false maka akan DISEMBUNYIKAN saat belum login tp akan dimunculkan saat sudah login
	*/
	$menu_samping = array(


        'sekolah' =>array(
            'caption' => 'Sekolah',
            'title' => 'Manajemen Data Sekolah',
            'style' => 'icon-th',
            'controller' => 'index.php', 
            'unit' => 'si',//boleh kosong default folder app/modul/public
            'module' => 'sekolah',//.($_SESSION['hak_akses'] == ADMIN ? '&tahun='.(date("Y")-1):''),//undur 1 thn  
            'parent' => 'sekolah',//.($_SESSION['hak_akses'] == ADMIN ? '&tahun='.(date("Y")-1):''),//undur 1 thn  
            'public' => true
        ), 

        'puskesmas' =>array(
            'caption' => 'Puskesmas',
            'title' => 'Manajemen Data puskesmas',
            'style' => 'icon-th',
            'controller' => 'index.php', 
            'unit' => 'si',//boleh kosong default folder app/modul/public
            'module' => 'puskesmas',//.($_SESSION['hak_akses'] == ADMIN ? '&tahun='.(date("Y")-1):''),//undur 1 thn  
            'parent' => 'sekolah',//.($_SESSION['hak_akses'] == ADMIN ? '&tahun='.(date("Y")-1):''),//undur 1 thn  
            'public' => true
        ),


        'pengaturan' => array(
            'caption' => 'Pengaturan Kec. - Kel.',
            'detail' => 'Pengaturan Kecamatan - <br>&nbsp;&nbsp;&nbsp;Kelurahan',
            'style' => 'icon-cog',
            'public' => true,
            //'no_check_access' => true,
            //tuk mengakali hak akses :(
            'unit' => 'pengaturan',            
            'module' => 'kecamatan',

            'sub' => array(
                'kecamatan' =>array(
                    'caption' => 'Kecamatan',
                    'title' => 'Manajemen Data Kecamatan',
                    'style' => 'icon-th',
                    'controller' => 'index.php', 
                    'unit' => 'si',//boleh kosong default folder app/modul/public
                    'module' => 'kecamatan',//.($_SESSION['hak_akses'] == ADMIN ? '&tahun='.(date("Y")-1):''),//undur 1 thn  
                    'parent' => 'sekolah',//.($_SESSION['hak_akses'] == ADMIN ? '&tahun='.(date("Y")-1):''),//undur 1 thn  
                    'public' => true
                ),  
                'kelurahan' =>array(
                    'caption' => 'Kelurahan',
                    'title' => 'Manajemen Data Kelurahan',
                    'style' => 'icon-th',
                    'controller' => 'index.php', 
                    'unit' => 'si',//boleh kosong default folder app/modul/public
                    'module' => 'kelurahan',//.($_SESSION['hak_akses'] == ADMIN ? '&tahun='.(date("Y")-1):''),//undur 1 thn  
                    'parent' => 'sekolah',//.($_SESSION['hak_akses'] == ADMIN ? '&tahun='.(date("Y")-1):''),//undur 1 thn  
                    'public' => true
                ), 
            )
        ),  

    );

?>