<?php
	/**
		NOTE: 
		-bedanya option public dan hide_when_login adalah :
			public = true maka akan ditampilkan saat belum login maupun sudah login
			public = false maka TIDAK akan ditampilkan saat belum login tp ditampilkan saat sudah login
			hide_when_login = true maka akan ditampilkan saat belum login tp akan disembunyikan saat sudah login
			hide_when_login = false maka akan DISEMBUNYIKAN saat belum login tp akan dimunculkan saat sudah login
	*/
	$menu_samping = array(

        'Mahasiswa' =>array(
            'caption' => 'Mahasiswa',
            'title' => 'Manajemen Data Mahasiswa',
            'style' => 'icon-th-list',
            'controller' => 'index.php', 
            'unit' => 'si',//boleh kosong default folder app/modul/public
            'module' => 'mahasiswa',//.($_SESSION['hak_akses'] == ADMIN ? '&tahun='.(date("Y")-1):''),//undur 1 thn  
            'parent' => 'pegawai',
            'public' => true
        ), 

        /*'setting_pgw' =>array(
            'caption' => 'Setting',
            'title' => 'Setting',
            'style' => 'icon-edit',
            'controller' => 'index.php', 
            'unit' => 'profil',//boleh kosong default folder app/modul/public
            'module' => 'ubah_password',
            

            'sub' => array(

                //jika siswa maka dari data_nilai akan jadi data_nilai_view
                'setting_tingkat_pendidikan'  => array(
                    'caption' => 'Tingkat Pendidikan',
                    'title' => 'Ubah Data Tingkat Pendidikan',
                    'style' => 'icon-pencil',
                    'controller' => 'index.php', 
                    'unit' => 'si',//boleh kosong default folder app/modul/public

                    //neh hanya untuk siswa supaya hanya bisa view
                    'module' => 'setting_tingkat_pendidikan'
                ),    
                
                'setting_eselon'  => array(
                    'caption' => 'Eselon',
                    'title' => 'Ubah Data Eselon',
                    'style' => 'icon-pencil',
                    'controller' => 'index.php', 
                    'unit' => 'si',//boleh kosong default folder app/modul/public

                    //neh hanya untuk siswa supaya hanya bisa view
                    'module' => 'setting_eselon'
                ),    
                
                
            )

        ),
        'mutasi_pgw' =>array(
            'caption' => 'Mutasi/Pensiun',
            'title' => 'Mutasi/Pensiun',
            'style' => 'icon-table',
            'controller' => 'index.php', 
            'unit' => '',//boleh kosong default folder app/modul/public
            'module' => '',
            

            'sub' => array(

                //jika siswa maka dari data_nilai akan jadi data_nilai_view
                'mutasi'  => array(
                    'caption' => 'Mutasi Out Pegawai',
                    'title' => 'Setting Mutasi Out Pegawai',
                    'style' => 'icon-retweet',
                    'controller' => 'index.php', 
                    'unit' => 'si',//boleh kosong default folder app/modul/public

                    //neh hanya untuk siswa supaya hanya bisa view
                    'module' => 'mutasi',
                    'parent' => 'pegawai'
                ),    
                
                'pensiun'  => array(
                    'caption' => 'Pensiun Pegawai',
                    'title' => 'Setting Pensiun Pegawai',
                    'style' => 'icon-remove-sign',
                    'controller' => 'index.php', 
                    'unit' => 'si',//boleh kosong default folder app/modul/public

                    //neh hanya untuk siswa supaya hanya bisa view
                    'module' => 'pensiun',
                    'parent' => 'pegawai',
                ),    
                
                
            )

        )
        */
        

    );

?>