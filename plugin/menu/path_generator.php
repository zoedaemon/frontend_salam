<?php
    /**
        Fungsi untuk menggenerate path yang diperlukan untuk operasi
    */

    use lib\security;

    function path_generator($controller, $unit, $module, $parent=null) {
        return $controller . (empty($unit)?'':'?unit='.$unit) . (empty($module)?'':'&amp;module='.$module) . (empty($parent)?'':'&amp;parent='.$parent);	
    }

    function path_generator_menu_bawah($controller, $unit, $module, $parent=null, $header=null) {
        $secret_path = (empty($unit)?'':'unit='.$unit) . (empty($module)?'':'&module='.$module) . (empty($parent)?'':'&parent='.$parent)
                    . (empty($header)?'':'&header='.$header."&\0=\0");
        $ec = security::set_ec('sid='.$_SESSION[SESSID].'&'.$secret_path);
        $path = md5($secret_path);
        $path = $controller.'?mhash='.$path.'&amp;ec='.$ec;
        //echo $path;
        return $path;	
    }
?>