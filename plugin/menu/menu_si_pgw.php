<?php
	/**
		NOTE: 
		-bedanya option public dan hide_when_login adalah :
			public = true maka akan ditampilkan saat belum login maupun sudah login
			public = false maka TIDAK akan ditampilkan saat belum login tp ditampilkan saat sudah login
			hide_when_login = true maka akan ditampilkan saat belum login tp akan disembunyikan saat sudah login
			hide_when_login = false maka akan DISEMBUNYIKAN saat belum login tp akan dimunculkan saat sudah login
	*/
	$menu_samping = array(

         'pegawai' =>array(
            'caption' => '<span class="label label-important">Kembali Ke Mahasiswa</span> ',
            'title' => 'Manajemen Data Mahasiswa',
            'style' => 'icon-arrow-left',
            'controller' => 'index.php', 
            'unit' => 'si',//boleh kosong default folder app/modul/public
            'module' => 'mahasiswa',//.($_SESSION['hak_akses'] == ADMIN ? '&tahun='.(date("Y")-1):''),//undur 1 thn  
            'parent' => 'pegawai',
            'public' => true
        ), 

        'mahasiswa_detail' =>array(
            'caption' => 'Detail Mahasiswa',
            'title' => 'Detail Mahasiswa',
            'style' => 'icon-book',
            'controller' => 'index.php', 
            'unit' => 'si',
            'module' => 'mahasiswa_detail&act=view',
            'parent' => 'pgw',//supaya mengarah ke plugin/menu/menu_si_pgw.php
            'public' => true
        ), 

/*
        'pendidikan_terakhir' =>array(
            'caption' => 'Pendidikan Terakhir',
            'title' => 'Manajemen Data Pendidikan Terakhir',
            'style' => 'icon-book',
            'controller' => 'index.php', 
            'unit' => 'si',
            'module' => 'pgw_pendidikan_terakhir',
            'parent' => 'pgw',//supaya mengarah ke plugin/menu/menu_si_pgw.php
            'public' => true
        ), 
    

        'riwayat_golongan' =>array(
            'caption' => 'Riwayat Golongan',
            'title' => 'Manajemen Data Riwayat Golongan',
            'style' => 'icon-book',
            'controller' => 'index.php', 
            'unit' => 'si',
            'module' => 'pgw_riwayat_golongan',
            'parent' => 'pgw',
            'public' => true
        ),    

        'riwayat_jabatan' =>array(
            'caption' => 'Riwayat Jabatan',
            'title' => 'Manajemen Data Riwayat Jabatan',
            'style' => 'icon-book',
            'controller' => 'index.php', 
            'unit' => 'si',
            'module' => 'pgw_riwayat_jabatan',
            'parent' => 'pgw',
            'public' => true
        ),

        'diklat_struktural' =>array(
            'caption' => 'Riwayat Diklat Struktural',
            'title' => 'Manajemen Data Diklat Struktural',
            'style' => 'icon-book',
            'controller' => 'index.php', 
            'unit' => 'si',
            'module' => 'pgw_riwayat_diklatstruk',
            'parent' => 'pgw',
            'public' => true
        ),

        'diklat_fungsional' =>array(
            'caption' => 'Riwayat Diklat Fungsional',
            'title' => 'Manajemen Data Diklat Fungsional',
            'style' => 'icon-book',
            'controller' => 'index.php', 
            'unit' => 'si',
            'module' => 'pgw_riwayat_diklatfung',
            'parent' => 'pgw',
            'public' => true
        ),

        'data_guru' =>array(
            'caption' => 'Data Guru',
            'title' => 'Manajemen Data Guru',
            'style' => 'icon-book',
            'controller' => 'index.php', 
            'unit' => 'si',
            'module' => 'pgw_data_guru',
            'parent' => 'pgw',
            'public' => true,
            'function_visibility' => function(){

                //nip tidak boleh kosong
                $nip = $_GET['nip'];
                if ( empty($nip) ) {
                    return false;  
                } 

                //supress warning pakai @ dan lakukan query
                $q = @mysql_query("SELECT * FROM pegawai WHERE nip='$nip'");
                if (!mysql_error()) {
                    if ($q = mysql_fetch_array($q)) {
                        //echo '$q[id_skpd]' . $q['id_SKPD'];
                        //supress warning lagi dan query data skpd
                        $q = @mysql_query("SELECT * FROM subskpd WHERE id='$q[id_SubSKPD]'");
                        if (!mysql_error() && ($q = @mysql_fetch_array($q)) ) {
                            //echo '$q[nama_skpd]' . $q['nama_skpd'];
                            //lowercase semua baru bandingkan
                            $s = @mysql_query("SELECT * FROM skpd WHERE id='$q[id_skpd]'");
                            $s = @mysql_fetch_array($s);
                            $check_me = strtolower($s['nama_skpd']);
                            if ( strpos($check_me, 'pendidikan') !== false ) { 
                                return true; 
                            }
                        }
                    }
                }

                return false;
            }
        ),             

        'data_dokter' =>array(
            'caption' => 'Tenaga Medis',
            'title' => 'Manajemen Tenaga Medis',
            'style' => 'icon-book',
            'controller' => 'index.php', 
            'unit' => 'si',
            'module' => 'pgw_data_dokter',
            'parent' => 'pgw',
            'public' => true,
            'function_visibility' => function(){

                //nip tidak boleh kosong
                $nip = $_GET['nip'];
                if ( empty($nip) ) {
                    return false;  
                } 

                //supress warning pakai @ dan lakukan query
                $q = @mysql_query("SELECT * FROM pegawai WHERE nip='$nip'");
                if (!mysql_error()) {
                    if ($q = mysql_fetch_array($q)) {
                        //echo '$q[id_skpd]' . $q['id_SKPD'];
                        //supress warning lagi dan query data skpd
                        $q = @mysql_query("SELECT * FROM subskpd WHERE id='$q[id_SubSKPD]'");
                        if (!mysql_error() && ($q = @mysql_fetch_array($q)) ) {
                            //echo '$q[nama_skpd]' . $q['nama_skpd'];
                            //lowercase semua baru bandingkan
                            $s = @mysql_query("SELECT * FROM skpd WHERE id='$q[id_skpd]'");
                            $s = @mysql_fetch_array($s);
                            $check_me = strtolower($s['nama_skpd']);
                            if ( strpos($check_me, 'kesehatan') !== false ) { 
                                return true; 
                            }
                        }
                    }
                }

                return false;
            }
        ),             
*/
    );

?>