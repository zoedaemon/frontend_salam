<?php
	/**
		NOTE: 
		-bedanya option public dan hide_when_login adalah :
			public = true maka akan ditampilkan saat belum login maupun sudah login
			public = false maka TIDAK akan ditampilkan saat belum login tp ditampilkan saat sudah login
			hide_when_login = true maka akan ditampilkan saat belum login tp akan disembunyikan saat sudah login
			hide_when_login = false maka akan DISEMBUNYIKAN saat belum login tp akan dimunculkan saat sudah login
	*/
	$menu_samping = array(

        'datasekolah' =>array(
            'caption' => 'List Sekolah',
            'title' => 'List Sekolah',
            'style' => 'icon-file',
            'controller' => 'index.php', 
            'unit' => 'si',//boleh kosong default folder app/modul/public
            'module' => 'datasekolah',//.($_SESSION['hak_akses'] == ADMIN ? '&tahun='.(date("Y")-1):''),//undur 1 thn  
            'parent' => 'pendidikan',
            'public' => true
        ),

        'daftarsekolah' =>array(
            'caption' => 'Daftar Sekolah',
            'title' => 'Daftar Sekolah',
            'style' => 'icon-file',
            'controller' => 'index.php', 
            'unit' => 'si',//boleh kosong default folder app/modul/public
            'module' => 'daftarsekolah',//.($_SESSION['hak_akses'] == ADMIN ? '&tahun='.(date("Y")-1):''),//undur 1 thn  
            'parent' => 'pendidikan',
            'public' => true
        ),

        'datapendidik' =>array(
            'caption' => 'Daftar Tenaga Pendidik',
            'title' => 'Daftar Tenaga Pendidik',
            'style' => 'icon-file',
            'controller' => 'index.php', 
            'unit' => 'si',//boleh kosong default folder app/modul/public
            'module' => 'datapendidik',//.($_SESSION['hak_akses'] == ADMIN ? '&tahun='.(date("Y")-1):''),//undur 1 thn  
            'parent' => 'pendidikan',
            'public' => true
        ),

        'listpendidik' =>array(
            'caption' => 'List Tenaga Pendidik',
            'title' => 'List Tenaga Pendidik',
            'style' => 'icon-file',
            'controller' => 'index.php', 
            'unit' => 'si',//boleh kosong default folder app/modul/public
            'module' => 'listpendidik',//.($_SESSION['hak_akses'] == ADMIN ? '&tahun='.(date("Y")-1):''),//undur 1 thn  
            'parent' => 'pendidikan',
            'public' => true
        )

    );

?>