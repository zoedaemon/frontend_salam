<?php
	/**
		NOTE: 
		-bedanya option public dan hide_when_login adalah :
			public = true maka akan ditampilkan saat belum login maupun sudah login
			public = false maka TIDAK akan ditampilkan saat belum login tp ditampilkan saat sudah login
			hide_when_login = true maka akan ditampilkan saat belum login tp akan disembunyikan saat sudah login
			hide_when_login = false maka akan DISEMBUNYIKAN saat belum login tp akan dimunculkan saat sudah login
	*/
	$menu = array(
        
        'user_beranda' =>array(
            'caption' => 'Beranda',
            'title' => 'Kembali ke halaman utama',
            'style' => 'icon-home',
            'controller' => '../index.php', 
            'unit' => 'member',//boleh kosong default folder app/modul/public
            'module' => 'home',
            'public' => false,
        ), 

        'beranda' =>array(
            'caption' => 'Beranda',
            'title' => 'Kembali ke halaman utama',
            'header' => 'Beranda', //akan ditampilkan di <h1>...</h1>
            'style' => '',
            'controller' => 'public/redirect_to_home.php', 
            'unit' => 'main',//boleh kosong default folder app/modul/public
            'module' => 'home',
            'public' => true,
            'function_visibility' => function(){
                //nip tidak boleh kosong
                if ( isset($_SESSION[SESSLOGIN]) ) {
                    return true;  
                } 
                return false;
            }
        ), 

        'aspirasi_date' => array(
            'caption' => 'Aspirasi Berdasarkan',
            'detail' => 'Aspirasi pelaporan masyarakat',
            'style' => '',
            'public' => false,
            //'no_check_access' => true,
            //tuk mengakali hak akses :(
            'unit' => 'si',            
            'module' => 'pendaftaran',
            'function_visibility' => function(){
                //nip tidak boleh kosong
                if ( isset($_SESSION[SESSLOGIN]) ) {
                    return true;  
                } 
                return false;
            },
            'sub' => array(
                
                'aspirasi_date' =>array(
                    'caption' => 'Waktu',
                    'title' => 'Data Aspirasi berdasarkan waktu/tgl pelaporan',
                    'header' => 'SMS Aspirasi Terbaru', //akan ditampilkan di <h1>...</h1>
                    'style' => 'icon-th-large',
                    'controller' => 'index.php', 
                    'unit' => 'main',//boleh kosong default folder app/modul/public
                    'module' => 'aspirasi_date',//.($_SESSION['hak_akses'] == ADMIN ? '&tahun='.(date("Y")-1):''),//undur 1 thn  
                    'parent' => 'aspirasi',//.($_SESSION['hak_akses'] == ADMIN ? '&tahun='.(date("Y")-1):''),//undur 1 thn  
                    'public' => false
                ), 

                'aspirasi_kategori' =>array(
                    'caption' => 'Kategori',
                    'title' => 'Data Aspirasi berdasarkan kategori pelaporan',
                    'header' => 'SMS Aspirasi Berdasarkan Kategori', //akan ditampilkan di <h1>...</h1>
                    'style' => 'icon-th-large',
                    'controller' => 'index.php', 
                    'unit' => 'main',//boleh kosong default folder app/modul/public
                    'module' => 'aspirasi_kategori',//.($_SESSION['hak_akses'] == ADMIN ? '&tahun='.(date("Y")-1):''),//undur 1 thn  
                    'parent' => 'aspirasi',//.($_SESSION['hak_akses'] == ADMIN ? '&tahun='.(date("Y")-1):''),//undur 1 thn  
                    'public' => false
                ), 
 
                
                'aspirasi_lokasi' =>array(
                    'caption' => 'Lokasi',
                    'title' => 'Data Aspirasi berdasarkan lokasi pelaporan',
                    'header' => 'SMS Aspirasi Berdasarkan Lokasi', //akan ditampilkan di <h1>...</h1>
                    'style' => 'icon-th-large',
                    'controller' => 'index.php', 
                    'unit' => 'main',//boleh kosong default folder app/modul/public
                    'module' => 'aspirasi_lokasi',//.($_SESSION['hak_akses'] == ADMIN ? '&tahun='.(date("Y")-1):''),//undur 1 thn  
                    'parent' => 'aspirasi',//.($_SESSION['hak_akses'] == ADMIN ? '&tahun='.(date("Y")-1):''),//undur 1 thn  
                    'public' => false
                ), 

                
                'aspirasi_prioritas' =>array(
                    'caption' => 'Status / Prioritas',
                    'title' => 'Data Aspirasi berdasarkan status/prioritas pelaporan',
                    'header' => 'SMS Aspirasi Berdasarkan Prioritas', //akan ditampilkan di <h1>...</h1>
                    'style' => 'icon-th-large',
                    'controller' => 'index.php', 
                    'unit' => 'main',//boleh kosong default folder app/modul/public
                    'module' => 'aspirasi_prioritas',//.($_SESSION['hak_akses'] == ADMIN ? '&tahun='.(date("Y")-1):''),//undur 1 thn  
                    'parent' => 'aspirasi',//.($_SESSION['hak_akses'] == ADMIN ? '&tahun='.(date("Y")-1):''),//undur 1 thn  
                    'public' => false
                ), 
                
 
            )
        ),


        
        'grafik_statistik' => array(
            'caption' => 'Statistik',
            'detail' => 'data-data statistik dari pelaporan-pelaporan masyarakat',
            'style' => '',
            'public' => false,
            //'no_check_access' => true,
            //tuk mengakali hak akses :(
            'unit' => 'si',            
            'module' => 'pendaftaran',
            'function_visibility' => function(){
                //nip tidak boleh kosong
                if ( isset($_SESSION[SESSLOGIN]) ) {
                    return true;  
                } 
                return false;
            },
            'sub' => array(
                
                'statistik_grafik' =>array(
                    'caption' => 'Grafik',
                    'title' => 'Data dalam bentuk grafik',
                    'header' => 'Data dalam bentuk grafik',
                    'style' => 'icon-th-large',
                    'controller' => 'index.php', 
                    'unit' => 'main',//boleh kosong default folder app/modul/public
                    'module' => 'statistik_grafik',//.($_SESSION['hak_akses'] == ADMIN ? '&tahun='.(date("Y")-1):''),//undur 1 thn  
                    'parent' => 'statistik',//.($_SESSION['hak_akses'] == ADMIN ? '&tahun='.(date("Y")-1):''),//undur 1 thn  
                    'public' => false
                ), 

                'statistik_rekapitulasi' =>array(
                    'caption' => 'Rekapitulasi Kelurahan',
                    'title' => 'Rekapitulasi Kelurahan',
                    'header' => 'Rekapitulasi Berdasarkan Kelurahan',
                    'style' => 'icon-th-large',
                    'controller' => 'index.php', 
                    'unit' => 'main',//boleh kosong default folder app/modul/public
                    'module' => 'statistik_rekapitulasi',//.($_SESSION['hak_akses'] == ADMIN ? '&tahun='.(date("Y")-1):''),//undur 1 thn  
                    'parent' => 'statistik',//.($_SESSION['hak_akses'] == ADMIN ? '&tahun='.(date("Y")-1):''),//undur 1 thn  
                    'public' => false
                ),

                'statistik_rekapitulasi_prioritas' =>array(
                    'caption' => 'Rekapitulasi Status/Prioritas',
                    'title' => 'Rekapitulasi data keseluruhan',
                    'header' => 'Rekapitulasi Berdasarkan Prioritas',
                    'style' => 'icon-th-large',
                    'controller' => 'index.php', 
                    'unit' => 'main',//boleh kosong default folder app/modul/public
                    'module' => 'statistik_rekapitulasi_prioritas',//.($_SESSION['hak_akses'] == ADMIN ? '&tahun='.(date("Y")-1):''),//undur 1 thn  
                    'parent' => 'statistik',//.($_SESSION['hak_akses'] == ADMIN ? '&tahun='.(date("Y")-1):''),//undur 1 thn  
                    'public' => false
                ),  
            )
        ),

        
        'manajemen_kategori' => array(
            'caption' => 'Sistem',
            'detail' => 'data-data statistik dari pelaporan-pelaporan masyarakat',
            'style' => '',
            'public' => false,
            //'no_check_access' => true,
            //tuk mengakali hak akses :(
            'unit' => 'si',            
            'module' => 'pendaftaran',
            'function_visibility' => function(){
                //nip tidak boleh kosong
                if ( isset($_SESSION[SESSLOGIN]) ) {
                    return true;  
                } 
                return false;
            },
            'sub' => array(
                
                'manajemen_kategori' =>array(
                    'caption' => 'Manajemen Kategori',
                    'title' => 'Manajemen Data Kategori-Kategori Aspirasi/Pelaporan',
                    'header' => 'Manajemen Data Kategori-Kategori Aspirasi/Pelaporan',
                    'style' => 'icon-th-large',
                    'controller' => 'index.php', 
                    'unit' => 'main',//boleh kosong default folder app/modul/public
                    'module' => 'manajemen_kategori',//.($_SESSION['hak_akses'] == ADMIN ? '&tahun='.(date("Y")-1):''),//undur 1 thn  
                    'parent' => 'sistem',//.($_SESSION['hak_akses'] == ADMIN ? '&tahun='.(date("Y")-1):''),//undur 1 thn  
                    'public' => false
                ), 

                'manajemen_lokasi' =>array(
                    'caption' => 'Manajemen Lokasi',
                    'title' => 'Manajemen Data Lokasi-Lokasi Pelaporan',
                    'header' => 'Manajemen Data Lokasi-Lokasi Aspirasi/Pelaporan',
                    'style' => 'icon-th-large',
                    'controller' => 'index.php', 
                    'unit' => 'main',//boleh kosong default folder app/modul/public
                    'module' => 'manajemen_lokasi',//.($_SESSION['hak_akses'] == ADMIN ? '&tahun='.(date("Y")-1):''),//undur 1 thn  
                    'parent' => 'sistem',//.($_SESSION['hak_akses'] == ADMIN ? '&tahun='.(date("Y")-1):''),//undur 1 thn  
                    'public' => false
                ),   

                'spam' =>array(
                    'caption' => 'Spam',
                    'title' => 'Manajemen Data-Data pelaporan yang tergolong spam',
                    'header' => 'Manajemen Data-Data pelaporan yang tergolong spam',
                    'style' => 'icon-th-large',
                    'controller' => 'index.php', 
                    'unit' => 'main',//boleh kosong default folder app/modul/public
                    'module' => 'spam',//.($_SESSION['hak_akses'] == ADMIN ? '&tahun='.(date("Y")-1):''),//undur 1 thn  
                    'parent' => 'sistem',//.($_SESSION['hak_akses'] == ADMIN ? '&tahun='.(date("Y")-1):''),//undur 1 thn  
                    'public' => false
                ),   

                'blacklist' =>array(
                    'caption' => 'Blacklist',
                    'title' => 'Nomor telpon pelapor yang blacklist (tidak diproses sama sekali jika ada sms masuk)',
                    'header' => 'Nomor telpon pelapor yang diblacklist',
                    'style' => 'icon-th-large',
                    'controller' => 'index.php', 
                    'unit' => 'main',//boleh kosong default folder app/modul/public
                    'module' => 'blacklist',//.($_SESSION['hak_akses'] == ADMIN ? '&tahun='.(date("Y")-1):''),//undur 1 thn  
                    'parent' => 'sistem',//.($_SESSION['hak_akses'] == ADMIN ? '&tahun='.(date("Y")-1):''),//undur 1 thn  
                    'public' => false
                ),

                /*
                'aktifitas_masuk' =>array(
                    'caption' => 'Aktifitas Masuk',
                    'title' => 'Log khusus aktifitas masuk',
                    'style' => 'icon-th-large',
                    'controller' => 'index.php', 
                    'unit' => 'main',//boleh kosong default folder app/modul/public
                    'module' => 'aktifitas_masuk',//.($_SESSION['hak_akses'] == ADMIN ? '&tahun='.(date("Y")-1):''),//undur 1 thn  
                    'parent' => 'sistem',//.($_SESSION['hak_akses'] == ADMIN ? '&tahun='.(date("Y")-1):''),//undur 1 thn  
                    'public' => false
                ),
                */   
            )
        ),





        'akun' => array(
            'caption' => 'Akun',
            'detail' => 'Akun',
            'style' => '',
            'public' => false,
            //'no_check_access' => true,
            //tuk mengakali hak akses :(
            'unit' => 'si',            
            'module' => 'pendaftaran',
            'function_visibility' => function(){
                //nip tidak boleh kosong
                if ( isset($_SESSION[SESSLOGIN]) ) {
                    return true;  
                } 
                return false;
            },
            'sub' => array(
                
                'manajemen_user' =>array(
                    'caption' => 'Manajemen User',
                    'title' => 'Manajemen User',
                    'header' => 'Manajemen User',
                    'style' => 'icon-th-large',
                    'controller' => 'index.php', 
                    'unit' => 'setting',//boleh kosong default folder app/modul/public
                    'module' => 'manajemen_user',//.($_SESSION['hak_akses'] == ADMIN ? '&tahun='.(date("Y")-1):''),//undur 1 thn  
                    'parent' => 'sistem',//.($_SESSION['hak_akses'] == ADMIN ? '&tahun='.(date("Y")-1):''),//undur 1 thn  
                    'public' => false
                ), 

                'logout' =>array(
                    'caption' => 'Logout',
                    'title' => 'Keluar dari halaman',
                    'style' => 'icon-remove',
                    'controller' => 'logout.php'.( $_SESSION['hak_akses'] == MHS ? '?user=true':''),//ganti jadi login.php 
                    'unit' => '',//boleh kosong default folder app/modul/public
                    'module' => '',
                    'public' => false,
                    //'no_check_access' => true,

                ),
            )
        ),
/*


        'user_profile_saya' =>array(
            'caption' => 'Profile Saya',
            'title' => 'Detail Profile Mahasiswa',
            'style' => 'icon-home',
            'controller' => 'index.php', 
            'unit' => 'member',//boleh kosong default folder app/modul/public
            'module' => 'profile',
            'public' => true,
            'function_visibility' => function(){
                //nip tidak boleh kosong
                if ( isset($_SESSION['profile-id']) ) {
                    return true;  
                } 
                return false;
            }
        ), 


        'user_profile_saya' =>array(
            'caption' => 'Profile Saya',
            'title' => 'Detail Profile Mahasiswa',
            'style' => 'icon-home',
            'controller' => 'index.php', 
            'unit' => 'member',//boleh kosong default folder app/modul/public
            'module' => 'profile',
            'public' => true,
            'function_visibility' => function(){
                //nip tidak boleh kosong
                if ( isset($_SESSION['profile-id']) ) {
                    return true;  
                } 
                return false;
            }
        ), 


        'mahasiswa' =>array(
            'caption' => 'Mahasiswa',
            'title' => 'Manajemen Data Mahasiswa',
            'style' => 'icon-th-list',
            'controller' => 'index.php', 
            'unit' => 'si',//boleh kosong default folder app/modul/public
            'module' => 'mahasiswa',//.($_SESSION['hak_akses'] == ADMIN ? '&tahun='.(date("Y")-1):''),//undur 1 thn  
            'parent' => 'mahasiswa',//.($_SESSION['hak_akses'] == ADMIN ? '&tahun='.(date("Y")-1):''),//undur 1 thn  
            'public' => true
        ),

*/

    /*
        'form_pendaftaran' =>array(
            'caption' => 'Form Pendaftaran',
            'title' => 'Input Data Mahasiswa Di Form Pendaftaran',
            'style' => 'icon-th-list',
            'controller' => 'index.php', 
            'unit' => 'si',//boleh kosong default folder app/modul/public
            'module' => 'form_pendaftaran',//.($_SESSION['hak_akses'] == ADMIN ? '&tahun='.(date("Y")-1):''),//undur 1 thn  
            'parent' => 'form_pendaftaran',//.($_SESSION['hak_akses'] == ADMIN ? '&tahun='.(date("Y")-1):''),//undur 1 thn  
            'public' => true
        ),


        'alumni' =>array(
            'caption' => 'Alumni',
            'title' => 'Manajemen Data Alumni',
            'style' => 'icon-th-large',
            'controller' => 'index.php', 
            'unit' => 'si',//boleh kosong default folder app/modul/public
            'module' => 'alumni',//.($_SESSION['hak_akses'] == ADMIN ? '&tahun='.(date("Y")-1):''),//undur 1 thn  
            'parent' => 'alumni',//.($_SESSION['hak_akses'] == ADMIN ? '&tahun='.(date("Y")-1):''),//undur 1 thn  
            'public' => false
        ), 

        

        'berita_view' =>array(
            'caption' => 'Berita',
            'title' => 'View Data Berita',
            'style' => 'icon-th-large',
            'controller' => 'index.php', 
            'unit' => 'si',//boleh kosong default folder app/modul/public
            'module' => 'berita_view',//.($_SESSION['hak_akses'] == ADMIN ? '&tahun='.(date("Y")-1):''),//undur 1 thn  
            'parent' => 'berita_view',//.($_SESSION['hak_akses'] == ADMIN ? '&tahun='.(date("Y")-1):''),//undur 1 thn  
            'public' => true,
            'function_visibility' => function(){
                //nip tidak boleh kosong
                if ( $_SESSION['hak_akses'] == ADMIN  ) {
                    return false;  
                } 
                return true;
            }
        ), 
*/
        'ubah_password' =>array(
            'caption' => 'Ubah Password',
            'title' => 'Ubah Data Login Anda',
            'style' => 'icon-pencil',
            'controller' => 'index.php', 
            'unit' => 'setting',//boleh kosong default folder app/modul/public
            'module' => 'ubah_password',//.($_SESSION['hak_akses'] == ADMIN ? '&tahun='.(date("Y")-1):''),//undur 1 thn  
            'public' => true,
            'function_visibility' => function(){
                //nip tidak boleh kosong
                if ( $_SESSION['hak_akses'] != ADMIN  || $_SESSION['hak_akses'] == PENGUNJUNG) {
                    return false;  
                } 
                return true;
            }
        ), 



        'berita' =>array(
            'caption' => 'Permintaan Kas',
            'title' => 'View Data Permintaan Kas',
            'style' => 'icon-th-large',
            'controller' => 'index.php', 
            'unit' => 'si',//boleh kosong default folder app/modul/public
            'module' => 'berita',//.($_SESSION['hak_akses'] == ADMIN ? '&tahun='.(date("Y")-1):''),//undur 1 thn  
            'parent' => 'berita',//.($_SESSION['hak_akses'] == ADMIN ? '&tahun='.(date("Y")-1):''),//undur 1 thn  
            'public' => false,
            'function_visibility' => function(){
                //nip tidak boleh kosong
                if ( $_SESSION['hak_akses'] == ADMIN  ) {
                    return true;  
                } 
                return false;
            }
        ), 

        
        /*
        'data' => array(
            'caption' => 'Data',
            'detail' => 'Laporan Data',
            'style' => 'icon-th-list',
            'public' => true,
            //'no_check_access' => true,
            //tuk mengakali hak akses :(
            'unit' => 'si',            
            'module' => 'data',

            'sub' => array(
                
                'universitas' =>array(
                    'caption' => 'Universitas-Fakultas-Jurusan',
                    'title' => 'Manajemen Data Universitas-Fakultas-Jurusan',
                    'style' => 'icon-th-large',
                    'controller' => 'index.php', 
                    'unit' => 'si',//boleh kosong default folder app/modul/public
                    'module' => 'universitas',//.($_SESSION['hak_akses'] == ADMIN ? '&tahun='.(date("Y")-1):''),//undur 1 thn  
                    'parent' => 'universitas',//.($_SESSION['hak_akses'] == ADMIN ? '&tahun='.(date("Y")-1):''),//undur 1 thn  
                    'public' => false
                ), 

                'Kecamatan' =>array(
                    'caption' => 'Kecamatan',
                    'title' => 'Manajemen Data Kecamatan',
                    'style' => 'icon-th-large',
                    'controller' => 'index.php', 
                    'unit' => 'si',//boleh kosong default folder app/modul/public
                    'module' => 'kecamatan',//.($_SESSION['hak_akses'] == ADMIN ? '&tahun='.(date("Y")-1):''),//undur 1 thn  
                    'parent' => 'kecamatan',//.($_SESSION['hak_akses'] == ADMIN ? '&tahun='.(date("Y")-1):''),//undur 1 thn  
                    'public' => false
                ), 
            )
        ),
        */
        /*
        'sekolah' =>array(
            'caption' => 'Data',
            'title' => 'Manajemen Data Sekolah/Puskesmas',
            'style' => 'icon-th-large',
            'controller' => 'index.php', 
            'unit' => 'si',//boleh kosong default folder app/modul/public
            'module' => 'sekolah',//.($_SESSION['hak_akses'] == ADMIN ? '&tahun='.(date("Y")-1):''),//undur 1 thn
            'parent' => 'sekolah',//.($_SESSION['hak_akses'] == ADMIN ? '&tahun='.(date("Y")-1):''),//undur 1 thn    
            'public' => true
        ),
        

        'laporan' => array(
            'caption' => 'Laporan',
            'detail' => 'Laporan',
            'style' => 'icon-th-list',
            'public' => true,
            //'no_check_access' => true,
            //tuk mengakali hak akses :(
            'unit' => 'laporan',            
            'module' => 'data',

            'sub' => array(
            
            'dataskpd' => array(
                    'caption' => 'Laporan SKPD',
                    'title' => 'Laporan SKPD',
                    'style' => 'icon-user',
                    'controller' => '', // kosong berarti adalah parentnya alias media.php?  
                    'unit' => 'si',            
                    'module' => 'dataskpd',
                    'parent' => 'dataskpd'

            ),
            'datasekolah' => array(
                    'caption' => 'Laporan Pendidikan',
                    'title' => 'Laporan Pendidikan',
                    'style' => 'icon-user',
                    'controller' => '', // kosong berarti adalah parentnya alias media.php?  
                    'unit' => 'si',            
                    'module' => 'datasekolah',
                    'parent' => 'pendidikan'

            ),
            'datatenagapuskesmas' => array(
                    'caption' => 'Laporan Tenaga Medis',
                    'title' => 'Laporan Tenaga Medis',
                    'style' => 'icon-user',
                    'controller' => '', // kosong berarti adalah parentnya alias media.php?  
                    'unit' => 'si',            
                    'module' => 'datatenagapuskesmas',
                    'parent' => 'medis'

            ),
            'datamutasi' => array(
                    'caption' => 'Laporan Mutasi Out Pegawai',
                    'title' => 'Laporan Data Mutasi Out Pegawai',
                    'style' => 'icon-th-list',
                    'controller' => '', // kosong berarti adalah parentnya alias media.php?  
                    'unit' => 'si',            
                    'module' => 'datamutasi',

            ),
            'datapensiun' => array(
                    'caption' => 'Laporan Pensiun Pegawai',
                    'title' => 'Laporan Data Pensiun Pegawai',
                    'style' => 'icon-th-list',
                    'controller' => '', // kosong berarti adalah parentnya alias media.php?  
                    'unit' => 'si',            
                    'module' => 'datapensiun',

            )
            )
        ),
        */

/*        'grafik' =>array(
            'caption' => 'Grafik',
            'title' => 'Grafik Data',
            'style' => 'icon-signal',
            'controller' => 'index.php', 
            'unit' => 'si',//boleh kosong default folder app/modul/public
            'module' => 'grafik',//.($_SESSION['hak_akses'] == ADMIN ? '&tahun='.(date("Y")-1):''),//undur 1 thn  
            'parent' => 'grafik',//.($_SESSION['hak_akses'] == ADMIN ? '&tahun='.(date("Y")-1):''),//undur 1 thn  
            'public' => true
        ), 
*/
        'pengaturan' => array(
            'caption' => 'Pengaturan',
            'detail' => 'Pengaturan',
            'style' => 'icon-cog',
            'public' => true,
            //'no_check_access' => true,
            //tuk mengakali hak akses :(
            'unit' => 'pengaturan',            
            'module' => 'pengguna',

            'sub' => array(
                'pengguna' => array(
                    'caption' => 'Manajemen User',
                    'title' => 'Manajemen User',
                    'style' => 'icon-user',
                    'controller' => '', // kosong berarti adalah parentnya alias media.php?  
                    'unit' => 'setting',            
                    'module' => 'manajemen_user',

                ),
                /*
                'setting' => array(
                    'caption' => 'Manajemen Setting',
                    'title' => 'Setting Data',
                    'style' => 'icon-th-list',
                    'controller' => '', // kosong berarti adalah parentnya alias media.php?  
                    'unit' => 'setting',            
                    'module' => 'manajemen_setting&parameter=AGAMA',//.($_SESSION['hak_akses'] == ADMIN ? '&tahun='.(date("Y")-1):''),//undur 1 thn  
                    'parent' => 'setting',
                    'public' => true

                ),*/
            )
        ), 


/*
        'profil' =>array(
            'caption' => 'Profil Saya',
            'title' => 'Profil Saya',
            'style' => 'icon-camera',
            'controller' => 'index.php', 
            'unit' => 'profil',//boleh kosong default folder app/modul/public
            'module' => 'ubah_password',
            

            'sub' => array(

                //jika siswa maka dari data_nilai akan jadi data_nilai_view
                'ubah_password'.($_SESSION['hak_akses'] == SISWA ? '_view':'')  => array(
                    'caption' => 'Ubah Password',
                    'title' => 'Ubah Password Anda',
                    'style' => 'icon-pencil',
                    'controller' => 'index.php', 
                    'unit' => 'si',//boleh kosong default folder app/modul/public

                    //neh hanya untuk siswa supaya hanya bisa view
                    'module' => 'ubah_password'
                ),    
                

                'logout' =>array(
                    'caption' => 'Logout',
                    'title' => 'Keluar dari halaman',
                    'style' => 'icon-remove',
                    'controller' => 'logout.php',//ganti jadi login.php 
                    'unit' => '',//boleh kosong default folder app/modul/public
                    'module' => '',

                    'no_check_access' => true,

                    'hide_when_login' => false //jgn dideklarasikan jika tidak perlu, karena akan dihidden jika nilai false dan status sedang tidak login
                ),
                
            )

        ),

        'logout' =>array(
            'caption' => 'Logout',
            'title' => 'Keluar dari halaman',
            'style' => 'icon-remove',
            'controller' => 'logout.php?'.( $_SESSION['hak_akses'] == MHS ? 'user=true':''),//ganti jadi login.php 
            'unit' => '',//boleh kosong default folder app/modul/public
            'module' => '',
            'public' => false,
            //'no_check_access' => true,

            'function_visibility' => function(){

                //nip tidak boleh kosong
                $nip = $_SESSION['hak_akses'];
                if ( $_SESSION['hak_akses'] != 'z' ) {
                    return true;  
                } 


                return false;
            }
        ),

*/

        'login_user' =>array(
            'caption' => 'Login',
            'title' => 'Login',
            'style' => 'icon-check',
            'controller' => 'login.php',//ganti jadi login.php 
            'unit' => '',//boleh kosong default folder app/modul/public
            'module' => '',
            'public' => true,

            'function_visibility' => function(){

                //nip tidak boleh kosong
                $nip = $_SESSION['hak_akses'];
                if ( $_SESSION['hak_akses'] == 'P') {
                    return true;  
                } 
                return false;
            }
        ),

    );

?>