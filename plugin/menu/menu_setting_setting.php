<?php
	/**
		NOTE: 
		-bedanya option public dan hide_when_login adalah :
			public = true maka akan ditampilkan saat belum login maupun sudah login
			public = false maka TIDAK akan ditampilkan saat belum login tp ditampilkan saat sudah login
			hide_when_login = true maka akan ditampilkan saat belum login tp akan disembunyikan saat sudah login
			hide_when_login = false maka akan DISEMBUNYIKAN saat belum login tp akan dimunculkan saat sudah login
	*/
	$menu_samping = array(

        'setting_agama' =>array(
            'caption' => 'Setting Agama',
            'title' => 'Setting Agama',
            'style' => 'icon-cog',
            'controller' => 'index.php', 
            'unit' => 'setting',//boleh kosong default folder app/modul/public
            'module' => 'manajemen_setting&parameter=AGAMA',//.($_SESSION['hak_akses'] == ADMIN ? '&tahun='.(date("Y")-1):''),//undur 1 thn  
            'parent' => 'setting',
            'public' => true
        ),

        'setting_jenis_dokter' =>array(
            'caption' => 'Setting Jenis Tenaga Medis',
            'title' => 'Setting Jenis Dokter',
            'style' => 'icon-cog',
            'controller' => 'index.php', 
            'unit' => 'setting',//boleh kosong default folder app/modul/public
            'module' => 'manajemen_setting&parameter=JENIS_DOKTER',//.($_SESSION['hak_akses'] == ADMIN ? '&tahun='.(date("Y")-1):''),//undur 1 thn  
            'parent' => 'setting',
            'public' => true
        ),

         /*'setting_pendidikan' =>array(
            'caption' => 'Setting Pendidikan',
            'title' => 'Setting Pendidikan',
            'style' => 'icon-cog',
            'controller' => 'index.php', 
            'unit' => 'setting',//boleh kosong default folder app/modul/public
            'module' => 'setting_pendidikan',//.($_SESSION['hak_akses'] == ADMIN ? '&tahun='.(date("Y")-1):''),//undur 1 thn  
            'parent' => 'setting',
            'public' => true
        ), 

        'setting_jabatan' =>array(
            'caption' => 'Setting Jabatan',
            'title' => 'Setting Jabatan',
            'style' => 'icon-cog',
            'controller' => 'index.php', 
            'unit' => 'setting',//boleh kosong default folder app/modul/public
            'module' => 'setting_jabatan',//.($_SESSION['hak_akses'] == ADMIN ? '&tahun='.(date("Y")-1):''),//undur 1 thn  
            'parent' => 'setting',
            'public' => true
        ),

        'setting_eselon' =>array(
            'caption' => 'Setting Eselon',
            'title' => 'Setting Eselon',
            'style' => 'icon-cog',
            'controller' => 'index.php', 
            'unit' => 'setting',//boleh kosong default folder app/modul/public
            'module' => 'manajemen_setting&parameter=ESELON',//.($_SESSION['hak_akses'] == ADMIN ? '&tahun='.(date("Y")-1):''),//undur 1 thn  
            'parent' => 'setting',
            'public' => true
        ), 

        'setting_kelas' =>array(
            'caption' => 'Setting Guru Kelas',
            'title' => 'Setting Guru Kelas',
            'style' => 'icon-cog',
            'controller' => 'index.php', 
            'unit' => 'setting',//boleh kosong default folder app/modul/public
            'module' => 'setting_kelas',//.($_SESSION['hak_akses'] == ADMIN ? '&tahun='.(date("Y")-1):''),//undur 1 thn  
            'parent' => 'setting',
            'public' => true
        ),*/

        'setting_pelajaran' =>array(
            'caption' => 'Setting Pelajaran',
            'title' => 'Setting Pelajaran',
            'style' => 'icon-cog',
            'controller' => 'index.php', 
            'unit' => 'setting',//boleh kosong default folder app/modul/public
            'module' => 'manajemen_setting&parameter=PELAJARAN',//.($_SESSION['hak_akses'] == ADMIN ? '&tahun='.(date("Y")-1):''),//undur 1 thn  
            'parent' => 'setting',
            'public' => true
        ),

    );

?>