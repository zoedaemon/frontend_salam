<?php
	/**
		NOTE: 
		-bedanya option public dan hide_when_login adalah :
			public = true maka akan ditampilkan saat belum login maupun sudah login
			public = false maka TIDAK akan ditampilkan saat belum login tp ditampilkan saat sudah login
			hide_when_login = true maka akan ditampilkan saat belum login tp akan disembunyikan saat sudah login
			hide_when_login = false maka akan DISEMBUNYIKAN saat belum login tp akan dimunculkan saat sudah login
	*/
	$menu_samping = array(

         'grafik_pegawai' =>array(
            'caption' => 'Grafik Pegawai',
            'title' => 'Grafik Data Kepegawaian',
            'style' => 'icon-signal',
            'controller' => 'index.php', 
            'unit' => 'si',//boleh kosong default folder app/modul/public
            'module' => 'grafik',//.($_SESSION['hak_akses'] == ADMIN ? '&tahun='.(date("Y")-1):''),//undur 1 thn  
            'parent' => 'grafik',
            'public' => true
        ), 

        'jenis_kelamin' =>array(
            'caption' => 'Jenis Kelamin',
            'title' => 'Grafik Data Jenis Kelamin',
            'style' => 'icon-signal',
            'controller' => 'index.php', 
            'unit' => 'si',
            'module' => 'jenis_kelamin',
            'parent' => 'grafik',//supaya mengarah ke plugin/menu/menu_si_pgw.php
            'public' => true
        ), 
    

        'grafik_sekolah' =>array(
            'caption' => 'Grafik Sekolah',
            'title' => 'Grafik Data Pegawai Sekolah',
            'style' => 'icon-signal',
            'controller' => 'index.php', 
            'unit' => 'si',
            'module' => 'grafik_sekolah',
            'parent' => 'grafik',
            'public' => true
        ),    

        'grafik_medis' =>array(
            'caption' => 'Grafik Medis',
            'title' => 'Grafik Data Pegawai Medis',
            'style' => 'icon-signal',
            'controller' => 'index.php', 
            'unit' => 'si',
            'module' => 'grafik_medis',
            'parent' => 'grafik',
            'public' => true
        ),

        'grafik_jumlah_kebutuhan' =>array(
            'caption' => 'Grafik Angka Kebutuhan',
            'title' => 'Grafik Angka Kebutuhan Pengajar',
            'style' => 'icon-signal',
            'controller' => 'index.php', 
            'unit' => 'si',
            'module' => 'grafik_jumlah_kebutuhan',
            'parent' => 'grafik',
            'public' => true
        ),            

    );

?>