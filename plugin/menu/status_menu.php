<?php
    /**
        Fungsi untuk pengecekan sesion sudah login atau belum (true sudah ; false belum)
    */
    function status_menu($public, $hide_when_login) {

        $show = true;
		
		

		//pengecekan session dilakukan jika HANYA jika option public diset dan bernilai false
		if ( isset($public) AND ! $public ) {
			$show = general_session(); //misalnya $_SESSION['username'] dan $_SESSION['password'] maka tampilkan private page
		}


		//sudah login
		if (sudah_login()) {

			//hidden jika sudah login
			if ( isset($hide_when_login) AND $hide_when_login )
				$show = false;

			//echo ">>$option[title]>>**". ( isset($hide_when_login) AND $hide_when_login ) ."**>>>(".var_dump($show).") <br/>";
		
		
		//belum login
		}else {

			//hidden jika tidak login
			if ( isset($hide_when_login) AND ! $hide_when_login )
				$show = false;
		}

		
        return $show;
    }
?>