<?php
	/**
		NOTE: 
		-bedanya option public dan hide_when_login adalah :
			public = true maka akan ditampilkan saat belum login maupun sudah login
			public = false maka TIDAK akan ditampilkan saat belum login tp ditampilkan saat sudah login
			hide_when_login = true maka akan ditampilkan saat belum login tp akan disembunyikan saat sudah login
			hide_when_login = false maka akan DISEMBUNYIKAN saat belum login tp akan dimunculkan saat sudah login
	*/
	$menu_samping = array(

        'universitas' =>array(
            'caption' => 'Universitas',
            'title' => 'Manajemen Data Universitas',
            'style' => 'icon-th',
            'controller' => 'index.php', 
            'unit' => 'si',//boleh kosong default folder app/modul/public
            'module' => 'universitas',//.($_SESSION['hak_akses'] == ADMIN ? '&tahun='.(date("Y")-1):''),//undur 1 thn  
            'parent' => 'universitas',
            'public' => true
        ), 



        'fakultas' =>array(
            'caption' => 'Fakultas',
            'title' => 'Manajemen Data Fakultas',
            'style' => 'icon-th',
            'controller' => 'index.php', 
            'unit' => 'si',//jurusanoleh kosong default folder app/modul/public
            'module' => 'fakultas',//.($_SESSION['hak_akses'] == ADMIN ? '&tahun='.(date("Y")-1):''),//undur 1 thn  
            'parent' => 'universitas',
            'public' => true
        ), 

        'jurusan' =>array(
            'caption' => 'Jurusan',
            'title' => 'Manajemen Data Jurusan',
            'style' => 'icon-asterisk',
            'controller' => 'index.php', 
            'unit' => 'si',//boleh kosong default folder app/modul/public
            'module' => 'jurusan',//.($_SESSION['hak_akses'] == ADMIN ? '&tahun='.(date("Y")-1):''),//undur 1 thn  
            'parent' => 'universitas',
            'public' => true
        ),


    );

?>