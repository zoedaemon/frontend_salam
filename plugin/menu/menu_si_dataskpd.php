<?php
	/**
		NOTE: 
		-bedanya option public dan hide_when_login adalah :
			public = true maka akan ditampilkan saat belum login maupun sudah login
			public = false maka TIDAK akan ditampilkan saat belum login tp ditampilkan saat sudah login
			hide_when_login = true maka akan ditampilkan saat belum login tp akan disembunyikan saat sudah login
			hide_when_login = false maka akan DISEMBUNYIKAN saat belum login tp akan dimunculkan saat sudah login
	*/
	$menu_samping = array(

        'dataskpd' =>array(
            'caption' => 'List Pegawai SKPD',
            'title' => 'List Pegawai SKPD',
            'style' => 'icon-file',
            'controller' => 'index.php', 
            'unit' => 'si',//boleh kosong default folder app/modul/public
            'module' => 'dataskpd',//.($_SESSION['hak_akses'] == ADMIN ? '&tahun='.(date("Y")-1):''),//undur 1 thn  
            'parent' => 'dataskpd',
            'public' => true
        ),

        'skpdjabatan' =>array(
            'caption' => 'Daftar Jabatan SKPD',
            'title' => 'Daftar Jabatan SKPD',
            'style' => 'icon-file',
            'controller' => 'index.php', 
            'unit' => 'si',//boleh kosong default folder app/modul/public
            'module' => 'skpdjabatan',//.($_SESSION['hak_akses'] == ADMIN ? '&tahun='.(date("Y")-1):''),//undur 1 thn  
            'parent' => 'dataskpd',
            'public' => true
        ),

        'skpdnamajabatan' =>array(
            'caption' => 'Daftar Nama Jabatan SKPD',
            'title' => 'Daftar Nama Jabatan SKPD',
            'style' => 'icon-file',
            'controller' => 'index.php', 
            'unit' => 'si',//boleh kosong default folder app/modul/public
            'module' => 'skpdnamajabatan',//.($_SESSION['hak_akses'] == ADMIN ? '&tahun='.(date("Y")-1):''),//undur 1 thn  
            'parent' => 'dataskpd',
            'public' => true
        ), 

        'skpdgolongan' =>array(
            'caption' => 'Daftar Golongan SKPD',
            'title' => 'Daftar Golongan SKPD',
            'style' => 'icon-file',
            'controller' => 'index.php', 
            'unit' => 'si',//boleh kosong default folder app/modul/public
            'module' => 'skpdgolongan',//.($_SESSION['hak_akses'] == ADMIN ? '&tahun='.(date("Y")-1):''),//undur 1 thn  
            'parent' => 'dataskpd',
            'public' => true
        )

    );

?>