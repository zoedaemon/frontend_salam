<?php

include_once "config/koneksi.php";
include "lib/library.php";
include "lib/fungsi_indotgl.php";
include  "lib/fungsi_combobox.php";
include "lib/paging.php";
include "lib/lib_zHakAkses.php";
//include_once "lib/lib_general.php";

use lib\zHakAkses;


$submodule = $path['submodule'];
$act = $path['act'];

//cek unit default 
if ( isset($path['unit']) AND ! empty($path['unit']) )
  $unit = $path['unit'];
else
  $unit = 'main';

if ( isset($path['module']) AND ! empty($path['module']) )
  $module = $path['module'];
else
  $module = 'home';


$file = "";

//cek halaman (module) apa aksi (submodule)
if ( !empty($module) ) {

  if(!empty($submodule))
    //TODO: perlu pakai submodul dan folder juga...?
    //TODO : ubah menjadi $folder/$submodule/$submodule.php 
      
    if ( isset($path['cnt'] ) )
      $file = "modul/$unit/$module/controller.php";
    else
      $file = "modul/$unit/$module/$submodule.php";
    
  else {
      $file = "modul/$unit/$module/index.php";
  }

}else{
    $file = "modul/$unit/home/index.php";
}




//////////////// BEGIN CEK HAK AKSES
$resource = "$unit-$module";
/*
$resource .= ($submodule ? '-'.$submodule : '');

zHakAkses::init_check ($_SESSION['hak_akses'], $resource);
if ( zHakAkses::isAllowed($_SESSION['hak_akses'], $resource, VIEW) ) {  

  //TODO: Lihat nilai resource raport, ada yg aneh...
//  echo '<br/>@@@@@@@@@ zHakAkses_isAllowed  ('.$_SESSION['hak_akses'].') : '.$resource.' <> '.zHakAkses::isAllowed($_SESSION['hak_akses'], $resource, VIEW).'<br/>';
  ;//terlewati

}else { 
 // echo '<br/>@@@@@@@@@ zHakAkses_isAllowed  ('.$_SESSION['hak_akses'].') : '.$resource.' <> '.zHakAkses::isAllowed($_SESSION['hak_akses'], $resource, VIEW).'<br/>';
  echo "Anda TIDAK dizinkan mengakses konten ini";  
  return;
}*/
//echo $resource;
//////////////// END CEK HAK AKSES



  

//echo $file;
//cek apakah file ada, jika gak ada tampilkan halaman tidak ditemukan
if ( file_exists(__DIR__ . '/' . $file) )
  include $file;
else {
  include "modul/error_page/404.php";
  //echo $file;
}




?>
