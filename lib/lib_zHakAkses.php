<?php
/**
@zod
zHakAkses v0.1 
*/
namespace lib;

//WAJIB casesensitive
define('DENY', 0, true);
define('SUPER_ADMIN', 1, true);
define('VIEW', 2, true);
define('INSERT', 4, true);
define('EDIT', 8, true);
define('DELETE', 16, true);

class zHakAkses
{
	function init() {
		//TODO: perlu implementasi PDO ato mysqli ???
	}

	/*
	Tambah user dengan role dgn default khusus jika ada
	*/
	static function role_add($role, $access = 0) {
		mysql_query("INSERT INTO role(role, default_access) VALUES ('$role',$access)");
	}

	/*
	Tambah resource atau path berdasarkan role dan hak akses
	*/
	static function resource_add($role, $resource, $access) {
				
		$query = mysql_query("SELECT * FROM role WHERE role = '$role'");
		
		$data = mysql_fetch_array($query);
		
		$query = "INSERT INTO resource(id_role, resource, access) VALUES ($data[id], '$resource', $access)";
		//echo $query;
		
		mysql_query($query);
		

	}

	/*
	neh fungsi biar gak kebnyakan select d fungsi zHakAkses_isAllowed, jadi disimpan di
	dalam session  bernama  $resource.'access' dan $resource.'default_access'
	*/
	static function init_check ($role, $resource) {
		
		$query = mysql_query("SELECT * FROM role WHERE role = '$role'");
		$data = mysql_fetch_array($query);
		
		$id_role = $data['id'];
		$default_access = $data['default_access'];
		
		$query = mysql_query("SELECT * FROM resource WHERE id_role = '$id_role' AND resource = '$resource' ");
		$data = mysql_fetch_array($query);
		
		//echo ($_SESSION[ $resource ] = $id_role) ."<br/>";
		$_SESSION[ $resource.'access' ] 	=  	$data['access'];
		$_SESSION[ $resource.'default_access' ] 	= $default_access;
		

	}

	/*
	cek hak akses misal pada modul saat ini (current_module) berdasarkan nilai hak akses 
	yg tersimpan di session yg di set oleh fungsi  self->init_check()
	*/ 
	static function isAllowed($role, $resource, $access) {
		
		/*
		echo $_SESSION[ $resource ] ."<br/>";
		echo $_SESSION[ $resource.'access' ]."<br/>";
		echo $_SESSION[ $resource.'default_access'];
		*/
		
		if ( isset($_SESSION[$resource]) ) {
			
			//$id_role = $_SESSION[ $resource ];
			$default_access = $_SESSION[ $resource.'default_access'];
			$data_access = $_SESSION[ $resource.'access'];
			
		}else {
			
			$query = mysql_query("SELECT * FROM role WHERE role = '$role'");
			$data = mysql_fetch_array($query);
			
			$id_role = $data['id'];
			$default_access = $data['default_access'];
			
			$query = mysql_query("SELECT * FROM resource WHERE id_role = '$id_role' AND resource = '$resource' ");
			$data = mysql_fetch_array($query);
			$data_access = $data['access'];
			
		}
		
		//cek dengan operasi bitwise cuy.... :D
		if ($data_access != NULL AND (($data_access & $access) == $access) ) {
			/*echo "data['access'] ==  $data[access]<br/>";
			echo $data['access']."<br/>";
			echo $access."<br/>";
			echo ($data['access'] & $access) == $access."<br/>";
			*/
			return 1;
		}elseif ($default_access > 0) { //pengecekan khusus jika ada default hak akses
			if (($default_access & $access) == $access) //hak akses non SUPER_ADMIN
				return 1;
			elseif($default_access == SUPER_ADMIN)
				return 1;
		}
		
	return 0;
	}

	/*
	cek hak akses misal pada modul saat ini (current_module) TAPI tidak ada ketergantungan terhadap
	fungsi self->init_check() jadi tidak ada pengecekan ke session yang bernama bernama  
	$resource.'access' dan $resource.'default_access'
	*/
	static function isAllowed_nosession($role, $resource, $access) {
		
		//ambil id role
		$query = mysql_query("SELECT * FROM role WHERE role = '$role'");
		$data = mysql_fetch_array($query);
		//copy gak berguna (TODO : query kedua pake variabel laen aza cuy)
		$id_role = $data['id'];
		$default_access = $data['default_access'];
		//dapatkan resource
		$query = mysql_query("SELECT * FROM resource WHERE id_role = '$id_role' AND resource = '$resource' ");
		$data = mysql_fetch_array($query);
		
		//cek dengan operasi bitwise cuy.... :D
		if ($data['access'] != NULL AND (($data['access'] & $access) == $access) ) {
			/*echo "data['access'] ==  $data[access]<br/>";
			echo $data['access']."<br/>";
			echo $access."<br/>";
			echo ($data['access'] & $access) == $access."<br/>";
			*/
			return 1;
		}elseif ($default_access > 0) { //pengecekan khusus jika ada default hak akses
			if (($default_access & $access) == $access) //hak akses non SUPER_ADMIN
				return 1;
			elseif($default_access == SUPER_ADMIN)
				return 1;
		}
	return 0;
	}



	//TODO : Operasi UPDATE OR
	function allow($role, $resource, $access){
		
	}

	//TODO : Operasi UPDATE XOR ($a ^ $b) pada posisi bit tertentu, 
	/*
		contoh hak akses JANGAN beri hak akses EDIT:
		11110 (DELETE, EDIT, INSERT, VIEW, 0)
		01000  (0, EDIT, 0, 0, 0)
		--------XOR (^)
		10110
		
		
		contoh hak akses JANGAN beri hak akses DELETE dan EDIT:
		11110 (DELETE, EDIT, INSERT, VIEW, 0)
		11000  (DELETE, EDIT, 0, 0, 0)
		--------XOR (^)
		00110
		
		
	*/
	function deny($role, $resource, $access) {
		
		
	}

}






?>