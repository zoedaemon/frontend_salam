<?php
namespace lib;

class timeout
{
    static function timer(){
		$time=60*60;
		$_SESSION['timeout']=time()+$time;
	}

	static function cek_login(){
		$t=$_SESSION['timeout'];
		if(time()<$t){
			timeout::timer();
			return true;
		}else{
			unset($_SESSION['timeout']);
			return false;
		}
	}
}
?>