<?php

	function tgl_indo($tgl){
			$tanggal = substr($tgl,8,2);
			$bulan = getBulan(substr($tgl,5,2));
			$tahun = substr($tgl,0,4);
			return $tanggal.' '.$bulan.' '.$tahun;		 
	}
	
	function getBulan($bln){
				switch ($bln){
					case 1: 
						return "Januari";
						break;
					case 2:
						return "Februari";
						break;
					case 3:
						return "Maret";
						break;
					case 4:
						return "April";
						break;
					case 5:
						return "Mei";
						break;
					case 6:
						return "Juni";
						break;
					case 7:
						return "Juli";
						break;
					case 8:
						return "Agustus";
						break;
					case 9:
						return "September";
						break;
					case 10:
						return "Oktober";
						break;
					case 11:
						return "November";
						break;
					case 12:
						return "Desember";
						break;
				}
			} 
	
	//cuman fungsi tukar nilai
	function swap_var ( &$var_first, &$var_end ) {

		//algoritma sederhana :D
		$temp = $var_first;
		$var_first = $var_end;
		$var_end = $temp;

	}
	
	//proses tanggal sebelum disimpan
	function tgl_to_db($tgl, $view_delimiter, $db_delimiter)
	{

			$tanggal = explode($view_delimiter, $tgl);
			
	/*		
			echo '<br>$tanggal[0]'.$tanggal[0].'<br>';
			echo '<br>$tanggal[1]'.$tanggal[1].'<br>';
			echo '<br>$tanggal[2]'.$tanggal[2].'<br>';
	*/
			//hnya index selain index pertama akan dilakukan swap data tahun
			if ( $tanggal[0] < 1000){
				//cek d index keberapa tahun berada
				if ( ($index1 = ($tanggal[1] > 99) ) OR ($index2 = ($tanggal[2] > 99) ) ) {
					//doswap
					$index1 ? swap_var($tanggal[0], $tanggal[1]) : '';
					$index2 ? swap_var($tanggal[0], $tanggal[2]) : '';
				}

					/*echo '<br>'.sizeof($tanggal[2]).':PPP';
					var_dump($index1);
					var_dump($index2);
					echo '<br>';*/
			}
			/*echo '<br>$tanggal[0]'.$tanggal[0].'<br>';
			echo '<br>$tanggal[1]'.$tanggal[1].'<br>';
			echo '<br>$tanggal[2]'.$tanggal[2].'<br>';
	*/
			//format tahun-bulan-hari
			return $tanggal[0].$db_delimiter.$tanggal[1].$db_delimiter.$tanggal[2];		 
	}


	//proses tanggal sebelum ditampilkan diform edit
	function tgl_to_view($tgl, $view_delimiter, $db_delimiter)
	{

			$tanggal = explode($db_delimiter, $tgl);
			
			//hnya index selain index pertama akan dilakukan swap data tahun
			//DONE: fixit salah neh tiru tgl_to_db
			if ( count($tanggal[0]) != 4){
				//cek d index keberapa tahun berada
				if ( ($index1 = ($tanggal[1] > 99) ) OR ($index2 = ($tanggal[2] > 99) ) ) {
					//doswap
					$index1 ? swap_var($tanggal[0], $tanggal[1]) : '';
					$index2 ? swap_var($tanggal[0], $tanggal[2]) : '';
				}

			}


			//format hari-bulan-tahun
			return $tanggal[2].$view_delimiter.$tanggal[1].$view_delimiter.$tanggal[0];		 
	}
?>