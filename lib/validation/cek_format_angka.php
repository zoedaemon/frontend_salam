<?php

	/**
	Fungsi tuk ngecek $num apakah numerik atau tidak
	*/
	function cek_format_angka($num) {
		
		if (is_numeric($num))
			return $num;
		else {

			 throw new Exception('"'.$num.'" bukan angka, cek nilai masukan anda');
			 return $num;
		} 
		
	}
	
	
	/**
	Fungsi tuk ngecek $tgl apakah tanggal dengan format hari{$delimiter}bulan{$delimiter}tahun
	tiap data selain delimiter harus angka
	*/
	function cek_tgl($tgl, $delimiter) {
		
		$tanggal = explode($delimiter, $tgl);

		if ( count($tanggal[0]) > 2 ) {
			throw new Exception('"'.$tgl.'" format HARI salah, pastikan kurang dari 2 digit');
			return $tgl;
		}

		if ( count($tanggal[1]) > 2 ) {
			throw new Exception('"'.$tgl.'" format BULAN salah, pastikan kurang dari 2 digit');
			return $tgl;
		}


		if ( count($tanggal[2]) > 4 ) {
			throw new Exception('"'.$tgl.'"    TAHUN salah, pastikan kurang dari 4 digit');
			return $tgl;
		}

		//TODO: pengecekan validasi data misal : 31-2-2014 belum bisa

		foreach ($tanggal as $t) {    
			if (is_numeric($t))
				continue;
			else {

				 throw new Exception('"'.$tgl.'" bukan tanggal, gunakan format "hari'.$delimiter.'bulan'.$delimiter.'tahun"');
				 return $tanggal;
			} 
		}

		return $tanggal[0].$delimiter.$tanggal[1].$delimiter.$tanggal[2];
		
	}  
 	
?>     