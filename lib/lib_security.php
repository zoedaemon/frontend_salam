<?php
namespace lib;

include 'password_hashing.php';
include 'defined_session.php';
require_once 'lib_general.php';//gunakan require_once tuk mencegah duplikat pemanggilan kelas lib\general            
       

//const FOO = 1;
//function foo() {}
class security
{
    static function anti_injection($mysqli, $data){
	  $filter = mysqli_real_escape_string($mysqli, stripslashes(strip_tags(htmlspecialchars($data,ENT_QUOTES))));
	  return $filter;
	}


    static function mcrypt_en($data, $key, & $iv_size){
	     # create a random IV to use with CBC encoding
	    $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
	    $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
	    
	    # creates a cipher text compatible with AES (Rijndael block size = 128)
	    # to keep the text confidential 
	    # only suitable for encoded input that never ends with value 00h
	    # (because of default zero padding)
	    //echo $iv.';'.$iv_size.'<br/>';
	    $ciphertext = mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $key,
	                                 $data, MCRYPT_MODE_CBC, $iv);

	    # prepend the IV for it to be available for decryption
	    $ciphertext = $iv . $ciphertext;
	    $ciphertext_base64 = base64_encode($ciphertext);
	    # encode the resulting cipher text so it can be represented by a string
	  return $ciphertext_base64 ;
	}

	
    static function mcrypt_dec($enc_data, $key, $iv_size){

		# === WARNING ===
	    # Resulting cipher text has no integrity or authenticity added
	    # and is not protected against padding oracle attacks.
	    # --- DECRYPTION ---
	    
	    $ciphertext_dec = base64_decode($enc_data);
	    
	    # retrieves the IV, iv_size should be created using mcrypt_get_iv_size()
	    $iv_dec = substr($ciphertext_dec, 0, $iv_size);
	    
	    # retrieves the cipher text (everything except the $iv_size in the front)
	    $ciphertext_dec = substr($ciphertext_dec, $iv_size);

	    # may remove 00h valued characters from end of plain text
	    $data = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key,
	                                    $ciphertext_dec, MCRYPT_MODE_CBC, $iv_dec);
	    
	    //echo  $plaintext_dec . "<br/>";

	  return $data;
	}

	static function mcrypt_en_urlencode($base64){
		return urlencode($base64);
	}


	static function mcrypt_dec_urldecode($base64){
		return strtr(urldecode($base64), ' ', '+');
	}

	function has_logged_in(){
		//if ($_SESSION[SESSLOGIN])//BUG kok SESSLOGIN gak kebaca
		if ($_SESSION['login.'.$_SESSION[SESSID]])
			return true;
		return false;
	}

	function get_secured_session_id(){
		//TODO: apakah salt_before_login bisa disimpan di storage/sql browser (modern browser) jd tau klo benar2 login gak malingan
		return md5(general::get_client_ip().':'.$_SESSION['salt_before_login.'.$_SESSION[SESSID]].':'.
                      			$_SERVER['HTTP_USER_AGENT']);			
	}


	function check_login_force_quit(){
		if ( ! security::has_logged_in() OR session_id() != security::get_secured_session_id()
			) {	
			//echo security::has_logged_in().'::'.session_id().'::'. security::get_secured_session_id();
			include "modul/error_page/404.php";
		    exit();
		}
		return true;
	}

	static function get_ec($base64_enc){
		return security::mcrypt_dec(security::mcrypt_dec_urldecode($base64_enc), 
									$_SESSION['key.'.$_SESSION[SESSID]], 
									$_SESSION['iv_size.'.$_SESSION[SESSID]]);
	}

	static function set_ec($base64_dec){
		return security::mcrypt_en_urlencode(security::mcrypt_en($base64_dec, 
									$_SESSION[SESSKEY], 
									$_SESSION[SESSIVSIZE]));
	}
}
?>