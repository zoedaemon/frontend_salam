<?php
session_start();

//load error reporting setting
include "config/error_reporting.php";

//koneksi
include "config/koneksi.php";

//autoload library phpfastcache
//include "vendor/autoload.php";
//use phpFastCache\CacheManager;
//use phpFastCache\Core\phpFastCache;

//loader untuk otomatis load general dan libs lainnya
//include "lib/loader.php";
//ato load langsung general.php nya
include "lib/lib_general.php";
include "lib/lib_security.php";
include "lib/lib_timeout.php";

//konstanta hak akses
include "plugin/hak_akses/define.php";

//NOTE: Untuk Hak akses dan pengecekannya
include "lib/lib_zHakAkses.php";//belum ditest

use lib\general;
use lib\security;
use lib\timeout;  

$username = security::anti_injection($mysqli, $_POST['username']);
$pass     = security::anti_injection($mysqli, $_POST['password']);

// pastikan username dan password adalah berupa huruf atau angka.
if (!ctype_alnum($username) OR !ctype_alnum($pass)) {
      
      session_start();
      session_destroy();
      echo "<link href='public/css/style.css' rel=stylesheet type=text/css>";
      echo "<center>LOGIN GAGAL! <br> 
            Username atau Password Anda tidak benar.<br>";
      echo "Username dan Password harus teks atau angka, bukan karakter tanda baca</center>";
      echo "<center><b>[MENGULANGI KEMBALI USERNAME DAN PASSWORD YANG SALAH]</b></center>";
      echo "<center><h5>Silahkan tunggu beberapa saat anda akan diarahkan ke halaman login.</h5></center>";
      echo "<script>setTimeout(function(){location.href='index.php';},3000);</script>";
}
else
{
      //$InstanceCache = CacheManager::getInstance('apc');

      //$CachedString = $InstanceCache->getItem($_POST['username']);
      
      /*if ($CachedString->get()) {

          if (isset($_GET['TEST'])) {
            echo "READ FROM CACHE //";
            //echo $CachedString->get();
          }
          $ketemu = 1;
          $type = ADMIN;
          
          $r['username'] = $_POST['user'];
          $r['password'] = $CachedString->get();
          
      }else {
      */  

          if (isset($_SESSION['key'])) {
            $key = $_SESSION['key'];
            $secret_path=security::mcrypt_dec(security::mcrypt_dec_urldecode($_GET['ec']), $key, $_SESSION['iv_size']);
            $secret_path = explode('&', $secret_path);
            $salt = explode('=', $secret_path['0']);
            if (strncmp($salt[1], $_SESSION['salt'],32 ) == 0)
              $salt_valid = true;
            else
              $salt_valid = false;
          }


          $sql = "SELECT * FROM pengguna WHERE username='$username' AND password=MD5('$pass')";
          $qry = $mysqli->query($sql);
          $ketemu=$qry->num_rows;
          //echo $sql;
          if ($ketemu <= 0) 
          {
               $type  = 'invalid';                  
          }
          else {
            $r=$qry->fetch_array(MYSQLI_BOTH);
            //$type  = ADMIN;//admin bisa ditambahkan di modul Profil, bukan bagian dari SI
            if ($r['type'] == ADMIN OR $r['type'] == AUDITOR)
              $type = $r['type'];
            //else {
            //  $type = SKPD;
            //  $singkatan_skpd = $r['type'];
            //}
          }
           
      //}

      
      // Apabila username dan password ditemukan
      if ($ketemu > 0 AND $type != 'invalid' AND $salt_valid)
      {

        //$_SESSION['singkatan_skpd'] = $singkatan_skpd;
        
        //$_SESSION['id']     = $r['id_pengguna'];

//////////////////////////////////////

        //cek data di cache sesuai dengan key yg sama di atas (getItem function)
        /*if (is_null($CachedString->get())) {
            //data akan expired dalam 60 detik
            $CachedString->set($_SESSION['passuser'])->expiresAfter(60);
            $InstanceCache->save($CachedString);
            if (isset($_GET['TEST'])) {
              echo "FIRST LOAD // WROTE OBJECT TO CACHE // RELOAD THE PAGE AND SEE // ";
              //echo $CachedString->get();
            }
        }*/

///////////////////////////////////////
        

      	//$sid_lama = session_id();//TODO : boleh di hapus ???
      	//session_regenerate_id();        
        //$sid_baru = session_id();
        $salt_before_login = $_SESSION['salt'];//overprotective XD
        session_destroy();//harus dinonaktifkan dulu seblum set session_id baru
        $sid_baru = md5(general::get_client_ip().':'.$salt_before_login.':'.$_SERVER['HTTP_USER_AGENT']);
        session_id($sid_baru);
        session_start();

        $qry = $mysqli->query("UPDATE pengguna SET id_session='$sid_baru', last_ip_address='".general::get_client_ip()."' 
                              WHERE username='$username'");

        //$_SESSION['user']     = $r['username'];
        //$_SESSION['passuser'] = $r['password'];//TODO: apakah aman ???
        $_SESSION['id_session']     = $sid_baru;
        $_SESSION['salt_before_login.'.$_SESSION['id_session']] = $salt_before_login;
        $_SESSION['user.'.$_SESSION['id_session']]     = $r['username'];
        $_SESSION['hak_akses.'.$_SESSION['id_session']] = $type;          
        $_SESSION['login.'.$_SESSION['id_session']] = true;

        //generate ulang key tp tetap gunakan iv_size yg sama seperti seblum login
        $salt = base64_encode(mcrypt_create_iv(PBKDF2_SALT_BYTE_SIZE, MCRYPT_DEV_URANDOM));
        $key = pbkdf2(
                PBKDF2_HASH_ALGORITHM,
                PBKDF2_HASH_BYTE_REGEX,
                $salt,
                PBKDF2_ITERATIONS,
                PBKDF2_HASH_BYTE_SIZE,
                true
              );
        //NOTE: cukup gunakan sid tuk pindah2 menu, tp gunakan salt jika mengeksekusi operasi CRUD
        //ambilmenu
        include "plugin/menu/menu.php";
        $header_title = $menu['aspirasi_date']['sub']['aspirasi_date']['header'];
        $dat = "sid=$sid_baru&unit=main&module=aspirasi_date&header=$header_title&\0=\0";//ada string berlebih hasil decryp
        $enc = security::mcrypt_en($dat, $key, $_SESSION['iv_size.'.$_SESSION['id_session']]);//NOTE: iv_size gak perlu di backup coz pass by reference 
        $_SESSION['key.'.$_SESSION['id_session']] = $key;

        //aktifkan timer spaya logout otomatis
        timeout::timer();
        
        //Memproses HAK AKSES
        //include "plugin/hak_akses/init.php";

        //NOTE: penanda sudah login apa blum --- TODO: kyknya mubajir coba cek di fungsi status_menu() 
        
        //redirect berdasarkan tipe user
        /*if ($_SESSION['hak_akses'] == SISWA)
          header('location:SI/index.php?unit=si&module=data_nilai&submodule=view&id='.$_SESSION['id']);
        elseif ($_SESSION['hak_akses'] == GURU)
          header('location:SI/index.php?unit=si&module=data_nilai');
        else
        */  
        header('location:index.php?ec='.security::mcrypt_en_urlencode($enc));
      }
      else
      {
        
        session_destroy();
        echo "<link href='public/css/style.css' rel=stylesheet type=text/css>";
        echo "<center>LOGIN GAGAL! <br> 
              Username atau Password Anda salah.<br>";

        echo "<center><b>ISI ULANG KEMBALI USERNAME DAN PASSWORD DENGAN TEPAT</b></center>";
        echo "<center><h5>Silahkan tunggu beberapa saat anda akan diarahkan ke halaman login.</h5></center>";
        echo "<script>setTimeout(function(){location.href='login.php';},3000);</script>";
      }


}
?>
