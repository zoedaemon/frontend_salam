<?php

include_once "plugin/hak_akses/define.php";


include_once "config/koneksi.php";

include_once "plugin/menu/menu.php"; 
include_once "plugin/menu/path_generator.php";
include_once "plugin/session/general_session.php"; 
include_once "plugin/session/sudah_login.php"; 
include_once "plugin/menu/status_menu.php"; 

//include_once "lib/lib_zHakAkses";
//$page  = $_GET['module'];//TODO: HAPUS neh nanti

//use lib\zHakAkses;

$unit = $_GET['unit'];
$module = $_GET['module'];

if ( ! isset($menu[ $unit ]) ) 
		$unit = "si";	


if ( ! isset($menu[ $unit ][$module]) ) 
		$module = "home";	



// BEGIN manipulasi submodule
$use_submodule = false;
$module = explode('-', $module);
if ( sizeof($module) > 1) {
	$submodule = $module[1];
	$use_submodule = true;	
}
$module = $module[0];
//END manipulasi submodule


	//TODO perlu pengecekan zHakAkses dan custom tuk logout dan login
	foreach ($menu as $menu => $option) {


		//$_SESSION['login'] = true;//TEST status sudah login
	


		//jika ada sub menunya
		if ( isset($option['sub']) ) {


			//echo(zHakAkses_isAllowed_nosession( $_SESSION['hak_akses'], "$option[unit]-$option[module]", VIEW));
			//jika no_check_access == false maka perlu cek hak akses menu, jika false maka lewatkan penampilan menu
            if ( ! $option['no_check_access'] AND ! isset($option['function_visibility']) )// AND ! zHakAkses::isAllowed_nosession( $_SESSION['hak_akses'], "$option[unit]-$option[module]", VIEW) ) // 
                 continue;

            //hak akses diabaikan dan cek berdasarkan function_visibility untuk penampilannya
			if(isset($option['function_visibility'])) {
				$function_visibility = $option['function_visibility'];
				if ($function_visibility() == false) {
					continue;
				}			
			}

			//mencoba menampilkan menu berdasarkan option public dan hide_when_login			                
            if ( status_menu( $option['public'], $option['hide_when_login'] )  ) {
                
            	// misal tuk notifikasi tambahkan : <span class="label label-important">5</span> 
                echo '
                		<li class="dropdown" id="menu-'.$menu.'"><a href="#" data-toggle="dropdown" data-target="#menu-'.$menu.'" class="dropdown-toggle">
    						<i class="icon '.$option['style'].'"></i> <span class="text">'.$option['caption'].'</span></a>
      
		                    <ul class="dropdown-menu" style="right: auto; left: 0;">
		                   
		        	';
		        
		        //telusuri semua SUB menu
		        foreach ($option['sub'] as $submenu => $submenu_option) {
    			

					//jika no_check_access == false maka perlu cek hak akses menu, jika false maka lewatkan penampilan menu
		            //if ( ! $option['no_check_access'] AND ! zHakAkses_isAllowed_nosession( $_SESSION['hak_akses'], "$submenu_option[unit]-$submenu", VIEW) ) 
		            //     continue;			

    				//mencoba menampilkan SUB menu berdasarkan option public dan hide_when_login	
    				//if ( status_menu( $submenu_option['public'], $submenu_option['hide_when_login'] )  ) {
    					
    					//NOTE: sub menu harus dicek juga
		            	//if ( ! $option['no_check_access'] AND zHakAkses::isAllowed_nosession( $_SESSION['hak_akses'], "$submenu_option[unit]-$submenu", VIEW) ) 
		                 	echo '
				                        <li>
				                            <a href="'.path_generator_menu_bawah($submenu_option['controller'], $submenu_option['unit'], $submenu_option['module'], $submenu_option['parent'], $submenu_option['header']).'"
				                            	title="'.$option['title'].'">
				                            	<i class="icon '.$submenu_option['style'].'"></i>
				                            	<span class="text">'.$submenu_option['caption'].'
				                            	</span>
				                            </a> 
				                           
				                        </li>
				                ';
		            //}
		        }

		        echo '
		                    </ul>
	                	</li>
	                ';
		    }


		//tanpa sub menu
		}else {


			$resource = "$option[unit]-";
			
			$modules = explode('&', $option['module']);
			
			if ( sizeof($modules) > 1) {

				$acts = explode('=', $modules[1]);
				$act = $acts[1];	
			
				$resource .= $modules[0];
				$resource .= '-'.$acts[1];
				//echo 'resource'.$resource.'<br/>';
				//print_r( zHakAkses::isAllowed_nosession( $_SESSION['leveluser'], $resource, VIEW) );

			}else
				$resource .= $option['module'];
			
			//echo '$resource='.$resource.' $_SESSION[hak_akses]='.$_SESSION['hak_akses'].' ---- '.
			//	zHakAkses::isAllowed_nosession( $_SESSION['hak_akses'], $resource, VIEW).'<br/>';

			//jika no_check_access == true maka perlu cek hak akses menu, jika false maka lewatkan penampilan menu
			//tapi jika function_visibility ada maka jangan lewatkan
            if ( ! $option['no_check_access'] AND ! isset($option['function_visibility']) ) 
            	//AND ! zHakAkses::isAllowed_nosession( $_SESSION['hak_akses'], $resource, VIEW) ) 
                 continue;			

            //hak akses diabaikan dan cek berdasarkan function_visibility untuk penampilannya
			if(isset($option['function_visibility'])) {
				$function_visibility = $option['function_visibility'];
				if ($function_visibility() == false) {
					continue;
				}			
			}


			
			if ( status_menu( $option['public'], $option['hide_when_login'] ) ) {
    			//Generate menu yang gak memiliki sub menu
    			echo '
	                <li class="topmenu"> 
	                	<a href="'.path_generator_menu_bawah($option['controller'], $option['unit'], $option['module'], $option['parent'], $option['header']).'"
				                            	title="'.$option['title'].'">
				                            	<i class="icon '.$option['style'].'"></i>
				                            	<span class="text">'.$option['caption'].'
				                            	</span>
				        </a>
	                </li>


	                ';

/*
				echo '<li class="topmenu"><a  href="home.php?module=home" style="height:32px;line-height:32px;">
					<img src="../admin_menu_files/css3menu1/home22.png" alt=""/>Beranda</a></li>';
	*/
            }
		}


	}



?>