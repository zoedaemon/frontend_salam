<?php
session_start();
include 'config/koneksi.php';
include 'config/error_reporting.php';
include 'lib/lib_general.php';            
include 'lib/lib_timeout.php';
include 'lib/lib_security.php';
include 'lib/defined_session.php';

use lib\general;
use lib\timeout;
use lib\security;

if (!empty($_SESSION[SESSKEY])) {
  $secret_path = security::get_ec($_GET['ec']);
  $path = general::parsing_url($secret_path);
  if (strncmp($path['sid'], $_SESSION[SESSID],32 ) == 0)
    $salt_valid = true;
  else
    $salt_valid = false;
}

//echo session_id().':::'.$secure_session;
if (session_id() != security::get_secured_session_id())
  $hash_session_valid = false;
else
  $hash_session_valid = true;

//hapus session sudah login jika melewati timeout
if( general::get_login() == 1 AND $salt_valid AND $hash_session_valid){
  if( ! timeout::cek_login()){
    general::set_login(0);
    echo $_SESSION[SESSLOGIN];
  }
}else{
  include "modul/error_page/404.php";
  return;
}
 
$html ='

<!DOCTYPE HTML>
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr">
<head>

  <title>Dinas</title>


  <style type="text/css">
  
  table {
    font-family: "Open Sans", sans-serif;    
    border-collapse: collapse;
    /*
    Ooppps bikin error TODO: hapus ini pada commit berikutnya
    text-align: center;
    position: absolute;
    */
    
  }

  table, th, td {
      border: 1px solid black;
  }


  th,td {
    padding:5px;
    font-size:10px;
  }

  .center {
    text-align: center;
  }
  .b {
    font-weight: bold;
  }
  .warna {
    background-color: grey;
    color: white;
  }


  #footer {
    text-align: center;
    height: 75px;
    width: 1260px;
    position: absolute;
    color:#ffffff;
    font-weight:bold;
  
  }

   #content { 
    font-family: Arial, Helvetica, sans-serif;
    font-size: 14px;
    width: 1260px;
    text-align: center;
    position: absolute;
    margin-top:10px;
  }

</style>

</head>

<body>
<div id="wrapper">
<div id="header"></div> 
<div id="content" align="center">
  
';

  
  if ($_GET['module'] != 'jadwal_pelajaran_detail')
    $html .= '<div id="header">
                <div style="border-bottom:2px solid black; width:95%;margin-left: auto;margin-right: auto;">
                <div style="width:90px;height:90px;float:left;">
                    <img height="100px" src="public/images/pemkot.png" >
                </div>
                <!--<div style="width:90px;height:90px;float:right;">
                    <img src="logo_kanan.png" >
                </div>-->
                <div style="margin-top:-40px;"><h1 align="center" >Pemerintah Kota Palangka Raya<br/>
                Sekretariat Daerah Kota Palangka Raya</h1></div>
                <br/>
                <div style="margin-top:-40px;">
                <p align="center">Alamat : Jl. Cilik Riwut KM. 5,5 Bukit Tunggal, Jekan Raya, 
                <br />Palangkaraya - Kalimantan Tengah, Indonesia
                <br />Telp : (0536) 31488, kode pos 73112
                </p>
                </div>
            </div>';


//dapatkan lokasi content_print
$content_path = dirname($_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF']); 
//str_replace('/app/plugin/print/', '', $content_path);
//dirname($content_path, 2);
//$arr = explode('/', $content_path);
//$content_path = $arr[0].'/'.$arr[1].'/'.$arr[2];
//echo $content_path;
$secret = md5(mcrypt_create_iv(PBKDF2_SALT_BYTE_SIZE, MCRYPT_DEV_URANDOM));//TODO: secret ini temporary n bisa di akses melalui database dan akan dihapus saat sudah tercetak
$retval = $mysqli->query("INSERT INTO secure_cetak(session, secret) VALUES('".session_id()."', '$secret')");
echo $retval->error;
sleep(2);//manual delay tuk mencegah proses input mysql yg lambat (andaikan fungsi mysqli ini async alias non blocking function)
$content_path = 'http://'.$content_path.'/content_print.php?unit='.$path['unit'].'&module='.$path['module'].'&sid='.$path['sid']
                .'&do=cetak&secret='.$secret;
//echo $content_path;

//print_r($_SERVER);
//echo '$_SERVER[HTTP_COOKIE]=>'.$_SERVER['HTTP_COOKIE'];
//setting sessionnya bro
//tp gak perlu passing session kyknya aman2 aza...
$headers = array( 'Cookie: ' . $_SERVER['HTTP_COOKIE'].';', 'Connection: close');//,'Content-type: application/x-www-form-urlencoded'
$opts = array('http' => array(
                'user_agent' => $_SERVER['HTTP_USER_AGENT'], 
                'header'=> implode($headers, '\r\n'),
                'Cookie' => $_SERVER['HTTP_COOKIE']
               )
            );
//print_r($opts);
$context = stream_context_create($opts);
$url = file_get_contents($content_path, false, $context);

$html .= $url;

$html .= '
          </div>

          <div id="clearer"></div>
            <div id="footer" style="color:#444"><p>Copyright &copy; 2017 <a>KESRA</a>. All rights reserved.</p></div>
            </div>
          </div>
          </body>
          </html>
          ';


										
//harus include ini dulu
include 'plugin/print/mpdf.php';

//inisialisasi PDF
$mpdf=new mPDF('c'); 
$mpdf->SetDisplayMode('fullpage');

//khusus halaman module jadwal_pelajaran_detail 
//if ($_GET['module'] == 'jadwal_pelajaran_detail')
//  $mpdf->AddPage('L');


$mpdf->WriteHTML($html);
//cetak pdfnya
$mpdf->Output();

//echo $html;
//echo $content_path;
exit;
?>