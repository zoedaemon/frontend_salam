<?php

  include 'rt/config/koneksi.php';
  include 'rt/config/error_reporting.php';
  include 'rt/lib/lib_general.php';            
  include 'rt/lib/lib_timeout.php';
  include 'rt/lib/lib_security.php';
  include 'rt/lib/defined_session.php';


  use lib\general;
  use lib\timeout;
  use lib\security;

?>
<!DOCTYPE html>
<html>
<head>
	<title>Sistem Aplikasi Layanan Aspirasi Masyarakat</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">    
	<link href="rt/public/css/style.css" rel="stylesheet" type="text/css" media="screen" />
	<link href="rt/public/css/bootstrap.min.css" rel="stylesheet" type="text/css" media="screen" /> 
	<link href="rt/public/css/jquery.smartmenus.bootstrap.css" rel="stylesheet" type="text/css" media="screen" />

	<script>
	//var last_timestamp = 0;
	//alert(document.cookie);
	</script>
	<style>
	  /* Optional: Makes the sample page fill the window. */
	  html, body {
	    height: 100%;
	    margin: 0;
	    padding: 0;
	  }
	</style>
</head>
</body>
<div style=" margin:50px auto">
	<center>
		<span style="margin: 0px auto"><h2>SALAM - Sistem Aplikasi Layanan Aspirasi Masyarakat </h2></span>
	</center>
</div>
<div class="container-fluid padded" style="margin-top:40px;   ">	
<center>
<div id="titte" class="title-bar" > 
	&nbsp;
</div>  
</center>
</div>
<marquee>Silahkan kirim SMS Aspirasi/Pelaporan anda ke <strong>08115211777</strong> dengan format bebas tp wajib menyertakan lokasi kelurahan...terima kasih atas aspirasi anda</marquee>
<div class="container-fluid padded" style="margin-top:40px; background:url(rt/public/images/background-noise.png) repeat!important;">
		<div class="row md-col-12" style="margin-top:20px">
	
		<center>
		<br/>
		<div id="test" style="margin-top:50px;">
			<img src="public/walikota.jpg" height="250px"/>
			<img src="public/logo-palangkaraya.png" height="250px"/>
			<img src="public/wakilwalikota.jpg" height="250px"/>
		</div>
		</center>
		<div id="test" style="margin-top:50px;">
			
			<p style="margin-right:400px;margin-left:200px;display:inline;float:left;position:relative">
				<img src="public/abramsyah.jpg" height="200px"/>
				<br/><strong style="padding-left:100px">Abramsyah, S.Sos</strong>
				
					<br/>
					<span style="padding-left:5px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						Kepala Bagian Kesejahteraan Rakyat
					</span> 
					<br/>
					<span style="padding-left:5px">&nbsp;&nbsp;&nbsp;&nbsp;
						Sekretariat Daerah Kota Palangka Raya
					</span>
				</span>
			</p>
			
			<p style="display:inline">
				<span style="padding-left:60px"><img src="public/ery.jpg" height="200px"/></span>
				<br><strong style="padding-left:100px">Eriwan, SE</strong>
				<br/>Kepala sub bagian Sosial dan Kemasyarakatan
				<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;KESRA SETDA Kota Palangka Raya
			</p>
		</div> 
		<br/><br/>

	</div>
	<div class="row md-col-12" style="margin-left:-40px">
		<div class="flashmsg">
		<?php 
		  //flash.output() 
		?>
		</div>

		<?php 
		  include 'rt/modul/main/SHARED_aspirasi/public_index.php';
		?>
		<br/>
	</div>
</div>

</script>
	<script type="text/javascript" src="rt/public/js/jquery-1.10.2.min.js"> </script>   
	<script type="text/javascript" src="rt/public/js/bootstrap.min.js"> </script>   
	<script type="text/javascript" src="rt/public/js/jquery.smartmenus.min.js"> </script>   
	<script type="text/javascript" src="rt/public/js/jquery.smartmenus.bootstrap.min.js"> </script>
</body>
</body>
</html>
