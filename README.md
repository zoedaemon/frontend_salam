SALAM - Sistem Aplikasi Layanan Aspirasi Masyarakat

Fitur Utama

SALAM yang beralamatkan salam.palangkaraya.go.id ini memiliki backend terpadu. Dimana PHPClient.php adalah penerima data dari gammu-smsd, yang akan meneruskan data ke GoServer.go (BACKEND binary) untuk diproses secara detail. Pemprosesan tersebut meliputi, pengecekan pelaporan spam, tagging topik (tags), pembacaan lokasi, dan perangkingan pelaporan berdasarkan tags.